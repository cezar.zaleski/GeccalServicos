
truncate geccal.tb_descricao_movimento_conta;

select * from geccal.tb_movimentacao_conta_descricao;
select * from geccal.tb_descricao_movimento_conta;

insert into geccal.tb_descricao_movimento_conta values (1, 'Resgate Poupança');
insert into geccal.tb_descricao_movimento_conta values (2, 'Tarifa Manut. Conta Ativa, Extr.');
insert into geccal.tb_descricao_movimento_conta values (3, 'Crédito Cartões Cielo');
insert into geccal.tb_descricao_movimento_conta values (4, 'Cheque Compensado');
insert into geccal.tb_descricao_movimento_conta values (5, 'Transferência Online');
insert into geccal.tb_descricao_movimento_conta values (6, 'Débito automático');
insert into geccal.tb_descricao_movimento_conta values (7, 'Saldo Anterior');
insert into geccal.tb_descricao_movimento_conta values (8, 'Juros/Correção Monetária');
insert into geccal.tb_descricao_movimento_conta values (9, 'Resgate Automático C/P para C/C');
insert into geccal.tb_descricao_movimento_conta values (10, 'Depósitos');