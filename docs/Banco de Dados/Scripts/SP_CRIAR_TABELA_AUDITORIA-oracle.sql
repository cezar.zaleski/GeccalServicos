CREATE OR REPLACE PROCEDURE                SP_CRIAR_TABELA_AUDITORIA (P_OWNER VARCHAR2, P_TABLE  VARCHAR2) IS 
 
--CRIANDO O CURSOR 
CURSOR C_COLUMNS IS 
SELECT  column_name, data_type, data_length, data_precision, data_scale, nullable 
FROM    all_tab_columns 
WHERE   1 = 1 
AND owner       = P_OWNER 
AND table_name  = P_TABLE 
ORDER BY column_id; 
 
--VARIÁVEIS NECESSÁRIAS 
cOwnerDestino   VARCHAR2(100) := 'FIES_AUDITORIA'; 
vNomeTable      VARCHAR2(50)  := 'LG' || REPLACE(P_TABLE,'TB',''); 
vScriptTable    VARCHAR2(10000); 
 
 
BEGIN 
 
vScriptTable := 'CREATE TABLE ' || cOwnerDestino || '.' || vNomeTable || '( ' || CHR(13); 
 
FOR C_COLUMNS_REC IN C_COLUMNS LOOP 
 
vScriptTable := vScriptTable || '    ' || C_COLUMNS_REC.column_name  || ' ' 
 
--VERIFICANDO O TIPO DA DADOS DA COLUNA 
|| 
CASE WHEN C_COLUMNS_REC.data_type IN ('CHAR', 'VARCHAR', 'VARCHAR2') 
THEN C_COLUMNS_REC.data_type || '('||C_COLUMNS_REC.data_length||')' 
WHEN C_COLUMNS_REC.data_type IN ('NUMBER', 'DECIMAL', 'TIMESTAMP') 
THEN C_COLUMNS_REC.data_type || 
CASE WHEN C_COLUMNS_REC.data_precision is not null and C_COLUMNS_REC.data_scale is not null 
THEN '('||C_COLUMNS_REC.data_precision||','||C_COLUMNS_REC.data_scale||')' 
WHEN C_COLUMNS_REC.data_length is not null 
THEN '('||C_COLUMNS_REC.data_length||')' 
WHEN C_COLUMNS_REC.data_precision is not null 
THEN '('||C_COLUMNS_REC.data_precision||')' 
ELSE '' 
END 
ELSE C_COLUMNS_REC.data_type 
END 
--VERIFICA SE O CAMPO PODE SER NULO 
|| 
CASE WHEN C_COLUMNS_REC.nullable = 'N' 
THEN ' NOT NULL ' 
ELSE '' 
END 
 
--VIRGULA FINAL DAS COLUNAS 
|| ',' || CHR(13); 
 
END LOOP; 
 
--COLUNAS DE AUDITORIA 
vScriptTable := vScriptTable 
|| '    TP_OPERACAO_LOG CHAR(1) NOT NULL, ' || CHR(13) 
|| '    DT_OPERACAO_LOG TIMESTAMP(6) NOT NULL, ' || CHR(13) 
|| '    NO_USUARIO_BANCO_LOG VARCHAR2(100) NOT NULL, ' || CHR(13) 
|| '    NO_USUARIO_OS_LOG VARCHAR2(100) NOT NULL, ' || CHR(13) 
|| '    DS_ENDERECO_IP_LOG VARCHAR2(15) NOT NULL' ; 
 
--vScriptTable := SUBSTR(vScriptTable, 0, LENGTH(vScriptTable)-2); 
vScriptTable := vScriptTable || CHR(13) ||  ' );' || CHR(13); 
 
vScriptTable := vScriptTable || 'GRANT INSERT ON '||cOwnerDestino||'.'||vNomeTable||' TO ' || P_OWNER || ';' || CHR(13); 
 
DBMS_OUTPUT.PUT_LINE(vScriptTable); 
 
END;
  
  select owner from all_tab_columns;