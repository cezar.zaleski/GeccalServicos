ALTER TABLE geccal.tb_movimentacao_conta ADD
INDEX conta_nu_ano_mes (nu_ano_mes,id_conta);

ALTER TABLE geccal.tb_movimentacao_conta_descricao
ADD CONSTRAINT checkNuCheque CHECK(id_desc_movimento_conta = 4 AND nu_cheque is not null);
ALTER TABLE geccal.tb_movimentacao_conta_descricao
ADD CONSTRAINT checkDsTransferencia CHECK(id_desc_movimento_conta = 5 AND ds_transferencia is not null);
ALTER TABLE geccal.tb_movimentacao_conta_descricao
ADD CONSTRAINT checkDsDebito CHECK(id_desc_movimento_conta = 6 AND ds_debito is not null);

