
/**tipo de conta*/
INSERT INTO geccal.tb_tipo_conta VALUES (1, 'Corrente'); 
INSERT INTO geccal.tb_tipo_conta VALUES (2, 'Poupança'); 

/**contas*/
INSERT INTO geccal.tb_conta VALUES (1, 'Movimento de Conta Corrente', 6656.92, null, 1);
INSERT INTO geccal.tb_conta VALUES (2, 'Movimento de Conta Poupança', 119445.65, 1, 2);
INSERT INTO geccal.tb_conta VALUES (3, 'Movimento de Conta Poupança', 21657.47, 51, 2);