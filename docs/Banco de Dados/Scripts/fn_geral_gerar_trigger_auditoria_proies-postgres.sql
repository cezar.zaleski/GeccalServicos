CREATE OR REPLACE FUNCTION auditoria.fn_geral_gerar_trigger_auditoria_proies(tabela varchar, esquema varchar, esquema_destino varchar)
  RETURNS text
AS
$BODY$
  declare 
    vRegistro                 record;
    vFunction                 varchar(20000);
    vTrigger                  varchar(500);
    vRetorno                  text;
    vCorpotrg_ins1            varchar(10000);
    vCorpotrg_ins2            varchar(10000);
    vCorpotrg_upd1            varchar(10000);
    vCorpotrg_upd2            varchar(10000);
    vCorpotrg_del1            varchar(10000);
    vCorpotrg_del2            varchar(10000);
    vTabela                   varchar(5000);
    vGrantUsuario             varchar(500);
    vMaxcampo                 int4;
    i                         integer;
    v_no_tabela_auditoria     varchar;
    v_tabela_auditoria_criada boolean;
    v_contador                integer;
    v_script_correcao         varchar;
    v_script_view             varchar;

begin

    v_no_tabela_auditoria := 'lg_proies' || replace(tabela,'tb_fies','');

    -- Verifica se a tabela de auditoria existe
    if exists (select * 
               from pg_tables 
               where schemaname = esquema_destino and 
                     tablename  = v_no_tabela_auditoria)
    then
        v_tabela_auditoria_criada := true;
    else
        v_tabela_auditoria_criada := false;
    end if;


    -- Verifica se a trigger já está criada
    if exists (select tgname from pg_trigger where tgname = 'tg_auditoria_'|| replace(tabela,'tb_fies',''))
    then
        vTrigger  := chr(10) || 'drop trigger tg_auditoria_'|| replace(tabela,'tb_fies','') || ';' || chr(10);
    else
        vTrigger  := '';
    end if;

    
    vGrantUsuario  := 'alter table '|| coalesce(esquema_destino, 'public') || '.lg_proies' ||  replace($1,'tb_fies','')||' owner to enterprisedb; ' || chr(10) ||
                      'grant select on '|| coalesce(esquema_destino, 'public') || '.lg_proies' ||  replace($1,'tb_fies','')||' to sys'||current_database()||';';
    vTrigger  := vTrigger || chr(10)||'CREATE TRIGGER tg_auditoria_'|| replace(tabela,'tb_fies','') ||' AFTER INSERT OR UPDATE OR DELETE ON '|| COALESCE(esquema, 'public') || '.' || tabela ||
                 chr(10)||'FOR EACH ROW EXECUTE PROCEDURE '|| coalesce(esquema, 'public') || '.' || 'fn_auditoria'||SUBSTR($1,3,100)||'_trigger();';
    vFunction := chr(10)||'CREATE OR REPLACE FUNCTION ' || COALESCE(esquema, 'public') || '.' || 'fn_auditoria'||SUBSTR($1,3,100)||'_trigger() RETURNS trigger AS $BODY$'||
                 chr(10)||'declare '||
                 chr(10)||'        vnu_ip_cliente varchar(15);'||
                 chr(10)||'        vno_usuario_banco varchar(50);'||
                 chr(10)||'        vdt_log_alteracao timestamp;'||
                 chr(10)||'        vnu_spid    int4;'||
                 chr(10)||'begin '||
                 chr(10)||'    vdt_log_alteracao  := clock_timestamp();'||
                 chr(10)||'    vnu_spid           := pg_backend_pid();'||
                 chr(10)||'    vnu_ip_cliente     := inet_client_addr();'||
                 chr(10)||'    vno_usuario_banco  := session_user;';

                 
    i:= 1;
    vCorpotrg_ins1 := '';
    vCorpotrg_ins2 := '';
    vCorpotrg_upd1 := '';
    vCorpotrg_upd2 := '';
    vCorpotrg_del1 := '';
    vCorpotrg_del2 := '';
    vTabela        := '';
    v_script_correcao:= '';
    --

    SELECT max(ordinal_position) into vMaxcampo
    from information_schema.columns 
    where table_schema = esquema and
          table_name   = tabela;

    --
    for vRegistro in SELECT -- colunas da tabela a ser auditada
                            tab.ordinal_position         as attnum, 
			    tab.column_name              as attname, 
                            case
                              when tab.udt_name = 'bpchar' then 'char'
                              else tab.udt_name
                            end                          as typname, 
			    tab.character_maximum_length as attlen,
			    tab.numeric_precision        as precomma,
			    tab.numeric_scale            as postcomma,

			    -- colunas da tabela de auditoria
			    auditoria.ordinal_position         as attnum_aud, 
			    auditoria.column_name              as attname_aud, 
			    case
                              when auditoria.udt_name = 'bpchar' then 'char'
                              else auditoria.udt_name
                            end                                as typname_aud, 
			    auditoria.character_maximum_length as attlen_aud,
			    auditoria.numeric_precision        as precomma_aud,
			    auditoria.numeric_scale            as postcomma_aud
                     from information_schema.columns tab
                          left join (select * 
                                     from information_schema.columns 
                                     where 
                                        table_name   = v_no_tabela_auditoria and
                                        table_schema = esquema_destino)  as auditoria
                             on tab.column_name = auditoria.column_name
                     
		     where tab.table_schema = esquema and
                           tab.table_name   = tabela
                     order by tab.ordinal_position

    loop  
        if (i = 1) 
        then
        begin
            vTabela        := vTabela||chr(10)||'create table ' || 
                              coalesce(esquema_destino, 'public') || 
                              '.lg_proies'||
                              REPLACE($1,'tb_fies','')|| 
                              '( nu_spid int4 ,tp_log char(1),nu_ip_cliente varchar(15), no_usuario_banco varchar(100), dt_log_alteracao timestamp,'||
                              vRegistro.attname||' '
                                     ||case when vRegistro.typname not in ('varchar','char', 'bpchar', 'varbit','numeric','decimal') then vRegistro.typname 
                                            when vRegistro.typname in ('varchar','char', 'bpchar', 'varbit') then vRegistro.typname|| case
                                                                                                                               when vRegistro.attlen is not null then '('|| vRegistro.attlen||')'
                                                                                                                               else ''
                                                                                                                            end
                                            when vRegistro.typname in ('numeric','decimal') then vRegistro.typname|| case
                                                                                                                        when vRegistro.precomma is not null and
                                                                                                                             vRegistro.postcomma is not null then  ' ('||vRegistro.precomma||','||vRegistro.postcomma||')'
                                                                                                                        else ''
                                                                                                                     end
                                       end;
                                       
            vCorpotrg_ins1 := vCorpotrg_ins1||chr(10)||'    if TG_OP = ''INSERT'' then '||chr(10)||'        INSERT INTO ' || coalesce(esquema_destino, 'public') || '.lg_proies'||REPLACE($1,'tb_fies','')|| '( nu_spid,tp_log,nu_ip_cliente,no_usuario_banco,dt_log_alteracao,'||vRegistro.attname;
            vCorpotrg_ins2 := vCorpotrg_ins2||' values ( vnu_spid,''I'',vnu_ip_cliente,vno_usuario_banco,vdt_log_alteracao,NEW.'||vRegistro.attname;
            vCorpotrg_upd1 := vCorpotrg_upd1||chr(10)||'    if TG_OP = ''UPDATE'' then '||chr(10)||'        INSERT INTO ' || coalesce(esquema_destino, 'public') || '.lg_proies'||REPLACE($1,'tb_fies','')|| '( nu_spid,tp_log,nu_ip_cliente,no_usuario_banco,dt_log_alteracao,'||vRegistro.attname;
            vCorpotrg_upd2 := vCorpotrg_upd2||' values ( vnu_spid,''A'',vnu_ip_cliente,vno_usuario_banco,vdt_log_alteracao,NEW.'||vRegistro.attname;
            vCorpotrg_del1 := vCorpotrg_del1||chr(10)||'    if TG_OP = ''DELETE'' then '||chr(10)||'        INSERT INTO ' || coalesce(esquema_destino, 'public') || '.lg_proies'||REPLACE($1,'tb_fies','')|| '( nu_spid,tp_log,nu_ip_cliente,no_usuario_banco,dt_log_alteracao,'||vRegistro.attname;
            vCorpotrg_del2 := vCorpotrg_del2||' values ( vnu_spid,''E'',vnu_ip_cliente,vno_usuario_banco,vdt_log_alteracao,OLD.'||vRegistro.attname;
        end;
	else

        begin
            vTabela        := vTabela||','||vRegistro.attname||' '
                                     ||case when vRegistro.typname not in ('varchar','char', 'varbit','numeric','decimal') 
                                                 then vRegistro.typname
                                            when vRegistro.typname in ('varchar','char', 'varbit') 
                                                 then vRegistro.typname|| case
                                                                            when vRegistro.attlen is not null 
                                                                                 then '('|| vRegistro.attlen||')'
                                                                            else ''
                                                                          end
                                            when vRegistro.typname in ('numeric','decimal') 
                                                 then vRegistro.typname|| case
                                                                             when vRegistro.precomma is not null and vRegistro.postcomma is not null 
                                                                                  then ' ('||vRegistro.precomma||','||vRegistro.postcomma||')'
                                                                             else ''
                                                                          end
                                       end||
                                       case 
                                          when vRegistro.attnum = vMaxcampo then 
                                               ');'||chr(10) 
                                          else '' 
                                       end;  
                                       
            vCorpotrg_ins1 := vCorpotrg_ins1||','||vRegistro.attname||case when vRegistro.attnum = vMaxcampo then ')' else '' end;
            vCorpotrg_ins2 := vCorpotrg_ins2||',NEW.'||vRegistro.attname||case when vRegistro.attnum = vMaxcampo then ');'||chr(10)||'        RETURN NEW;'||chr(10)||'    end if;' else '' end;
            vCorpotrg_upd1 := vCorpotrg_upd1||','||vRegistro.attname||case when vRegistro.attnum = vMaxcampo then ')' else '' end;
            vCorpotrg_upd2 := vCorpotrg_upd2||',NEW.'||vRegistro.attname||case when vRegistro.attnum = vMaxcampo then ');'||chr(10)||'        RETURN NEW;'||chr(10)||'    end if;' else '' end;
            vCorpotrg_del1 := vCorpotrg_del1||','||vRegistro.attname||case when vRegistro.attnum = vMaxcampo then ')' else '' end;
            vCorpotrg_del2 := vCorpotrg_del2||',OLD.'||vRegistro.attname||case when vRegistro.attnum = vMaxcampo then ');'||chr(10)||'        RETURN OLD;'||chr(10)||'    end if;' else '' end;

        end;
        
        end if;

        -----------------------------------------------------
        -- Verifica se tabela sofreu alterações
        -----------------------------------------------------
        IF v_tabela_auditoria_criada is true
        then
            -- verifica se a coluna é nova e deverá ser inserida na auditoria
            if vRegistro.attname_aud is null
            then
               v_script_correcao := v_script_correcao || 'alter table ' || esquema_destino || '.' || v_no_tabela_auditoria || 
                                                         ' add ' || vRegistro.attname|| ' ' ||
                                                                    case when vRegistro.typname not in ('varchar','char','bpchar', 'varbit','numeric','decimal') 
                                                                              then vRegistro.typname
                                                                         when vRegistro.typname in ('varchar','char', 'bpchar', 'varbit') 
                                                                              then vRegistro.typname|| case
                                                                                                         when vRegistro.attlen is not null 
                                                                                                              then '('|| vRegistro.attlen||')'
                                                                                                         else ''
                                                                                                       end
                                                                         when vRegistro.typname in ('numeric','decimal') 
                                                                              then vRegistro.typname|| case
                                                                                                          when vRegistro.precomma is not null and vRegistro.postcomma is not null 
                                                                                                               then ' ('||vRegistro.precomma||','||vRegistro.postcomma||')'
                                                                                                          else ''
                                                                                                       end
                                                                    end || ';' || chr(10);

            else   

                    -- Verifica se houve alteração do tipo de dados ou tamanho
                    if (vRegistro.typname               <> vRegistro.typname_aud) or -- tipo de dados diferente
                       (coalesce(vRegistro.attlen,0)    <> coalesce(vRegistro.attlen_aud,0)) or -- tamanho (varchar) diferente
                       (coalesce(vRegistro.precomma,0)  <> coalesce(vRegistro.precomma_aud,0)) or -- Casas antes da virgula diferente
                       (coalesce(vRegistro.postcomma,0) <> coalesce(vRegistro.postcomma_aud,0)) -- casas posteriores à vírcula diferente
                    then
                        if (vRegistro.typname in ('varchar','char') and
                            vRegistro.typname_aud in ('numeric','decimal')) or 
                           (vRegistro.typname = vRegistro.typname_aud and
                            ((coalesce(vRegistro.attlen,0)    <> coalesce(vRegistro.attlen_aud,0)) or 
                             (coalesce(vRegistro.precomma,0)  <> coalesce(vRegistro.precomma_aud,0)) or 
                             (coalesce(vRegistro.postcomma,0) <> coalesce(vRegistro.postcomma_aud,0)))
                            )-- permite a alteração
                        then
                               v_script_correcao := v_script_correcao || 'alter table ' || esquema_destino || '.' || v_no_tabela_auditoria || 
                                                                         ' alter ' || vRegistro.attname|| ' type ' ||
                                                                                    case when vRegistro.typname not in ('varchar','char','bpchar', 'varbit','numeric','decimal') 
                                                                                              then vRegistro.typname
                                                                                         when vRegistro.typname in ('varchar','char', 'bpchar', 'varbit') 
                                                                                              then vRegistro.typname|| case
                                                                                                                         when vRegistro.attlen is not null 
                                                                                                                              then '('|| vRegistro.attlen||')'
                                                                                                                         else ''
                                                                                                                       end
                                                                                         when vRegistro.typname in ('numeric','decimal') 
                                                                                              then vRegistro.typname|| case
                                                                                                                          when vRegistro.precomma is not null and vRegistro.postcomma is not null 
                                                                                                                               then ' ('||vRegistro.precomma||','||vRegistro.postcomma||')'
                                                                                                                          else ''
                                                                                                                       end
                                                                                    end || ';' || chr(10);                    
                        else
                               -- renomeia coluna 
                               v_contador := 0;
                               --
                               select count(*) + 1
                               into v_contador
                               from information_schema.columns
                               where table_name   = tabela and
                                     table_schema = esquema and
                                     column_name  ilike vRegistro.attname || '_ren%';
                               -- 
                               v_script_correcao := v_script_correcao || 'alter table ' || esquema_destino || '.' || v_no_tabela_auditoria || 
                                                                         ' rename ' || vRegistro.attname|| ' to ' || vRegistro.attname || '_ren' || lpad(v_contador::varchar,2,'0') || ';' || chr(10);
                               
                               -- Insere nova coluna com o tipo de dados novo
                               v_script_correcao := v_script_correcao || 'alter table ' || esquema_destino || '.' || v_no_tabela_auditoria || 
                                                                         ' add ' || vRegistro.attname|| ' ' ||
                                                                                    case when vRegistro.typname not in ('varchar','char','bpchar', 'varbit','numeric','decimal')  
                                                                                              then vRegistro.typname
                                                                                         when vRegistro.typname in ('varchar','char', 'bpchar', 'varbit') 
                                                                                              then vRegistro.typname|| case
                                                                                                                         when vRegistro.attlen is not null 
                                                                                                                              then '('|| vRegistro.attlen||')'
                                                                                                                         else ''
                                                                                                                       end
                                                                                         when vRegistro.typname in ('numeric','decimal') 
                                                                                              then vRegistro.typname|| case
                                                                                                                          when vRegistro.precomma is not null and vRegistro.postcomma is not null 
                                                                                                                               then ' ('||vRegistro.precomma||','||vRegistro.postcomma||')'
                                                                                                                          else ''
                                                                                                                       end
                                                                                    end || ';' || chr(10);
                        end if;

                    end if;
            
            end if;

            
        end if;

        --- Incrementa contador i
        i := i+1;

    end loop;
    --
    vFunction := vFunction||vCorpotrg_ins1||vCorpotrg_ins2 ||vCorpotrg_upd1||vCorpotrg_upd2||vCorpotrg_del1||vCorpotrg_del2;
    vFunction := vFunction||chr(10)||'end; '||chr(10)||
                '$BODY$ LANGUAGE plpgsql SECURITY DEFINER;' ;
    vFunction := vFunction||chr(10)||'ALTER FUNCTION ' || COALESCE(esquema, 'public') || '.' || 'fn_auditoria'||SUBSTR($1,3,100)||'_trigger() OWNER to enterprisedb;';
    --
    vRetorno := 'DROP VIEW if exists ' || esquema_destino || '.' || 'vw_' || v_no_tabela_auditoria || ';' || chr(10) ||
                v_script_correcao  || chr(10) || 
                case
                   when v_tabela_auditoria_criada is false
                        then vTabela ||chr(10)
                   else ''
                end  || vGrantUsuario  || chr(10) || vFunction || chr(10) || 
                vTrigger || chr(10);

    /*
    EXECUTE vTabela;
    EXECUTE vGrantUsuario;
    EXECUTE vFunction;
    EXECUTE vTrigger;
    --*/
    --
    execute vRetorno;
    
    select *
    into v_script_view
    from auditoria.fn_gerar_view_auditoria(v_no_tabela_auditoria, esquema_destino, null);
    execute v_script_view;

    vRetorno := vRetorno || chr(10) || v_script_view;
    
    return vRetorno;
end;
$BODY$
LANGUAGE plpgsql VOLATILE;
