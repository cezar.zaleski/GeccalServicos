CREATE OR REPLACE PROCEDURE                SP_CRIAR_TRIGGER_AUDITORIA (P_OWNER VARCHAR2, P_TABLE  VARCHAR2) IS 
 
CURSOR C_COLUMNS IS SELECT  column_name, data_type 
FROM    all_tab_columns 
WHERE   1 = 1 
AND owner       = P_OWNER 
AND table_name  = P_TABLE 
ORDER BY column_id; 
 
--VARIÁVEIS 
vScriptTrigger           VARCHAR2(10000); 
vColunasInsert           VARCHAR2(10000) := ''; 
vColunasInsertValuesNEW  VARCHAR2(10000) := ''; 
vColunasInsertValuesOLD  VARCHAR2(10000) := ''; 
vNomeTriggerAuditoria    VARCHAR2(50)    := SUBSTR( ('TG_AUDIT_' || REPLACE(P_TABLE,'TB_','')) ,0, 30); 
vNomeTabelaLOG           VARCHAR2(50) := 'LG' || REPLACE(P_TABLE,'TB',''); 
 
BEGIN 
dbms_output.put_line( 'CREATE OR REPLACE TRIGGER '||P_OWNER||'.'|| vNomeTriggerAuditoria); 
dbms_output.put_line( 'AFTER '); 
dbms_output.put_line( 'INSERT OR DELETE OR UPDATE '); 
dbms_output.put_line( 'ON '||P_OWNER||'.'||P_TABLE); 
dbms_output.put_line( 'REFERENCING NEW AS NEW OLD AS OLD '); 
dbms_output.put_line( 'FOR EACH ROW '); 
dbms_output.put_line( 'DECLARE '); 
 
dbms_output.put_line( '  vTP_OPERACAO_LOG        CHAR(1);'); 
dbms_output.put_line( '  vDT_OPERACAO_LOG        DATE          := SYSDATE;'); 
dbms_output.put_line( '  vNO_USUARIO_BANCO_LOG   VARCHAR2(100) := SYS_CONTEXT(''USERENV'', ''SESSION_USER'');'); 
dbms_output.put_line( '  vNO_USUARIO_OS_LOG      VARCHAR2(100) := SYS_CONTEXT(''USERENV'', ''OS_USER'');'); 
dbms_output.put_line( '  vDS_ENDERECO_IP_LOG     VARCHAR2(15)  := SYS_CONTEXT(''USERENV'', ''IP_ADDRESS'');'); 
 
dbms_output.put_line( 'BEGIN '); 
 
dbms_output.put_line( '  IF DELETING THEN '); 
dbms_output.put_line( '    vTP_OPERACAO_LOG := ''D''; '); 
dbms_output.put_line( '  ELSIF UPDATING THEN '); 
dbms_output.put_line( '    vTP_OPERACAO_LOG := ''U''; '); 
dbms_output.put_line( '  ELSIF INSERTING THEN '); 
dbms_output.put_line( '    vTP_OPERACAO_LOG := ''I''; '); 
dbms_output.put_line( '  END IF; '); 
 
 
FOR C_COLUMNS_REC IN C_COLUMNS LOOP 
 
vColunasInsert := vColunasInsert || C_COLUMNS_REC.column_name || ', '; 
vColunasInsertValuesNEW := vColunasInsertValuesNEW || ':NEW.' || C_COLUMNS_REC.column_name || ', '; 
vColunasInsertValuesOLD := vColunasInsertValuesOLD || ':OLD.' || C_COLUMNS_REC.column_name || ', '; 
 
END LOOP; 
 
--vTP_OPERACAO_LOG, vDT_OPERACAO_LOG, vNO_USUARIO_BANCO_LOG, vNO_USUARIO_OS_LOG, vDS_ENDERECO_IP_LOG 
 
vScriptTrigger := vScriptTrigger || '  IF vTP_OPERACAO_LOG <> ''D'' THEN ' || CHR(13); 
vScriptTrigger := vScriptTrigger || '    INSERT INTO FIES_AUDITORIA.'||vNomeTabelaLOG||' ('|| vColunasInsert || ' TP_OPERACAO_LOG, DT_OPERACAO_LOG, NO_USUARIO_BANCO_LOG, NO_USUARIO_OS_LOG, DS_ENDERECO_IP_LOG  ) VALUES ( '|| vColunasInsertValuesNEW || 'vTP_OPERACAO_LOG, vDT_OPERACAO_LOG, vNO_USUARIO_BANCO_LOG, vNO_USUARIO_OS_LOG, NVL(vDS_ENDERECO_IP_LOG,''local'') );' || CHR(13); 
--vScriptTrigger := vScriptTrigger || '    RETURN NEW ' || CHR(13); 
vScriptTrigger := vScriptTrigger || '  ELSE ' || CHR(13); 
vScriptTrigger := vScriptTrigger || '    INSERT INTO FIES_AUDITORIA.'||vNomeTabelaLOG||' ('|| vColunasInsert || ' TP_OPERACAO_LOG, DT_OPERACAO_LOG, NO_USUARIO_BANCO_LOG, NO_USUARIO_OS_LOG, DS_ENDERECO_IP_LOG  ) VALUES ( '|| vColunasInsertValuesOLD || 'vTP_OPERACAO_LOG, vDT_OPERACAO_LOG, vNO_USUARIO_BANCO_LOG, vNO_USUARIO_OS_LOG, NVL(vDS_ENDERECO_IP_LOG,''local'') );' || CHR(13); 
--vScriptTrigger := vScriptTrigger || '    RETURN NEW ' || CHR(13); 
vScriptTrigger := vScriptTrigger || '  END IF;' || CHR(13); 
 
dbms_output.put_line( vScriptTrigger ); 
 
dbms_output.put_line( 'EXCEPTION '); 
dbms_output.put_line( 'WHEN OTHERS THEN '); 
dbms_output.put_line( '  RAISE_APPLICATION_ERROR(-20001, ''Erro na trigger de transacao da tabela '||P_OWNER||'.'||P_TABLE||'''); '); 
dbms_output.put_line( 'END;'); 
 
 
--ELSE 
--    dbms_output.put_line( ' Erro! Tabela sem PK - '||P_OWNER||'.'||P_TABLE||' '); 
--END IF; 
 
END;