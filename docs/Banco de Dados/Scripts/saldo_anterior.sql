select 
  SUM(CASE WHEN (rg.id_tipo_registro_caixa = 1 and id_tipo_pagamento = 1) THEN pg.nu_valor ELSE 0 END) -
  SUM(CASE WHEN (rg.id_tipo_registro_caixa = 2 and id_tipo_pagamento = 1) THEN pg.nu_valor ELSE 0 END) AS sl_dinheiro,
  SUM(CASE WHEN (rg.id_tipo_registro_caixa = 1 and id_tipo_pagamento = 2) THEN pg.nu_valor ELSE 0 END)  -
  SUM(CASE WHEN (rg.id_tipo_registro_caixa = 2 and id_tipo_pagamento = 2) THEN pg.nu_valor ELSE 0 END) AS sl_cheque,
  SUM(CASE WHEN (rg.id_tipo_registro_caixa = 1 and id_tipo_pagamento = 3) THEN pg.nu_valor ELSE 0 END)  -
  SUM(CASE WHEN (rg.id_tipo_registro_caixa = 2 and id_tipo_pagamento = 3) THEN pg.nu_valor ELSE 0 END) AS sl_debito_entrada,
  SUM(CASE WHEN (rg.id_tipo_registro_caixa = 1 and id_tipo_pagamento = 4) THEN pg.nu_valor ELSE 0 END) -
  SUM(CASE WHEN (rg.id_tipo_registro_caixa = 2 and id_tipo_pagamento = 4) THEN pg.nu_valor ELSE 0 END) AS sl_credito_entrada
from geccal.tb_registro_caixa rg
  inner join geccal.tb_tipo_registro_caixa rc on rc.id_tipo_registro_caixa = rg.id_tipo_registro_caixa
  inner join geccal.tb_pagamento pg on pg.id_registro_caixa = rg.id_registro_caixa;