CREATE DATABASE  IF NOT EXISTS `geccal` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `geccal`;
-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: 192.168.25.18    Database: geccal
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acao`
--

DROP TABLE IF EXISTS `acao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acao` (
  `id_acao` int(11) NOT NULL AUTO_INCREMENT,
  `no_acao` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `no_menu` varchar(45) DEFAULT NULL,
  `id_controller` int(11) NOT NULL,
  `id_tipo_acao` int(11) NOT NULL,
  PRIMARY KEY (`id_acao`),
  KEY `fk_action_controller1_idx` (`id_controller`),
  KEY `fk_acao_tipo_acao1_idx` (`id_tipo_acao`),
  CONSTRAINT `fk_acao_tipo_acao1` FOREIGN KEY (`id_tipo_acao`) REFERENCES `tipo_acao` (`id_tipo_acao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_action_controller1` FOREIGN KEY (`id_controller`) REFERENCES `controller` (`id_controller`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acao`
--

LOCK TABLES `acao` WRITE;
/*!40000 ALTER TABLE `acao` DISABLE KEYS */;
INSERT INTO `acao` VALUES (1,'cadastrar',1,'Cadastrar',1,1),(2,'listar',1,'Listar',1,4),(3,'editar',1,NULL,1,2),(4,'excluir',1,NULL,1,3),(5,'cadastrar',1,'Registrar',2,1),(6,'listar',1,'Listar',2,4),(7,'editar',1,NULL,2,2),(8,'excluir',1,NULL,2,3),(9,'gerar relatório',1,NULL,2,5),(10,'listar',1,'Listar',3,4);
/*!40000 ALTER TABLE `acao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conta`
--

DROP TABLE IF EXISTS `conta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conta` (
  `id_conta` int(11) NOT NULL AUTO_INCREMENT,
  `no_conta` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `id_tipo_conta` int(11) NOT NULL,
  PRIMARY KEY (`id_conta`),
  KEY `fk_conta_tipo_conta1_idx` (`id_tipo_conta`),
  CONSTRAINT `fk_conta_tipo_conta1` FOREIGN KEY (`id_tipo_conta`) REFERENCES `tipo_conta` (`id_tipo_conta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conta`
--

LOCK TABLES `conta` WRITE;
/*!40000 ALTER TABLE `conta` DISABLE KEYS */;
/*!40000 ALTER TABLE `conta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controller`
--

DROP TABLE IF EXISTS `controller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controller` (
  `id_controller` int(11) NOT NULL AUTO_INCREMENT,
  `no_controller` varchar(45) NOT NULL,
  `no_menu` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `no_img` varchar(45) NOT NULL,
  `id_modulo` int(11) NOT NULL,
  PRIMARY KEY (`id_controller`),
  KEY `fk_controller_modulo1_idx` (`id_modulo`),
  CONSTRAINT `fk_controller_modulo1` FOREIGN KEY (`id_modulo`) REFERENCES `modulo` (`id_modulo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controller`
--

LOCK TABLES `controller` WRITE;
/*!40000 ALTER TABLE `controller` DISABLE KEYS */;
INSERT INTO `controller` VALUES (1,'perfil','Perfil',1,'fa fa-user-md fa-fw',1),(2,'registro-caixa','Registro de Caixa',1,'fa fa-user-md fa-fw',2),(3,'relatorio','Relatórios',1,'fa fa-user-md fa-fw',2);
/*!40000 ALTER TABLE `controller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `desativacao_usuario`
--

DROP TABLE IF EXISTS `desativacao_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `desativacao_usuario` (
  `id_desativacao_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `dt_desativacao` date NOT NULL,
  `no_motivo_desativacao` text NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `no_motivo_ativacao` text,
  `dt_ativacao` date DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_desativacao_usuario`),
  KEY `fk_desativacao_usuario_usuario1_idx` (`id_usuario`),
  CONSTRAINT `fk_desativacao_usuario_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `desativacao_usuario`
--

LOCK TABLES `desativacao_usuario` WRITE;
/*!40000 ALTER TABLE `desativacao_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `desativacao_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `descricao_lancamento`
--

DROP TABLE IF EXISTS `descricao_lancamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `descricao_lancamento` (
  `id_desc_lancamento` int(11) NOT NULL AUTO_INCREMENT,
  `no_desc_lancamento` varchar(45) NOT NULL,
  `co_desc_lancamento` varchar(45) NOT NULL,
  `id_tipo_lancamento` int(11) NOT NULL,
  PRIMARY KEY (`id_desc_lancamento`),
  KEY `fk_descricao_lancamento_tipo_lancamento_idx` (`id_tipo_lancamento`),
  CONSTRAINT `fk_descricao_lancamento_tipo_lancamento` FOREIGN KEY (`id_tipo_lancamento`) REFERENCES `tipo_lancamento` (`id_tipo_lancamento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `descricao_lancamento`
--

LOCK TABLES `descricao_lancamento` WRITE;
/*!40000 ALTER TABLE `descricao_lancamento` DISABLE KEYS */;
INSERT INTO `descricao_lancamento` VALUES (1,'Mensalidade de Associados','1.1',1),(2,'Venda de Livros','1.2',1),(3,'Rendimentos de Aplicações Financeiras','1.3',1),(4,'Promoção Beneficiente','1.4',1),(5,'Donativos','1.5',1),(6,'Receita Bazar','1.6',1),(7,'Recuperação de Despesas','1.7',1),(8,'Doações de Gêneros Materiais','1.8',1),(9,'Souvenirs GECCAL 40 anos','1.9',1),(10,'Caixa','2.1',2),(11,'Banco do Brasil S/A - C/Corrente','2.2',2),(12,'Banco do Brasil S/A - C/Poupança','2.3',2),(13,'Água, Luz e Telefone','3.1',3),(14,'Despesas c/ Assistência Social','3.2',3),(15,'Aquisição de Livros p/ Venda','3.3',3),(16,'Salários, Gratificações, Férias e 13 Sal.','3.4',3),(17,'INSS, FGTS e outros encargos','3.5',3),(18,'Material de Limpeza e Higiene','3.6',3),(19,'Materiais de Escritório e Didáticos','3.7',3),(20,'Despesas Bancárias','3.8',3),(21,'Emolumentos e Taxas diversas','3.9',3),(22,'Manutenção e Reparos','3.10',3),(23,'Custos de Promoções Beneficentes','3.11',3),(24,'Contribuições Diversas','3.12',3),(25,'Despesas com Eventos Sociais','3.13',3),(26,'Despesas Seguro Predial','3.14',3),(27,'Móveis e Utensílios','4.1',4),(28,'Máquinas e Aparelhos','4.2',4),(29,'Instalações','4.3',4),(30,'Crédito Cielo','1.10',1);
/*!40000 ALTER TABLE `descricao_lancamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `descricao_movimento_conta`
--

DROP TABLE IF EXISTS `descricao_movimento_conta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `descricao_movimento_conta` (
  `id_desc_movimento_conta` int(11) NOT NULL AUTO_INCREMENT,
  `no_desc_movimentacao_conta` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_desc_movimento_conta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `descricao_movimento_conta`
--

LOCK TABLES `descricao_movimento_conta` WRITE;
/*!40000 ALTER TABLE `descricao_movimento_conta` DISABLE KEYS */;
/*!40000 ALTER TABLE `descricao_movimento_conta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historico`
--

DROP TABLE IF EXISTS `historico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historico` (
  `id_historico` int(11) NOT NULL AUTO_INCREMENT,
  `dt_historico` date NOT NULL,
  `no_acao` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_historico`),
  KEY `fk_historico_usuario1_idx` (`id_usuario`),
  CONSTRAINT `fk_historico_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historico`
--

LOCK TABLES `historico` WRITE;
/*!40000 ALTER TABLE `historico` DISABLE KEYS */;
/*!40000 ALTER TABLE `historico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulo`
--

DROP TABLE IF EXISTS `modulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulo` (
  `id_modulo` int(11) NOT NULL AUTO_INCREMENT,
  `no_modulo` varchar(45) NOT NULL,
  `no_menu` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_modulo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulo`
--

LOCK TABLES `modulo` WRITE;
/*!40000 ALTER TABLE `modulo` DISABLE KEYS */;
INSERT INTO `modulo` VALUES (1,'admin','Administrador',1),(2,'tesouraria','Tesouraria',1);
/*!40000 ALTER TABLE `modulo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimentacao_conta`
--

DROP TABLE IF EXISTS `movimentacao_conta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimentacao_conta` (
  `id_movimentacao_conta` int(11) NOT NULL AUTO_INCREMENT,
  `nu_mes` int(11) NOT NULL,
  `nu_ano` int(11) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `id_conta` int(11) NOT NULL,
  `id_desc_movimento_conta` int(11) NOT NULL,
  PRIMARY KEY (`id_movimentacao_conta`),
  KEY `fk_movimentacao_conta_conta1_idx` (`id_conta`),
  KEY `fk_movimentacao_conta_descricao_movimento_conta1_idx` (`id_desc_movimento_conta`),
  CONSTRAINT `fk_movimentacao_conta_conta1` FOREIGN KEY (`id_conta`) REFERENCES `conta` (`id_conta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_movimentacao_conta_descricao_movimento_conta1` FOREIGN KEY (`id_desc_movimento_conta`) REFERENCES `descricao_movimento_conta` (`id_desc_movimento_conta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimentacao_conta`
--

LOCK TABLES `movimentacao_conta` WRITE;
/*!40000 ALTER TABLE `movimentacao_conta` DISABLE KEYS */;
/*!40000 ALTER TABLE `movimentacao_conta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagamento`
--

DROP TABLE IF EXISTS `pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagamento` (
  `id_pagamento` int(11) NOT NULL AUTO_INCREMENT,
  `nu_valor` double NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `id_tipo_pagamento` int(11) NOT NULL,
  `id_registro_caixa` int(11) NOT NULL,
  PRIMARY KEY (`id_pagamento`),
  KEY `fk_forma_pagamento_tipo_pagamento1_idx` (`id_tipo_pagamento`),
  KEY `fk_forma_pagamento_registro_caixa1_idx` (`id_registro_caixa`),
  CONSTRAINT `fk_forma_pagamento_registro_caixa1` FOREIGN KEY (`id_registro_caixa`) REFERENCES `registro_caixa` (`id_registro_caixa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_forma_pagamento_tipo_pagamento1` FOREIGN KEY (`id_tipo_pagamento`) REFERENCES `tipo_pagamento` (`id_tipo_pagamento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagamento`
--

LOCK TABLES `pagamento` WRITE;
/*!40000 ALTER TABLE `pagamento` DISABLE KEYS */;
INSERT INTO `pagamento` VALUES (16,25,1,3,89),(17,25,1,3,89),(18,25,1,3,89),(19,25,1,3,89),(46,25,1,2,93),(51,52.45,1,4,94),(62,52,1,1,74);
/*!40000 ALTER TABLE `pagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil` (
  `id_perfil` int(11) NOT NULL AUTO_INCREMENT,
  `no_perfil` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_perfil`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil`
--

LOCK TABLES `perfil` WRITE;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` VALUES (1,'Administrador',1),(2,'Teste',1),(3,'asdd',1);
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil_acao`
--

DROP TABLE IF EXISTS `perfil_acao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil_acao` (
  `id_perfil` int(11) NOT NULL,
  `id_acao` int(11) NOT NULL,
  PRIMARY KEY (`id_perfil`,`id_acao`),
  KEY `fk_perfil_has_acao_acao1_idx` (`id_acao`),
  KEY `fk_perfil_has_acao_perfil1_idx` (`id_perfil`),
  CONSTRAINT `fk_perfil_has_acao_acao1` FOREIGN KEY (`id_acao`) REFERENCES `acao` (`id_acao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_perfil_has_acao_perfil1` FOREIGN KEY (`id_perfil`) REFERENCES `perfil` (`id_perfil`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil_acao`
--

LOCK TABLES `perfil_acao` WRITE;
/*!40000 ALTER TABLE `perfil_acao` DISABLE KEYS */;
INSERT INTO `perfil_acao` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10);
/*!40000 ALTER TABLE `perfil_acao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa` (
  `id_pessoa` int(11) NOT NULL AUTO_INCREMENT,
  `no_pessoa` varchar(100) NOT NULL,
  `dt_cadastro` date NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `no_endereco` text,
  `no_bairro` varchar(45) DEFAULT NULL,
  `nu_cep` int(11) DEFAULT NULL,
  `no_cidade` varchar(45) DEFAULT NULL,
  `no_email` text,
  PRIMARY KEY (`id_pessoa`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pessoa`
--

LOCK TABLES `pessoa` WRITE;
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
INSERT INTO `pessoa` VALUES (1,'Geccal','2014-12-27',1,'','',0,'',NULL);
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro_caixa`
--

DROP TABLE IF EXISTS `registro_caixa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro_caixa` (
  `id_registro_caixa` int(11) NOT NULL AUTO_INCREMENT,
  `dt_registro` date NOT NULL,
  `no_descricao` text NOT NULL,
  `nu_valor` double NOT NULL,
  `id_desc_lancamento` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `id_tipo_registro_caixa` int(11) NOT NULL,
  PRIMARY KEY (`id_registro_caixa`),
  KEY `fk_registro_caixa_descricao_lancamento1_idx` (`id_desc_lancamento`),
  KEY `fk_registro_caixa_usuario1_idx` (`id_usuario`),
  KEY `fk_registro_caixa_tipo_registro_caixa1_idx` (`id_tipo_registro_caixa`),
  CONSTRAINT `fk_registro_caixa_descricao_lancamento1` FOREIGN KEY (`id_desc_lancamento`) REFERENCES `descricao_lancamento` (`id_desc_lancamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_caixa_tipo_registro_caixa1` FOREIGN KEY (`id_tipo_registro_caixa`) REFERENCES `tipo_registro_caixa` (`id_tipo_registro_caixa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_caixa_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro_caixa`
--

LOCK TABLES `registro_caixa` WRITE;
/*!40000 ALTER TABLE `registro_caixa` DISABLE KEYS */;
INSERT INTO `registro_caixa` VALUES (1,'2000-11-04','Venda de Livros Recibo nº 00035',120,2,1,1,1),(2,'2015-11-02','Crédito Cielo',52.03,30,1,1,2),(3,'2015-11-02','Taxa Cielo',1.97,20,1,1,2),(4,'2015-10-03','Salário Zeladora ref 8/2000',772.82,16,1,1,2),(5,'2000-10-03','FGTS competência. 9/2000',59.84,17,1,1,2),(6,'2015-10-04','Mens. Assoc. Recibo nº 00104, 00106, 00107',190,1,1,1,1),(7,'2000-10-04','Almoço Beneficente Ingressos recibos nº 00109, 00113',2020,4,1,1,1),(8,'2000-10-04','Almoço Beneficente Carnes recibos nº 00110',200,4,1,1,1),(9,'2000-10-01','Papelaria Anderson, Materias Papelaria CCF 004112',295.4,19,1,1,2),(10,'2000-10-02','Mari Papelaria, Mat Papelaria NF 1013',130,19,1,1,2),(11,'2000-10-02','Debito automático tarifa bancária ',21.9,20,1,1,1),(12,'2000-10-02','Tarifa Manutenção Conta Ativa',21.9,20,1,1,2),(13,'2000-10-04','Venda de Livros Recibo nº 00035',120,2,1,1,1),(14,'2000-10-04','Venda de Livros Recibo nº 00035',120,2,1,1,1),(15,'2000-10-02','Crédito Cielo',52.03,30,1,1,2),(16,'2000-10-02','Taxa Cielo',1.97,20,1,1,2),(17,'2000-10-03','Salário Zeladora ref 8/2000',772.82,16,1,1,2),(18,'2000-10-03','FGTS competência. 9/2000',59.84,17,1,1,2),(19,'2000-10-04','Mens. Assoc. Recibo nº 00104, 00106, 00107',190,1,1,1,1),(20,'2000-10-04','Almoço Beneficente Ingressos recibos nº 00109, 00113',2020,4,1,1,1),(21,'2000-10-04','Venda de Livros Recibo nº 00035',120,2,1,1,1),(22,'2000-10-02','Crédito Cielo',52.03,30,1,1,2),(23,'2000-10-02','Taxa Cielo',1.97,20,1,1,2),(24,'2000-10-03','Salário Zeladora ref 8/2000',772.82,16,1,1,2),(25,'2000-10-03','FGTS competência. 9/2000',59.84,17,1,1,2),(26,'2000-10-04','Mens. Assoc. Recibo nº 00104, 00106, 00107',190,1,1,1,1),(27,'2000-10-04','Almoço Beneficente Ingressos recibos nº 00109, 00113',2020,4,1,1,1),(28,'2000-10-04','Almoço Beneficente Carnes recibos nº 00110',200,4,1,1,1),(29,'2000-10-01','Papelaria Anderson, Materias Papelaria CCF 004112',295.4,19,1,1,2),(30,'2000-10-02','Mari Papelaria, Mat Papelaria NF 1013',130,19,1,1,2),(31,'2000-10-02','Debito automático tarifa bancária ',21.9,20,1,1,1),(32,'2000-10-02','Tarifa Manutenção Conta Ativa',21.9,20,1,1,2),(33,'2000-10-04','Venda de Livros Recibo nº 00035',120,2,1,1,1),(34,'2000-10-04','Venda de Livros Recibo nº 00035',120,2,1,1,1),(35,'2000-10-02','Crédito Cielo',52.03,30,1,1,2),(36,'2000-10-02','Taxa Cielo',1.97,20,1,1,2),(37,'2000-10-03','Salário Zeladora ref 8/2000',772.82,16,1,1,2),(38,'2000-10-03','FGTS competência. 9/2000',59.84,17,1,1,2),(39,'2000-10-04','Mens. Assoc. Recibo nº 00104, 00106, 00107',190,1,1,1,1),(40,'2000-10-04','Almoço Beneficente Ingressos recibos nº 00109, 00113',2020,4,1,1,1),(41,'2000-10-04','Almoço Beneficente Carnes recibos nº 00110',200,4,1,1,1),(42,'2000-10-01','Papelaria Anderson, Materias Papelaria CCF 004112',295.4,19,1,1,2),(43,'2000-10-02','Mari Papelaria, Mat Papelaria NF 1013',130,19,1,1,2),(44,'2000-10-02','Debito automático tarifa bancária ',21.9,20,1,1,1),(45,'2000-10-02','Tarifa Manutenção Conta Ativa',21.9,20,1,1,2),(46,'2000-10-04','Venda de Livros Recibo nº 00035',120,2,1,1,1),(47,'2000-10-04','Venda de Livros Recibo nº 00035',120,2,1,1,1),(48,'2000-10-02','Crédito Cielo',52.03,30,1,1,2),(49,'2000-10-02','Taxa Cielo',1.97,20,1,1,2),(50,'2000-10-03','Salário Zeladora ref 8/2000',772.82,16,1,1,2),(51,'2000-10-03','FGTS competência. 9/2000',59.84,17,1,1,2),(52,'2000-10-04','Mens. Assoc. Recibo nº 00104, 00106, 00107',190,1,1,1,1),(53,'2000-10-04','Almoço Beneficente Ingressos recibos nº 00109, 00113',2020,4,1,1,1),(54,'2000-10-04','Almoço Beneficente Carnes recibos nº 00110',200,4,1,1,1),(55,'2000-10-01','Papelaria Anderson, Materias Papelaria CCF 004112',295.4,19,1,1,2),(56,'2000-10-02','Mari Papelaria, Mat Papelaria NF 1013',130,19,1,1,2),(57,'2000-10-02','Debito automático tarifa bancária ',21.9,20,1,1,1),(58,'2000-10-02','Tarifa Manutenção Conta Ativa',21.9,20,1,1,2),(59,'2000-10-04','Venda de Livros Recibo nº 00035',120,2,1,1,1),(60,'2000-10-04','Venda de Livros Recibo nº 00035',120,2,1,1,1),(61,'2000-10-02','Crédito Cielo',52.03,30,1,1,2),(62,'2000-10-02','Taxa Cielo',1.97,20,1,1,2),(63,'2000-10-03','Salário Zeladora ref 8/2000',772.82,16,1,1,2),(64,'2000-10-03','FGTS competência. 9/2000',59.84,17,1,1,2),(65,'2000-10-04','Mens. Assoc. Recibo nº 00104, 00106, 00107',190,1,1,1,1),(66,'2000-10-04','Almoço Beneficente Ingressos recibos nº 00109, 00113',2020,4,1,1,1),(67,'2000-10-04','Almoço Beneficente Carnes recibos nº 00110',200,4,1,1,1),(68,'2000-10-01','Papelaria Anderson, Materias Papelaria CCF 004112',295.4,19,1,1,2),(69,'2000-10-02','Mari Papelaria, Mat Papelaria NF 1013',130,19,1,1,2),(70,'2000-10-02','Debito automático tarifa bancária ',21.9,20,1,1,1),(71,'2000-10-02','Tarifa Manutenção Conta Ativa',21.9,20,1,1,2),(72,'2000-10-04','Venda de Livros Recibo nº 00035',120,2,1,1,1),(74,'2015-03-01','asddas',52.45,1,1,1,1),(75,'2015-02-22','test',25.46,2,1,1,1),(76,'2015-02-22','sad',52.45,1,1,1,1),(80,'2015-03-01','Teste forma pagamento',50,1,1,1,1),(81,'2015-03-01','Teste forma pagamento',50,1,1,1,1),(89,'2015-03-01','Teste forma pagamento',50,1,1,1,1),(93,'2015-03-01','teste update3',50,1,1,1,1),(94,'2015-03-01','novo',50,1,1,1,1);
/*!40000 ALTER TABLE `registro_caixa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefone`
--

DROP TABLE IF EXISTS `telefone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefone` (
  `id_telefone` int(11) NOT NULL AUTO_INCREMENT,
  `nu_telefone` int(11) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `id_pessoa` int(11) NOT NULL,
  PRIMARY KEY (`id_telefone`),
  KEY `fk_telefone_pessoa1_idx` (`id_pessoa`),
  CONSTRAINT `fk_telefone_pessoa1` FOREIGN KEY (`id_pessoa`) REFERENCES `pessoa` (`id_pessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefone`
--

LOCK TABLES `telefone` WRITE;
/*!40000 ALTER TABLE `telefone` DISABLE KEYS */;
/*!40000 ALTER TABLE `telefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_acao`
--

DROP TABLE IF EXISTS `tipo_acao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_acao` (
  `id_tipo_acao` int(11) NOT NULL AUTO_INCREMENT,
  `no_tipo_acao` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tipo_acao`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_acao`
--

LOCK TABLES `tipo_acao` WRITE;
/*!40000 ALTER TABLE `tipo_acao` DISABLE KEYS */;
INSERT INTO `tipo_acao` VALUES (1,'cadastrar'),(2,'editar'),(3,'excluir'),(4,'listar'),(5,'gerar-relatorio');
/*!40000 ALTER TABLE `tipo_acao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_conta`
--

DROP TABLE IF EXISTS `tipo_conta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_conta` (
  `id_tipo_conta` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_conta` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tipo_conta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_conta`
--

LOCK TABLES `tipo_conta` WRITE;
/*!40000 ALTER TABLE `tipo_conta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_conta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_historico`
--

DROP TABLE IF EXISTS `tipo_historico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_historico` (
  `id_tipo_historico` int(11) NOT NULL AUTO_INCREMENT,
  `no_tipo_historico` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_tipo_historico`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_historico`
--

LOCK TABLES `tipo_historico` WRITE;
/*!40000 ALTER TABLE `tipo_historico` DISABLE KEYS */;
INSERT INTO `tipo_historico` VALUES (1,'A. ENTRADAS - TOTAL',1),(2,'B. SALDO DO MÊS ANTERIOR',1),(3,'C. SAIDAS',1),(4,'D. SALDO FINAL',1);
/*!40000 ALTER TABLE `tipo_historico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_lancamento`
--

DROP TABLE IF EXISTS `tipo_lancamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_lancamento` (
  `id_tipo_lancamento` int(11) NOT NULL AUTO_INCREMENT,
  `no_tipo_lancamento` varchar(45) NOT NULL,
  `id_tipo_historico` int(11) NOT NULL,
  PRIMARY KEY (`id_tipo_lancamento`),
  KEY `fk_tipo_lancamento_tipo_historico1_idx` (`id_tipo_historico`),
  CONSTRAINT `fk_tipo_lancamento_tipo_historico1` FOREIGN KEY (`id_tipo_historico`) REFERENCES `tipo_historico` (`id_tipo_historico`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_lancamento`
--

LOCK TABLES `tipo_lancamento` WRITE;
/*!40000 ALTER TABLE `tipo_lancamento` DISABLE KEYS */;
INSERT INTO `tipo_lancamento` VALUES (1,'1. RECEITAS',1),(2,'2. CAIXA E BANCOS',2),(3,'3. DESPESAS',3),(4,'4. INVESTIMENTOS',3),(5,'5. CAIXA E BANCOS',4);
/*!40000 ALTER TABLE `tipo_lancamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_pagamento`
--

DROP TABLE IF EXISTS `tipo_pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_pagamento` (
  `id_tipo_pagamento` int(11) NOT NULL AUTO_INCREMENT,
  `no_tipo_pagamento` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tipo_pagamento`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_pagamento`
--

LOCK TABLES `tipo_pagamento` WRITE;
/*!40000 ALTER TABLE `tipo_pagamento` DISABLE KEYS */;
INSERT INTO `tipo_pagamento` VALUES (1,'Dinheiro'),(2,'Cheque'),(3,'Cartão Débito'),(4,'Cartao Crédito');
/*!40000 ALTER TABLE `tipo_pagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_registro_caixa`
--

DROP TABLE IF EXISTS `tipo_registro_caixa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_registro_caixa` (
  `id_tipo_registro_caixa` int(11) NOT NULL AUTO_INCREMENT,
  `no_tipo_registro_caixa` varchar(45) NOT NULL,
  `no_finalidade` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tipo_registro_caixa`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_registro_caixa`
--

LOCK TABLES `tipo_registro_caixa` WRITE;
/*!40000 ALTER TABLE `tipo_registro_caixa` DISABLE KEYS */;
INSERT INTO `tipo_registro_caixa` VALUES (1,'Entrada','Receita'),(2,'Saída','Despesas');
/*!40000 ALTER TABLE `tipo_registro_caixa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `dt_cadastro` date NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `no_usuario` varchar(100) NOT NULL,
  `no_senha` text NOT NULL,
  `dt_ult_visita` date DEFAULT NULL,
  `st_cookie` text,
  `id_pessoa` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `fk_usuario_pessoa1_idx` (`id_pessoa`),
  KEY `fk_usuario_perfil1_idx` (`id_perfil`),
  CONSTRAINT `fk_usuario_perfil1` FOREIGN KEY (`id_perfil`) REFERENCES `perfil` (`id_perfil`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_pessoa1` FOREIGN KEY (`id_pessoa`) REFERENCES `pessoa` (`id_pessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'2014-12-28',1,'geccal','$2y$11$R3J1cG8gRXNww61yaXRhI.yRc.6Ifny/cW3XBoGoOh4tKEbTqyRKS',NULL,'',1,1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-23 14:24:04
