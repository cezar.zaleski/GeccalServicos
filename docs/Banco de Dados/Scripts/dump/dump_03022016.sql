CREATE DATABASE  IF NOT EXISTS `geccal` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `geccal`;
-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: geccal
-- ------------------------------------------------------
-- Server version	5.5.46-0ubuntu0.14.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_conta`
--

DROP TABLE IF EXISTS `tb_conta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_conta` (
  `id_conta` int(11) NOT NULL AUTO_INCREMENT,
  `no_conta` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `id_tipo_conta` int(11) NOT NULL,
  PRIMARY KEY (`id_conta`),
  KEY `fk_conta_tipo_conta1_idx` (`id_tipo_conta`),
  CONSTRAINT `fk_conta_tipo_conta1` FOREIGN KEY (`id_tipo_conta`) REFERENCES `tb_tipo_conta` (`id_tipo_conta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_conta`
--

LOCK TABLES `tb_conta` WRITE;
/*!40000 ALTER TABLE `tb_conta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_conta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_desativacao_usuario`
--

DROP TABLE IF EXISTS `tb_desativacao_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_desativacao_usuario` (
  `id_desativacao_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `dt_desativacao` date NOT NULL,
  `no_motivo_desativacao` text NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `no_motivo_ativacao` text,
  `dt_ativacao` date DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_desativacao_usuario`),
  KEY `fk_desativacao_usuario_usuario1_idx` (`id_usuario`),
  CONSTRAINT `fk_desativacao_usuario_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_desativacao_usuario`
--

LOCK TABLES `tb_desativacao_usuario` WRITE;
/*!40000 ALTER TABLE `tb_desativacao_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_desativacao_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_descricao_lancamento`
--

DROP TABLE IF EXISTS `tb_descricao_lancamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_descricao_lancamento` (
  `id_desc_lancamento` int(11) NOT NULL AUTO_INCREMENT,
  `no_desc_lancamento` varchar(45) NOT NULL,
  `co_desc_lancamento` varchar(45) NOT NULL,
  `id_tipo_lancamento` int(11) NOT NULL,
  PRIMARY KEY (`id_desc_lancamento`),
  KEY `fk_descricao_lancamento_tipo_lancamento_idx` (`id_tipo_lancamento`),
  CONSTRAINT `fk_descricao_lancamento_tipo_lancamento` FOREIGN KEY (`id_tipo_lancamento`) REFERENCES `tb_tipo_lancamento` (`id_tipo_lancamento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_descricao_lancamento`
--

LOCK TABLES `tb_descricao_lancamento` WRITE;
/*!40000 ALTER TABLE `tb_descricao_lancamento` DISABLE KEYS */;
INSERT INTO `tb_descricao_lancamento` VALUES (1,'Mensalidade de Associados','1.1',1),(2,'Venda de Livros','1.2',1),(3,'Rendimentos de Aplicações Financeiras','1.3',1),(4,'Promoção Beneficiente','1.4',1),(5,'Donativos','1.5',1),(6,'Receita Bazar','1.6',1),(7,'Recuperação de Despesas','1.7',1),(8,'Doações de Gêneros Materiais','1.8',1),(9,'Souvenirs GECCAL 40 anos','1.9',1),(10,'Caixa','2.1',2),(11,'Banco do Brasil S/A - C/Corrente','2.2',2),(12,'Banco do Brasil S/A - C/Poupança','2.3',2),(13,'Água, Luz e Telefone','3.1',3),(14,'Despesas c/ Assistência Social','3.2',3),(15,'Aquisição de Livros p/ Venda','3.3',3),(16,'Salários, Gratificações, Férias e 13 Sal.','3.4',3),(17,'INSS, FGTS e outros encargos','3.5',3),(18,'Material de Limpeza e Higiene','3.6',3),(19,'Materiais de Escritório e Didáticos','3.7',3),(20,'Despesas Bancárias','3.8',3),(21,'Emolumentos e Taxas diversas','3.9',3),(22,'Manutenção e Reparos','3.10',3),(23,'Custos de Promoções Beneficentes','3.11',3),(24,'Contribuições Diversas','3.12',3),(25,'Despesas com Eventos Sociais','3.13',3),(26,'Despesas Seguro Predial','3.14',3),(27,'Móveis e Utensílios','4.1',4),(28,'Máquinas e Aparelhos','4.2',4),(29,'Instalações','4.3',4),(30,'Crédito Cielo','1.10',1);
/*!40000 ALTER TABLE `tb_descricao_lancamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_descricao_movimento_conta`
--

DROP TABLE IF EXISTS `tb_descricao_movimento_conta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_descricao_movimento_conta` (
  `id_desc_movimento_conta` int(11) NOT NULL AUTO_INCREMENT,
  `no_desc_movimentacao_conta` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_desc_movimento_conta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_descricao_movimento_conta`
--

LOCK TABLES `tb_descricao_movimento_conta` WRITE;
/*!40000 ALTER TABLE `tb_descricao_movimento_conta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_descricao_movimento_conta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_historico`
--

DROP TABLE IF EXISTS `tb_historico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_historico` (
  `id_historico` int(11) NOT NULL AUTO_INCREMENT,
  `dt_historico` date NOT NULL,
  `no_acao` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_historico`),
  KEY `fk_historico_usuario1_idx` (`id_usuario`),
  CONSTRAINT `fk_historico_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_historico`
--

LOCK TABLES `tb_historico` WRITE;
/*!40000 ALTER TABLE `tb_historico` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_historico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_movimentacao_conta`
--

DROP TABLE IF EXISTS `tb_movimentacao_conta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_movimentacao_conta` (
  `id_movimentacao_conta` int(11) NOT NULL AUTO_INCREMENT,
  `nu_mes` int(11) NOT NULL,
  `nu_ano` int(11) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `id_conta` int(11) NOT NULL,
  `id_desc_movimento_conta` int(11) NOT NULL,
  PRIMARY KEY (`id_movimentacao_conta`),
  KEY `fk_movimentacao_conta_conta1_idx` (`id_conta`),
  KEY `fk_movimentacao_conta_descricao_movimento_conta1_idx` (`id_desc_movimento_conta`),
  CONSTRAINT `fk_movimentacao_conta_conta1` FOREIGN KEY (`id_conta`) REFERENCES `tb_conta` (`id_conta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_movimentacao_conta_descricao_movimento_conta1` FOREIGN KEY (`id_desc_movimento_conta`) REFERENCES `tb_descricao_movimento_conta` (`id_desc_movimento_conta`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_movimentacao_conta`
--

LOCK TABLES `tb_movimentacao_conta` WRITE;
/*!40000 ALTER TABLE `tb_movimentacao_conta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_movimentacao_conta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pagamento`
--

DROP TABLE IF EXISTS `tb_pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pagamento` (
  `id_pagamento` int(11) NOT NULL AUTO_INCREMENT,
  `nu_valor` double NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `id_tipo_pagamento` int(11) NOT NULL,
  `id_registro_caixa` int(11) NOT NULL,
  PRIMARY KEY (`id_pagamento`),
  KEY `fk_forma_pagamento_tipo_pagamento1_idx` (`id_tipo_pagamento`),
  KEY `fk_forma_pagamento_registro_caixa1_idx` (`id_registro_caixa`),
  CONSTRAINT `fk_forma_pagamento_registro_caixa1` FOREIGN KEY (`id_registro_caixa`) REFERENCES `tb_registro_caixa` (`id_registro_caixa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_forma_pagamento_tipo_pagamento1` FOREIGN KEY (`id_tipo_pagamento`) REFERENCES `tb_tipo_pagamento` (`id_tipo_pagamento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pagamento`
--

LOCK TABLES `tb_pagamento` WRITE;
/*!40000 ALTER TABLE `tb_pagamento` DISABLE KEYS */;
INSERT INTO `tb_pagamento` VALUES (1,5845.63,1,2,1),(3,111.11,1,1,1),(4,250,1,2,4),(5,100,1,1,7),(6,111.11,1,1,8);
/*!40000 ALTER TABLE `tb_pagamento` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER geccal.tg_ins_pagamento AFTER INSERT
ON geccal.tb_pagamento
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_pagamento (id_pagamento, nu_valor, st_ativo, id_tipo_pagamento, id_registro_caixa,  tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.id_pagamento, NEW.nu_valor, NEW.st_ativo, NEW.id_tipo_pagamento, NEW.id_registro_caixa,
            'I', sysdate(), session_user());
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER geccal.tg_upd_pagamento AFTER UPDATE
ON geccal.tb_pagamento
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_pagamento (id_pagamento, nu_valor, st_ativo, id_tipo_pagamento, id_registro_caixa,  tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.id_pagamento, NEW.nu_valor, NEW.st_ativo, NEW.id_tipo_pagamento, NEW.id_registro_caixa,
            'U', sysdate(), session_user());
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER geccal.tg_del_pagamento AFTER DELETE
ON geccal.tb_pagamento
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_pagamento (id_pagamento, nu_valor, st_ativo, id_tipo_pagamento, id_registro_caixa,  tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (OLD.id_pagamento, OLD.nu_valor, OLD.st_ativo, OLD.id_tipo_pagamento, OLD.id_registro_caixa,
            'D', sysdate(), session_user());
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_perfil`
--

DROP TABLE IF EXISTS `tb_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_perfil` (
  `id_perfil` int(11) NOT NULL AUTO_INCREMENT,
  `no_perfil` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_perfil`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_perfil`
--

LOCK TABLES `tb_perfil` WRITE;
/*!40000 ALTER TABLE `tb_perfil` DISABLE KEYS */;
INSERT INTO `tb_perfil` VALUES (1,'Master',1),(2,'Teste',1),(3,'asdd',1);
/*!40000 ALTER TABLE `tb_perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_pessoa`
--

DROP TABLE IF EXISTS `tb_pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_pessoa` (
  `id_pessoa` int(11) NOT NULL AUTO_INCREMENT,
  `no_pessoa` varchar(100) NOT NULL,
  `dt_cadastro` date NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `no_endereco` text,
  `no_bairro` varchar(45) DEFAULT NULL,
  `nu_cep` int(11) DEFAULT NULL,
  `no_cidade` varchar(45) DEFAULT NULL,
  `no_email` text,
  PRIMARY KEY (`id_pessoa`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_pessoa`
--

LOCK TABLES `tb_pessoa` WRITE;
/*!40000 ALTER TABLE `tb_pessoa` DISABLE KEYS */;
INSERT INTO `tb_pessoa` VALUES (1,'Geccal','2014-12-27',1,'','',0,'',NULL);
/*!40000 ALTER TABLE `tb_pessoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_registro_caixa`
--

DROP TABLE IF EXISTS `tb_registro_caixa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_registro_caixa` (
  `id_registro_caixa` int(11) NOT NULL AUTO_INCREMENT,
  `dt_registro` date DEFAULT NULL,
  `no_descricao` text,
  `nu_valor` double DEFAULT NULL,
  `id_desc_lancamento` int(11) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `id_tipo_registro_caixa` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_registro_caixa`),
  KEY `fk_registro_caixa_descricao_lancamento1_idx` (`id_desc_lancamento`),
  KEY `fk_registro_caixa_usuario1_idx` (`id_usuario`),
  KEY `fk_registro_caixa_tipo_registro_caixa1_idx` (`id_tipo_registro_caixa`),
  CONSTRAINT `fk_registro_caixa_descricao_lancamento1` FOREIGN KEY (`id_desc_lancamento`) REFERENCES `tb_descricao_lancamento` (`id_desc_lancamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_caixa_tipo_registro_caixa1` FOREIGN KEY (`id_tipo_registro_caixa`) REFERENCES `tb_tipo_registro_caixa` (`id_tipo_registro_caixa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_caixa_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_registro_caixa`
--

LOCK TABLES `tb_registro_caixa` WRITE;
/*!40000 ALTER TABLE `tb_registro_caixa` DISABLE KEYS */;
INSERT INTO `tb_registro_caixa` VALUES (1,'2016-01-14','venda edição teste',NULL,30,1,1,1),(4,'2016-01-20','Venda teste',NULL,2,1,1,2),(7,NULL,NULL,NULL,NULL,1,0,NULL),(8,NULL,NULL,NULL,NULL,1,0,NULL);
/*!40000 ALTER TABLE `tb_registro_caixa` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER geccal.tg_ins_registro_caixa AFTER INSERT
ON geccal.tb_registro_caixa
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_registro_caixa (id_registro_caixa, dt_registro, no_descricao,nu_valor, id_desc_lancamento, id_usuario,
                                                    st_ativo, id_tipo_registro_caixa, tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.id_registro_caixa, NEW.dt_registro, NEW.no_descricao, NEW.nu_valor, NEW.id_desc_lancamento,
            NEW.id_usuario, NEW.st_ativo, NEW.id_tipo_registro_caixa,
            'I', sysdate(), session_user());
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER geccal.tg_upd_registro_caixa AFTER UPDATE
ON geccal.tb_registro_caixa
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_registro_caixa (id_registro_caixa, dt_registro, no_descricao,nu_valor, id_desc_lancamento, id_usuario,
                                                    st_ativo, id_tipo_registro_caixa, tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.id_registro_caixa, NEW.dt_registro, NEW.no_descricao, NEW.nu_valor, NEW.id_desc_lancamento,
            NEW.id_usuario, NEW.st_ativo, NEW.id_tipo_registro_caixa,
            'U', sysdate(), session_user());
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER geccal.tg_del_registro_caixa AFTER DELETE
ON geccal.tb_registro_caixa
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_registro_caixa (id_registro_caixa, dt_registro, no_descricao,nu_valor, id_desc_lancamento, id_usuario,
                                                    st_ativo, id_tipo_registro_caixa, tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (OLD.id_registro_caixa, OLD.dt_registro, OLD.no_descricao, OLD.nu_valor, OLD.id_desc_lancamento,
            OLD.id_usuario, OLD.st_ativo, OLD.id_tipo_registro_caixa,
            'D', sysdate(), session_user());
  END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tb_telefone`
--

DROP TABLE IF EXISTS `tb_telefone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_telefone` (
  `id_telefone` int(11) NOT NULL AUTO_INCREMENT,
  `nu_telefone` int(11) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `id_pessoa` int(11) NOT NULL,
  PRIMARY KEY (`id_telefone`),
  KEY `fk_telefone_pessoa1_idx` (`id_pessoa`),
  CONSTRAINT `fk_telefone_pessoa1` FOREIGN KEY (`id_pessoa`) REFERENCES `tb_pessoa` (`id_pessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_telefone`
--

LOCK TABLES `tb_telefone` WRITE;
/*!40000 ALTER TABLE `tb_telefone` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_telefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_tipo_conta`
--

DROP TABLE IF EXISTS `tb_tipo_conta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_tipo_conta` (
  `id_tipo_conta` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_conta` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tipo_conta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_tipo_conta`
--

LOCK TABLES `tb_tipo_conta` WRITE;
/*!40000 ALTER TABLE `tb_tipo_conta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_tipo_conta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_tipo_historico`
--

DROP TABLE IF EXISTS `tb_tipo_historico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_tipo_historico` (
  `id_tipo_historico` int(11) NOT NULL AUTO_INCREMENT,
  `no_tipo_historico` varchar(45) NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_tipo_historico`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_tipo_historico`
--

LOCK TABLES `tb_tipo_historico` WRITE;
/*!40000 ALTER TABLE `tb_tipo_historico` DISABLE KEYS */;
INSERT INTO `tb_tipo_historico` VALUES (1,'A. ENTRADAS - TOTAL',1),(2,'B. SALDO DO MÊS ANTERIOR',1),(3,'C. SAIDAS',1),(4,'D. SALDO FINAL',1);
/*!40000 ALTER TABLE `tb_tipo_historico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_tipo_lancamento`
--

DROP TABLE IF EXISTS `tb_tipo_lancamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_tipo_lancamento` (
  `id_tipo_lancamento` int(11) NOT NULL AUTO_INCREMENT,
  `no_tipo_lancamento` varchar(45) NOT NULL,
  `id_tipo_historico` int(11) NOT NULL,
  PRIMARY KEY (`id_tipo_lancamento`),
  KEY `fk_tipo_lancamento_tipo_historico1_idx` (`id_tipo_historico`),
  CONSTRAINT `fk_tipo_lancamento_tipo_historico1` FOREIGN KEY (`id_tipo_historico`) REFERENCES `tb_tipo_historico` (`id_tipo_historico`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_tipo_lancamento`
--

LOCK TABLES `tb_tipo_lancamento` WRITE;
/*!40000 ALTER TABLE `tb_tipo_lancamento` DISABLE KEYS */;
INSERT INTO `tb_tipo_lancamento` VALUES (1,'1. RECEITAS',1),(2,'2. CAIXA E BANCOS',2),(3,'3. DESPESAS',3),(4,'4. INVESTIMENTOS',3),(5,'5. CAIXA E BANCOS',4);
/*!40000 ALTER TABLE `tb_tipo_lancamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_tipo_pagamento`
--

DROP TABLE IF EXISTS `tb_tipo_pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_tipo_pagamento` (
  `id_tipo_pagamento` int(11) NOT NULL AUTO_INCREMENT,
  `no_tipo_pagamento` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tipo_pagamento`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_tipo_pagamento`
--

LOCK TABLES `tb_tipo_pagamento` WRITE;
/*!40000 ALTER TABLE `tb_tipo_pagamento` DISABLE KEYS */;
INSERT INTO `tb_tipo_pagamento` VALUES (1,'Dinheiro'),(2,'Cheque'),(3,'Cartão Débito'),(4,'Cartao Crédito');
/*!40000 ALTER TABLE `tb_tipo_pagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_tipo_registro_caixa`
--

DROP TABLE IF EXISTS `tb_tipo_registro_caixa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_tipo_registro_caixa` (
  `id_tipo_registro_caixa` int(11) NOT NULL AUTO_INCREMENT,
  `no_tipo_registro_caixa` varchar(45) NOT NULL,
  `no_finalidade` varchar(45) NOT NULL,
  PRIMARY KEY (`id_tipo_registro_caixa`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_tipo_registro_caixa`
--

LOCK TABLES `tb_tipo_registro_caixa` WRITE;
/*!40000 ALTER TABLE `tb_tipo_registro_caixa` DISABLE KEYS */;
INSERT INTO `tb_tipo_registro_caixa` VALUES (1,'Entrada','Receita'),(2,'Saída','Despesas');
/*!40000 ALTER TABLE `tb_tipo_registro_caixa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_usuario`
--

DROP TABLE IF EXISTS `tb_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `dt_cadastro` date NOT NULL,
  `st_ativo` tinyint(1) NOT NULL,
  `no_usuario` varchar(100) NOT NULL,
  `no_senha` text NOT NULL,
  `dt_ult_visita` date DEFAULT NULL,
  `st_cookie` text,
  `id_pessoa` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `fk_usuario_pessoa1_idx` (`id_pessoa`),
  CONSTRAINT `fk_usuario_pessoa1` FOREIGN KEY (`id_pessoa`) REFERENCES `tb_pessoa` (`id_pessoa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_usuario`
--

LOCK TABLES `tb_usuario` WRITE;
/*!40000 ALTER TABLE `tb_usuario` DISABLE KEYS */;
INSERT INTO `tb_usuario` VALUES (1,'2014-12-28',1,'geccal','$2y$11$R3J1cG8gRXNww61yaXRhI.yRc.6Ifny/cW3XBoGoOh4tKEbTqyRKS',NULL,'',1);
/*!40000 ALTER TABLE `tb_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_usuario_perfil`
--

DROP TABLE IF EXISTS `tb_usuario_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_usuario_perfil` (
  `id_usuario` int(11) NOT NULL,
  `id_perfil` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`,`id_perfil`),
  KEY `fk_usuario_has_perfil_perfil1_idx` (`id_perfil`),
  KEY `fk_usuario_has_perfil_usuario1_idx` (`id_usuario`),
  CONSTRAINT `fk_usuario_has_perfil_perfil1` FOREIGN KEY (`id_perfil`) REFERENCES `tb_perfil` (`id_perfil`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_has_perfil_usuario1` FOREIGN KEY (`id_usuario`) REFERENCES `tb_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_usuario_perfil`
--

LOCK TABLES `tb_usuario_perfil` WRITE;
/*!40000 ALTER TABLE `tb_usuario_perfil` DISABLE KEYS */;
INSERT INTO `tb_usuario_perfil` VALUES (1,1);
/*!40000 ALTER TABLE `tb_usuario_perfil` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-03 22:08:39
