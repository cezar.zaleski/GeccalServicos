CREATE OR REPLACE FUNCTION auditoria.fn_fies_auditoria_procedure_function(p_no_servidor varchar, p_no_database varchar)
  RETURNS void
AS
$BODY$
  /***********************************************************************************************************
*
* Função utilizada para catalogar as alterações realizadas nas funções e procedures do banco dados,
* passados como parâmetro.
* Os dados catalogados são gravados na tabela de auditoria: AUDITORIA.TB_AUDITORIA_PROCEDURE_FUNCTION
*
************************************************************************************************************/


begin

    insert into auditoria.tb_auditoria_procedure_function  
    
    select  DISTINCT 
    p_no_servidor as no_servidor ,  
            p_no_database as no_database , 
            procedure.nspname,
            procedure.oid, 
            procedure.proname, 
            ds_criacao || procedure.prosrc, 
            now()  
            
    from 
    
        auditoria.tb_auditoria_procedure_function historico  
        
        inner join (select historico.no_servidor, 
                           historico.no_database,  
                           historico.no_schema,
                           historico.co_objeto, 
                           historico.no_objeto, 
                           max(historico.dt_objeto) as dt_objeto  
                           
                    from  
                    
                        auditoria.tb_auditoria_procedure_function historico  
                        
                    group by  
                    
                        historico.no_servidor, historico.no_database, historico.no_schema, historico.co_objeto, historico.no_objeto) as ultima_alteracao
                       
              on historico.no_servidor = ultima_alteracao.no_servidor and  
              
                 historico.no_database = ultima_alteracao.no_database and  
                 
                 historico.no_schema   = ultima_alteracao.no_schema   and
                 
                 historico.co_objeto   = ultima_alteracao.co_objeto   and
                   
                 historico.no_objeto   = ultima_alteracao.no_objeto  
                 
        right join (    select distinct ns.nspname, proc.oid, proc.proname, proc.prosrc ,
                                        'create or replace ' || case
                                                                  when protype = 0 then 'FUNCTION '
                                                                  else 'PROCEDURE '
                                                                end   || ns.nspname || '.' || proname || ' ('||
                                        coalesce(array_to_string(array(select proargnames[contador+1] || ' ' || format_type(proargtypes[contador], null)
                                                                       from generate_series(array_lower(proargtypes,1), array_upper(proargtypes,1)) contador),','), '') || ') as ' as ds_criacao

                        from 
                        
                             pg_proc proc
                             
                             inner join pg_namespace ns
                               on proc.pronamespace = ns.oid 
                               
                        where
                        
                           ns.nspname not like 'dbms%' and
                           
                           ns.nspname not like 'utl%' and
                           
                           ns.nspname not in ('sys', 'pg_catalog')


                        Union


                        select distinct vw.schemaname, cl.oid, vw.viewname, vw.definition ,
                                        'create or replace VIEW ' || vw.schemaname || '.' || vw.viewname || ' as '|| vw.definition as ds_criacao

                        from 
                        
                             pg_views vw

                             inner join pg_namespace ns
                                on vw.schemaname = ns.nspname

                             inner join pg_class cl
                                on cl.relname      = vw.viewname and
                                cl.relnamespace = ns.oid                             
                               
                        where
                          
                           vw.schemaname not in ('dbo', 'information_schema', 'pg_catalog', 'sys')

                           ) as procedure 
                           
              on historico.co_objeto = procedure.oid

    where
    
     (historico.no_servidor =  p_no_servidor and  
     
      historico.no_database =  p_no_database and  
      
      historico.dt_objeto   = ultima_alteracao.dt_objeto and  
      
      historico.ds_objeto <> (ds_criacao || procedure.prosrc)) or  
      
      (historico.co_objeto is null ) ;


end
$BODY$
LANGUAGE plpgsql VOLATILE;
