/**
CRIAÇÃO BANCO PARA AUDITORIA
**/
DROP IF EXISTS geccal_auditoria;
CREATE DATABASE geccal_auditoria;


/**
CRIAÇÃO TABELA AUDITORIA TB_PAGAMENTO
*/

DROP TABLE IF EXISTS geccal_auditoria.lg_pagamento;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE geccal_auditoria.lg_pagamento (
  id_pagamento int(11) NOT NULL,
  nu_valor double NOT NULL,
  st_ativo tinyint(1) NOT NULL,
  id_tipo_pagamento int(11) NOT NULL,
  id_registro_caixa int(11) NOT NULL,
  tp_operacao_log CHAR(1) NOT NULL,
  dt_operacao_log TIMESTAMP NOT NULL,
  no_usuario_banco_log VARCHAR(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

/**
TRIGUER INSERT
**/
DROP TRIGGER geccal.tg_ins_pagamento;
DELIMITER $$
CREATE TRIGGER geccal.tg_ins_pagamento AFTER INSERT
ON geccal.tb_pagamento
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_pagamento (id_pagamento, nu_valor, st_ativo, id_tipo_pagamento, id_registro_caixa,  tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.id_pagamento, NEW.nu_valor, NEW.st_ativo, NEW.id_tipo_pagamento, NEW.id_registro_caixa,
            'I', sysdate(), session_user());
  END$$

DELIMITER ;

/**
TRIGUER UPDATE
**/
DROP TRIGGER geccal.tg_upd_pagamento;
DELIMITER $$
CREATE TRIGGER geccal.tg_upd_pagamento AFTER UPDATE
ON geccal.tb_pagamento
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_pagamento (id_pagamento, nu_valor, st_ativo, id_tipo_pagamento, id_registro_caixa,  tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.id_pagamento, NEW.nu_valor, NEW.st_ativo, NEW.id_tipo_pagamento, NEW.id_registro_caixa,
            'U', sysdate(), session_user());
  END$$

DELIMITER ;

/**
TRIGUER DELETE
**/
DROP TRIGGER geccal.tg_del_pagamento;
DELIMITER $$
CREATE TRIGGER geccal.tg_del_pagamento AFTER DELETE
ON geccal.tb_pagamento
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_pagamento (id_pagamento, nu_valor, st_ativo, id_tipo_pagamento, id_registro_caixa,  tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (OLD.id_pagamento, OLD.nu_valor, OLD.st_ativo, OLD.id_tipo_pagamento, OLD.id_registro_caixa,
            'D', sysdate(), session_user());
  END$$

DELIMITER ;


-------------------------------------------------------------
/**
CRIAÇÃO TABELA AUDITORIA TB_REGISTRO_CAIXA
*/
DROP TABLE IF EXISTS geccal_auditoria.lg_registro_caixa;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE geccal_auditoria.lg_registro_caixa (
  id_registro_caixa int(11) NOT NULL,
  dt_registro date,
  no_descricao varchar(100),
  nu_valor double,
  st_ativo tinyint(1) NOT NULL,
  id_desc_lancamento int(11),
  id_tipo_registro_caixa int(11),
  id_usuario int(11) NOT NULL,
  tp_operacao_log CHAR(1) NOT NULL,
  dt_operacao_log TIMESTAMP NOT NULL,
  no_usuario_banco_log VARCHAR(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;


/**
TRIGUER INSERT
**/
DROP TRIGGER geccal.tg_ins_registro_caixa;
DELIMITER $$
CREATE TRIGGER geccal.tg_ins_registro_caixa AFTER INSERT
ON geccal.tb_registro_caixa
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_registro_caixa (id_registro_caixa, dt_registro, no_descricao,nu_valor, id_desc_lancamento, id_usuario,
                                                    st_ativo, id_tipo_registro_caixa, tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.id_registro_caixa, NEW.dt_registro, NEW.no_descricao, NEW.nu_valor, NEW.id_desc_lancamento,
            NEW.id_usuario, NEW.st_ativo, NEW.id_tipo_registro_caixa,
            'I', sysdate(), session_user());
  END$$

DELIMITER ;

/**
TRIGUER UPDATE
**/
DROP TRIGGER geccal.tg_upd_registro_caixa;
DELIMITER $$
CREATE TRIGGER geccal.tg_upd_registro_caixa AFTER UPDATE 
ON geccal.tb_registro_caixa
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_registro_caixa (id_registro_caixa, dt_registro, no_descricao,nu_valor, id_desc_lancamento, id_usuario,
                                                    st_ativo, id_tipo_registro_caixa, tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.id_registro_caixa, NEW.dt_registro, NEW.no_descricao, NEW.nu_valor, NEW.id_desc_lancamento,
            NEW.id_usuario, NEW.st_ativo, NEW.id_tipo_registro_caixa,
            'U', sysdate(), session_user());
  END$$

DELIMITER ;


/**
TRIGUER DELETE
**/
DROP TRIGGER geccal.tg_del_registro_caixa;
DELIMITER $$
CREATE TRIGGER geccal.tg_del_registro_caixa AFTER DELETE 
ON geccal.tb_registro_caixa
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_registro_caixa (id_registro_caixa, dt_registro, no_descricao,nu_valor, id_desc_lancamento, id_usuario,
                                                    st_ativo, id_tipo_registro_caixa, tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (OLD.id_registro_caixa, OLD.dt_registro, OLD.no_descricao, OLD.nu_valor, OLD.id_desc_lancamento,
            OLD.id_usuario, OLD.st_ativo, OLD.id_tipo_registro_caixa,
            'D', sysdate(), session_user());
  END$$

DELIMITER ;
drop table geccal_auditoria.lg_movimentacao_conta;

drop table geccal_auditoria.lg_movimentacao_conta;
# auditoria movimentacao_conta
CREATE TABLE geccal_auditoria.lg_movimentacao_conta (
  id_movimentacao_conta int(11),
  nu_ano_mes int(11),
  st_ativo tinyint(1),
  id_conta int(11),
  tp_operacao_log CHAR(1) NOT NULL,
  dt_operacao_log TIMESTAMP NOT NULL,
  no_usuario_banco_log VARCHAR(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/**
TRIGUER INSERT
**/
DROP TRIGGER geccal.tg_ins_movimentacao_conta;
DELIMITER $$
CREATE TRIGGER geccal.tg_ins_movimentacao_conta AFTER INSERT
ON geccal.tb_movimentacao_conta
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_movimentacao_conta
    (
      id_movimentacao_conta, nu_ano_mes, st_ativo, id_conta,
      tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.id_movimentacao_conta, NEW.nu_ano_mes, NEW.st_ativo, NEW.id_conta,
            'I', sysdate(), session_user());
  END$$

DELIMITER ;


/**
TRIGUER UPDATE
**/
DROP TRIGGER geccal.tg_upd_movimentacao_conta;
DELIMITER $$
CREATE TRIGGER geccal.tg_upd_movimentacao_conta AFTER UPDATE
ON geccal.tb_movimentacao_conta
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_movimentacao_conta
    (
      id_movimentacao_conta, nu_ano_mes, st_ativo, id_conta,
      tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (NEW.id_movimentacao_conta, NEW.nu_ano_mes, NEW.st_ativo, NEW.id_conta,
            'U', sysdate(), session_user());
  END$$

DELIMITER ;

/**
TRIGUER DELETE
**/
DROP TRIGGER geccal.tg_del_movimentacao_conta;
DELIMITER $$
CREATE TRIGGER geccal.tg_del_movimentacao_conta AFTER DELETE
ON geccal.tb_movimentacao_conta
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_movimentacao_conta
    (
      id_movimentacao_conta, nu_ano_mes, st_ativo, id_conta,
      tp_operacao_log, dt_operacao_log, no_usuario_banco_log)
    VALUES (OLD.id_movimentacao_conta, OLD.nu_ano_mes, OLD.st_ativo, OLD.id_conta,
            'D', sysdate(), session_user());
  END$$

DELIMITER ;

# auditoria movimentacao_conta_descricao
CREATE TABLE geccal_auditoria.lg_movimentacao_conta_descricao (
  id_mov_conta_descricao int(11),
  id_movimentacao_conta int(11),
  id_desc_movimento_conta int(11),
  id_tipo_registro_caixa int(11),
  nu_valor float,
  nu_cheque int(11),
  ds_transferencia varchar(200),
  ds_debito varchar(200),
  tp_operacao_log CHAR(1) NOT NULL,
  dt_operacao_log TIMESTAMP NOT NULL,
  no_usuario_banco_log VARCHAR(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

# triguer insert
DROP TRIGGER geccal.tg_ins_movimentacao_conta_descricao;
DELIMITER $$
CREATE TRIGGER geccal.tg_ins_movimentacao_conta_descricao AFTER INSERT
ON geccal.tb_movimentacao_conta_descricao
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_movimentacao_conta_descricao
    (
      id_mov_conta_descricao, id_movimentacao_conta, id_desc_movimento_conta,
      id_tipo_registro_caixa, nu_valor,
      nu_cheque, ds_transferencia,
      ds_debito,  tp_operacao_log,
      dt_operacao_log, no_usuario_banco_log
    )
    VALUES (NEW.id_mov_conta_descricao, NEW.id_movimentacao_conta, NEW.id_desc_movimento_conta,
            NEW.id_tipo_registro_caixa, NEW.nu_valor, NEW.nu_cheque, NEW.ds_transferencia,
            NEW.ds_debito,
            'I', sysdate(), session_user());
  END$$

DELIMITER ;

# triguer update
DROP TRIGGER geccal.tg_upd_movimentacao_conta_descricao;
DELIMITER $$
CREATE TRIGGER geccal.tg_upd_movimentacao_conta_descricao AFTER UPDATE
ON geccal.tb_movimentacao_conta_descricao
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_movimentacao_conta_descricao
    (
      id_mov_conta_descricao, id_movimentacao_conta, id_desc_movimento_conta,
      id_tipo_registro_caixa, nu_valor,
      nu_cheque, ds_transferencia,
      ds_debito,  tp_operacao_log,
      dt_operacao_log, no_usuario_banco_log
    )
    VALUES (NEW.id_mov_conta_descricao, NEW.id_movimentacao_conta, NEW.id_desc_movimento_conta,
            NEW.id_tipo_registro_caixa, NEW.nu_valor, NEW.nu_cheque, NEW.ds_transferencia,
            NEW.ds_debito,
            'U', sysdate(), session_user());
  END$$

DELIMITER ;

# triguer delete
DROP TRIGGER geccal.tg_del_movimentacao_conta_descricao;
DELIMITER $$
CREATE TRIGGER geccal.tg_del_movimentacao_conta_descricao AFTER DELETE
ON geccal.tb_movimentacao_conta_descricao
FOR EACH ROW
  BEGIN
    INSERT INTO geccal_auditoria.lg_movimentacao_conta_descricao
    (
      id_mov_conta_descricao, id_movimentacao_conta, id_desc_movimento_conta,
      id_tipo_registro_caixa, nu_valor,
      nu_cheque, ds_transferencia,
      ds_debito,  tp_operacao_log,
      dt_operacao_log, no_usuario_banco_log
    )
    VALUES (OLD.id_mov_conta_descricao, OLD.id_movimentacao_conta, OLD.id_desc_movimento_conta,
            OLD.id_tipo_registro_caixa, OLD.nu_valor, OLD.nu_cheque, OLD.ds_transferencia,
            OLD.ds_debito,
            'D', sysdate(), session_user());
  END$$

DELIMITER ;