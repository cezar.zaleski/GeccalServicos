<?php

namespace Inscricao\Form;

use Zend\Form\Form;

class QuestionarioForm extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct('questionario-form');
        $this->setInputFilter(new QuestionarioFilter());
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'coRacaCor',
            'type' => 'select',
            'attributes' => array(
                'class' => 'form-control required-input',
                'id' => 'coRacaCor',
            ),
            'options' => array(
                'label' => 'Raça/Cor ',
                'value_options' => $options['comboRacaCor'],
                'disable_inarray_validator' => true,
                'empty_option' => 'Selecione',
            ),
        ));

        $this->add(array(
            'name' => 'stPossuiDeficiencia',
            'type' => 'Zend\Form\Element\Radio',
            'options' => array(
                'disable_inarray_validator' => true,
                'value_options' => array(
                    'Sim' => array(
                        'label' => ' Sim',
                        'value' => 'S',
                        'attributes' => array(
                            'class' => 'radio-inline stPossuiDeficiencia required-input',
                            'id' => 'stPossuiDeficienciaSim'
                        ),
                    ),
                    'Nao' => array(
                        'label' => ' Não',
                        'value' => 'N',
                        'attributes' => array(
                            'class' => 'radio-inline stPossuiDeficiencia required-input',
                            'id' => 'stPossuiDeficienciaNao'
                        ),
                    ),
                ),
                'label' => 'Pessoa com deficiência?',
                'label_attributes' => array(
                    'style' => 'padding-right:20px; font-weight: normal;',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'coDeficiencia',
            'type' => 'select',
            'attributes' => array(
                'class' => 'form-control required-input',
                'id' => 'coDeficiencia',
            ),
            'options' => array(
                'label' => 'Tipo de deficiência: ',
                'label_attributes' => array(
                    'id' => 'comboDeficiencia',
                ),
                'value_options' => $options['comboDeficiencia'],
                'empty_option' => 'Selecione',
                'disable_inarray_validator' => true,
            ),
        ));

        $this->add(array(
            'name' => 'stEscolaPublica',
            'type' => 'Zend\Form\Element\Radio',
            'attributes' => array(
                'class' => 'form-horizontal radio required-input',
                'id' => 'stEscolaPublica',
            ),
            'options' => array(
                'disable_inarray_validator' => true,
                'value_options' => array(
                    'Sim' => array(
                        'label' => ' Cursei o ensino médio COMPLETO em escola da rede pública.',
                        'value' => 'S',
                        'attributes' => array(
                            'class' => 'radio-inline  required-input',
                        ),
                    ),
                    'Parcial' => array(
                        'label' => ' Cursei o ensino médio PARCIALMENTE em escola da rede pública.',
                        'value' => 'P',
                        'attributes' => array(
                            'class' => 'radio-inline',
                        ),
                    ),
                    'Nao' => array(
                        'label' => ' NÃO cursei o ensino médio em escola da rede pública.',
                        'value' => 'N',
                        'attributes' => array(
                            'class' => 'radio-inline',
                        ),
                    ),
                ),
                'label' => 'Cursou o ensino médio em escola da rede pública?',
                'label_attributes' => array(
                    'style' => 'padding-right:20px; font-weight: normal; width: 100%;',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'stEnsinoSuperior',
            'type' => 'Zend\Form\Element\Radio',
            'attributes' => array(
                'class' => 'form-control radio-inline'
            ),
            'options' => array(
                'disable_inarray_validator' => true,
                'label' => 'Concluiu curso superior?',
                'label_attributes' => array(
                    'style' => 'padding-right:20px; font-weight: normal;',
                ),
                'value_options' => array(
                    'Sim' => array(
                        'label' => ' Sim',
                        'value' => 'S',
                        'attributes' => array(
                            'class' => 'radio-inline required-input',
                        ),
                    ),
                    'Nao' => array(
                        'label' => ' Não',
                        'value' => 'N',
                        'attributes' => array(
                            'class' => 'radio-inline',
                        ),
                    ),
                ),
            ),
        ));

        $this->add(array(
            'name' => 'nuAnoConclusaoEnsMedio',
            'type' => 'select',
            'attributes' => array(
                'class' => 'form-control required-input',
                'id' => 'nuAnoConclusaoEnsMedio',
            ),
            'options' => array(
                'value_options' => $options['comboAnoEnsinoMedio'],
                'disable_inarray_validator' => true,
                'empty_option' => 'Selecione',
                'label' => 'Ano de conclusão do ensino médio '
             ),
        ));

        $this->add(array(
            'name' => 'stDeclaracaoProfessor',
            'type' => 'radio',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'stDeclaracaoProfessor',
            ),
            'options' => array(
                'value_options' => array(
                    'Sim' => array(
                        'label' => ' Sim',
                        'value' => 'S',
                        'attributes' => array(
                            'class' => 'radio-inline stDeclaracaoProfessor required-input',
                            'id' => 'stDeclaracaoProfessorSim'
                        ),
                    ),
                    'Nao' => array(
                        'label' => ' Não',
                        'value' => 'N',
                        'attributes' => array(
                            'class' => 'radio-inline stDeclaracaoProfessor required-input',
                            'id' => 'stDeclaracaoProfessorNao'
                        ),
                    ),
                ),
                'disable_inarray_validator' => true,
                'label' => 'Professor do quadro permanente da rede pública de ensino, em efetivo exercício na educação básica?',
                'label_attributes' => array(
                    'style' => 'padding-right:20px; font-weight: normal;',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'stConcorrerProfessor',
            'type' => 'checkbox',
            'attributes' => array(
                'id' => 'stConcorrerProfessor'
            ),
            'options' => array(
                'use_hidden_element' => true,
                'checked_value' => 'S',
                'unchecked_value' => 'N'
            ),
        ));

        $this->add(array(
            'name' => 'noEscola',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'noEscola',
                'maxlength' => 100,
            ),
            'options' => array(
                'label' => 'Nome da escola em que leciona ',
            )
        ));

        $this->add(array(
            'name' => 'coUf',
            'type' => 'select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'coUf',
            ),
            'options' => array(
                'value_options' => $options['comboUf'],
                'disable_inarray_validator' => true,
                'empty_option' => 'Selecione',
                'label' => 'UF '
            ),
        ));

        $this->add(array(
            'name' => 'coMunicipioEscola',
            'type' => 'select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'coMunicipioEscola',
            ),
            'options' => array(
                'disable_inarray_validator' => true,
                'empty_option' => 'Selecione',
                'label' => 'Município ',
            ),
        ));

        $this->add(array(
            'name' => 'stRedeEnsinoEscola',
            'type' => 'select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'stRedeEnsinoEscola',
            ),
            'options' => array(
                    'value_options' => array(
                        'F' => 'Federal',
                        'E' => 'Estadual',
                        'M' => 'Municipal',
                    ),
                'disable_inarray_validator' => true,
                'empty_option' => 'Selecione',
                'label' => 'Rede de ensino '
            ),
        ));

        $this->add(array(
            'name' => 'dsMatrInstProfEscola',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'dsMatrInstProfEscola',
                'maxlength' => 100,
            ),
            'options' => array(
                'label' => 'Matrícula institucional '
            )
        ));

        $this->add(array(
            'name' => 'dsAreaAtuacaoEscola',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'dsAreaAtuacaoEscola',
                'maxlength' => 100,
            ),
            'options' => array(
                'label' => 'Área de atuação '
            )
        ));

        $this->add(array(
            'name' => 'stInadimplenteCreduc',
            'type' => 'checkbox',
            'attributes' => array(
                'id' => 'creduc',
            ),
            'options' => array(
                'unchecked_value' => '',
            )
        ));

        $this->add(array(
            'name' => 'stConcordanciaRegras',
            'type' => 'checkbox',
            'attributes' => array(
                'id' => 'fies',
            ),
            'options' => array(
                'unchecked_value' => ''
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Gravar e avançar',
                'id' => 'submit',
                'class' => 'btn btn-success'
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'sec_questionario',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 1800
                )
            )
        ));
    }

}