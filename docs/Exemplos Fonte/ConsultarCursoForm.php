<?php

namespace Inscricao\Form;

use Zend\Form\Form;

class ConsultarCursoForm extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct('consultar-curso-form');
        $this->setInputFilter(new ConsultarCursoFilter());
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'opcaoProcurar',
            'type' => 'Zend\Form\Element\Radio',
            'attributes' => array(
                'class' => 'form-control radio required-input',
                'id' => 'opcaoProcurar',
                'value' => 'C',
                'class' => 'required',
            ),
            'options' => array(
                'disable_inarray_validator' => true,
                'id' => 'opcaoProcurar',
                'value_options' => array(
                    'Procurar por curso' => array(
                        'label' => 'Procurar por curso',
                        'value' => 'C',
                        'attributes' => array(
                        ),
                    ),
                    'Procurar por instituição' => array(
                        'label' => 'Procurar por instituição',
                        'value' => 'I',
                        'attributes' => array(
                        ),
                    ),
                    'Procurar por município' => array(
                        'label' => 'Procurar por município',
                        'value' => 'M',
                        'attributes' => array(
                        ),
                    ),
                ),
                'label_attributes' => array(
                    'class' => 'btn btn-default opcao',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'noCurso',
            'type' => 'text',
            'options' => array(
                'label' => 'Pesquisar'
            ),
            'attributes' => array(
                'class' => 'form-control typeahead',
                'id' => 'noCurso',
                'maxlength' => 100,
            ),
        ));
        $this->add(array(
            'name' => 'noIes',
            'type' => 'text',
            'options' => array(
                'label' => 'Pesquisar'
            ),
            'attributes' => array(
                'class' => 'form-control typeahead',
                'id' => 'noIes',
                'maxlength' => 100,
            ),
        ));
        $this->add(array(
            'name' => 'noMunicipio',
            'type' => 'text',
            'options' => array(
                'label' => 'Pesquisar'
            ),
            'attributes' => array(
                'class' => 'form-control typeahead',
                'id' => 'noMunicipio',
                'maxlength' => 100,
            ),
        ));
        $this->add(array(
            'name' => 'btnPesquisar',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Pesquisar',
                'class' => 'btn btn-success',
                'id' => 'btnPesquisar',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Csrf',
            'name' => 'sec_consultar_curso',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 1800
                )
            )
        ));
    }

}