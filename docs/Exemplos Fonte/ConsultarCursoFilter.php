<?php

namespace Inscricao\Form;

use Zend\InputFilter\InputFilter;

class ConsultarCursoFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'opcaoProcurar',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'MSG003'),
                    ),
                ),
                array(
                    'name' => 'Callback',
                    'options' => array(
                        'callback' => function($value) {
                            switch ($value) {
                                case "C":
                                    $this->get('noCurso')->setRequired(true);
                                    $this->get('noIes')->setRequired(false);
                                    $this->get('noMunicipio')->setRequired(false);
                                    break;
                                case "I":
                                    $this->get('noCurso')->setRequired(false);
                                    $this->get('noIes')->setRequired(true);
                                    $this->get('noMunicipio')->setRequired(false);
                                    break;
                                case "M":
                                    $this->get('noCurso')->setRequired(false);
                                    $this->get('noIes')->setRequired(false);
                                    $this->get('noMunicipio')->setRequired(true);
                                    break;
                            }
                            return true;
                        }
                    )
                )
            ),
        ));
        $this->add(array(
            'name' => 'noCurso',
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'MSG003')
                    )),
            )
        ));
        $this->add(array(
            'name' => 'noIes',
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'MSG003')
                    )),
            )
        ));
        $this->add(array(
            'name' => 'noMunicipio',
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'MSG003')
                    )),
            )
        ));


    }
}