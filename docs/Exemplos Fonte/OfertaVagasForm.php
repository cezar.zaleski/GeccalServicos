<?php

namespace Parametro\Form;

use Zend\Form\Form;

class OfertaVagasForm extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct('oferta-vagas', $options);

        $this->setInputFilter(new OfertaVagasFilter());
        $this->setAttribute('method', 'post');
        $this->setAttributes(array(
            'method' => 'post',
            'role' => 'form',
            'novalidate' => 'novalidate'
        ));

        $this->add(array(
            'name' => 'coSemestre',
            'type' => 'hidden',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'dtInicio',
                'maxlength' => 30,
            ),
        ));

        $this->add(array(
            'name' => 'dtInicioOfertaVagas',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'dtInicioOfertaVagas',
                'maxlength' => 30,
            ),
            'options' => array(
                'label' => 'Período para atualização dos cursos: '
            )
        ));

        $this->add(array(
            'name' => 'dtFimOfertaVagas',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'dtFimOfertaVagas',
                'maxlength' => 30,
            ),
            'options' => array(
                'label' => 'Período para atualização dos cursos: '
            )
        ));

        $this->add(array(
            'name' => 'coSemestre',
            'type' => 'select',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'coSemestre'
            ),
            'options' => array(
                'label' => 'Semestre/Ano referência: ',
                'disable_inarray_validator' => true,
                'empty_option' => 'Selecione',
                'value_options' => $options,
                'label_attributes' => array(
                    'class' =>'required'
                ),
            ),
        ));

        $this->add(array(
            'name' => 'dsObservacao',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'dsObservacao',
                'maxlength' => 200,
                'rows' => 3
            ),
            'options' => array(
                'label' => 'Observação: ',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Salvar',
                'class' => 'btn btn-primary',
                'id'    => 'btnSalvar'
            ),
        ));

        $this->add(array(
            'name' => 'vlTetoSemestralidadeMinimo',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'vlTetoSemestralidadeMinimo'
            ),
            'options' => array(
                'label' => 'Valor mínimo da semestralidade do curso:',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'vlTetoSemestralidadeMaximo',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'vlTetoSemestralidadeMaximo'
            ),
            'options' => array(
                'label' => 'Valor máximo da semestralidade do curso: ',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'pcAjusteFies',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'pcAjusteFies'
            ),
            'options' => array(
                'label' => 'Percentual de desconto para o valor total do curso e semestralidade para o FIES:',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'pcLimiteVagaNota3',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'pcLimiteVagaNota3'
            ),
            'options' => array(
                'label' => 'Conceito 3 ',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'pcLimiteVagaNota4',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'pcLimiteVagaNota4'
            ),
            'options' => array(
                'label' => 'Conceito 4 ',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'pcLimiteVagaNota5',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'pcLimiteVagaNota5'
            ),
            'options' => array(
                'label' => 'Conceito 5 ',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'pcLimiteVagaAutorizada',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id'    => 'pcLimiteVagaAutorizada'
            ),
            'options' => array(
                'label' => 'Autorizados ',
                'label_attributes' => array(
                    'class' => 'required',
                ),
            ),
        ));
    }
}
