<?php

namespace Inscricao\Form;

use Zend\InputFilter\InputFilter;
use Application\Entity\FiesGlobal\RacaCorEntity;
use Application\Service\EnemService;

class QuestionarioFilter extends InputFilter
{
    public function __construct()
    {

        $this->add(array(
            'name' => 'coRacaCor',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                    'messages' => array('isEmpty' => 'MSG003')
                )),
                array(
                    'name' => 'Callback',
                    'options' => array(
                        'callback' => function($value) {
                            return true;
                        },
                    ),
                ),
            ),
        ));

        $this->add(array(
            'name' => 'dsPovoIndigena',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));

        $this->add(array(
            'name' => 'dsTerraIndigena',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));

        $this->add(array(
            'name' => 'stPossuiDeficiencia',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'MSG003'),
                    ),
                ),
                array(
                    'name' => 'Callback',
                    'options' => array(
                        'callback' => function($value) {
                            if ($value == 'S') {
                                $this->get('coDeficiencia')->setRequired(true);
                            } else {
                                $this->get('coDeficiencia')->setRequired(false);
                            }
                            return true;
                        }
                    )
                )
            ),
        ));

        $this->add(array(
            'name' => 'coDeficiencia',
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'MSG003'),
                    ),
                ),
            ),
        ));

        $this->add(array(
            'name' => 'stEscolaPublica',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                    'messages' => array('isEmpty' => 'MSG003')
                )),
            )
        ));

        $this->add(array(
            'name' => 'stEnsinoSuperior',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                    'messages' => array('isEmpty' => 'MSG003')
                )),
            )
        ));

        $this->add(array(
            'name' => 'nuAnoConclusaoEnsMedio',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                    'messages' => array('isEmpty' => 'MSG003')
                )),
            )
        ));

        $this->add(array(
            'name' => 'stDeclaracaoProfessor',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                    'messages' => array('isEmpty' => 'MSG003')
                )),
            ),
        ));

        $this->add(array(
            'name' => 'stConcorrerProfessor',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Callback',
                    'options' => array(
                        'callback' => function($value) {
                            if ($value == 'S') {

                                if ($value == 'S') {
                                    $srvEnem = new EnemService();

                                    $podeConcorrerPeloEnem = $srvEnem->podeConcorrerPeloEnem();
                                    if ($podeConcorrerPeloEnem) {
//                                         $this->get('stTipoConcorrencia')->setRequired(true);
                                    }
                                }

//                                 $this->get('noEscola')->setRequired(true);
//                                 $this->get('coUf')->setRequired(true);
//                                 $this->get('coMunicipioEscola')->setRequired(true);
//                                 $this->get('stRedeEnsinoEscola')->setRequired(true);
//                                 $this->get('dsMatrInstProfEscola')->setRequired(true);
//                                 $this->get('dsAreaAtuacaoEscola')->setRequired(true);
                            } else {
//                                 $this->get('noEscola')->setRequired(false);
//                                 $this->get('coUf')->setRequired(false);
//                                 $this->get('coMunicipioEscola')->setRequired(false);
//                                 $this->get('stRedeEnsinoEscola')->setRequired(false);
//                                 $this->get('dsMatrInstProfEscola')->setRequired(false);
//                                 $this->get('dsAreaAtuacaoEscola')->setRequired(false);
                            }
                            return true;
                        }
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'noEscola',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));

        $this->add(array(
            'name' => 'coUf',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));


        $this->add(array(
            'name' => 'coMunicipioEscola',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));

        $this->add(array(
            'name' => 'stRedeEnsinoEscola',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));

        $this->add(array(
            'name' => 'dsMatrInstProfEscola',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));

        $this->add(array(
            'name' => 'dsAreaAtuacaoEscola',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));

        $this->add(array(
            'name' => 'stInadimplenteCreduc',
            'required' => false,
            'allow_empty' => true
        ));

        $this->add(array(
            'name' => 'stConcordanciaRegras',
            'required' => false,
            'allow_empty' => true
        ));
    }
}