angular.module('geccalApp').
    factory('AclSrv', ['AuthorizationSrv', '$location', 'Notification', function(AuthorizationSrv, $location, Notification){
        var authService = {};
        authService.auth = function(resource) {
            AuthorizationSrv.get(
                {id: resource},
                function(data, status, headers, config){

                },
                function(data, status, headers, config){
                    Notification.error({message: data.data.listaMensagem.mensagem.msg, delay: 10000, title: 'Erro'});
                    $location.path('/login')
                }

            );
        }
        return authService;
    }]
);