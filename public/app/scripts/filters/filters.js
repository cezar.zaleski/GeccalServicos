angular.module('geccalApp', [])
    .filter('month', function () {
    return function(nuMes){
        switch (nuMes){
            case 1:
                return 'Janeiro';
            case 2:
                return 'Fevereiro';
            case 3:
                return 'Março';
            case 4:
                return 'Abril';
            case 5:
                return 'Maio';
            case 6:
                return 'Junho';
            case 7:
                return 'Julho';
            case 8:
                return 'Agosto';
            case 9:
                return 'Setembro';
            case 10:
                return 'Outubro';
            case 11:
                return 'Novembro';
            case 12:
                return 'Dezembro';
            default:
                return 'Ano/Mes';
        }

    }
})
.filter('digits', function () {
    return function(a,b){
        return(1e4+a+"").slice(-b);
    }});