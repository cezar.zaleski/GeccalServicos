angular.module('geccalApp').
    controller('MenuHeaderCtrl', ['$scope',
        function($scope){
            $scope.teste = true;
            $scope.menuSidebar = function(){
                $scope.teste = !$scope.teste;
            }

            $scope.menuItems = ['Home', 'Contact', 'About', 'Other'];
            $scope.activeMenu = $scope.menuItems[0];

            $scope.setActive = function(menuItem) {
                $scope.activeMenu = menuItem
            }
    }]
)