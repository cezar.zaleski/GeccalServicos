angular.module('geccalApp').
    controller('LoginCtrl', ['$scope','AutenticacaoSrv','$filter','$location', 'Notification',
        function($scope, AutenticacaoSrv, $filter, $location, Notification){
            $scope.loginSubmit = function(itemLogin){
                $scope.result = AutenticacaoSrv.save(
                    {},
                    itemLogin,
                    function(data, status, headers, config){
                        $location.path('/tesouraria/registro-caixa/listar')
                    },
                    function(data, status, headers, config){
                        angular.forEach(data.data.listaMensagem, function(value) {
                            Notification.error({message: value.msg, delay: 10000, title: 'Erro'});
                        });

                    }
                )
            }
    }]
)