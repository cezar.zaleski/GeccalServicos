'use strict';
/**
 * @ngdoc overview
 * @name geccalApp
 * @description
 * # geccalApp
 *
 * Main module of the application.
 */

angular
        .module('geccalApp', [
            'oc.lazyLoad',
            'ui.router',
            'ui.bootstrap',
            'angular-loading-bar',
            'ngResource',
            'datatables',
            'datatables.bootstrap',
            'ngAnimate',
            'ui.utils.masks',
            'localytics.directives',
            'ui-notification',
        ])
    .constant("DESCRICAO_MOVIMENTO", {
        'CHEQUE' : 4,
        'TRANSFERENCIA_ONLINE' : 5,
        'DEBITO_AUTOMATICO' : 6
    })
    .constant("CONTAS", [
        {'idConta': 1, 'descConta': 'Conta Corrente'},
        {'idConta': 2, 'descConta': 'Conta Poupança OP. 1'},
        {'idConta': 3, 'descConta': 'Conta Poupança OP. 51'},
    ])
    .constant("MESES", [
        {'nuMes': 1, 'noMes': 'Janeiro'},
        {'nuMes': 2, 'noMes': 'Fevereiro'},
        {'nuMes': 3, 'noMes': 'Março'},
        {'nuMes': 4, 'noMes': 'Abril'},
        {'nuMes': 5, 'noMes': 'Maio'},
        {'nuMes': 6, 'noMes': 'Junho'},
        {'nuMes': 7, 'noMes': 'Julho'},
        {'nuMes': 8, 'noMes': 'Agosto'},
        {'nuMes': 9, 'noMes': 'Setembro'},
        {'nuMes': 10, 'noMes': 'Outubro'},
        {'nuMes': 11, 'noMes': 'Novembro'},
        {'nuMes': 12, 'noMes': 'Dezembro'}
    ])
    .controller('AbstractController', ['$scope', 'Notification', '$location',
        function($scope, Notification, $location) {
        $scope.tratamentoErros = function(data){
            angular.forEach(data.data.listaMensagem, function (mensagem) {
                Notification.error({message: mensagem.msg, delay: 10000, title: 'Erro'});
                if(mensagem.coMensagem == "E099" || mensagem.coMensagem == "E006"){
                    $location.path('/login')
                }
            });
        }
    }])
    .config(['datepickerConfig','datepickerPopupConfig', function (datepickerConfig, datepickerPopupConfig){
        datepickerPopupConfig.datepickerPopup = "dd/MM/yyyy";
        datepickerPopupConfig.currentText = "Hoje";
        datepickerPopupConfig.clearText = "Limpar";
        datepickerPopupConfig.closeText = "Sair";
    }])
    .factory('AuthorizationSrv', ['$resource', function($resource){
        return $resource(
            '/authorization', {
            }
        )

    }])
    .factory('AclSrv', ['AuthorizationSrv', '$location', 'Notification', function(AuthorizationSrv, $location, Notification){
        var authService = {};
        authService.auth = function(resource) {
            AuthorizationSrv.get(
                {id: resource},
                function(data, status, headers, config){

                },
                function(data, status, headers, config){
                    Notification.error({message: data.data.listaMensagem.mensagem.msg, delay: 10000, title: 'Erro'});
                    $location.path('/login')
                }

            );
        }
        return authService;
    }])
    .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$resourceProvider',

            function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $resourceProvider) {
                $ocLazyLoadProvider.config({
                    debug: false,
                    events: true
                });

                $urlRouterProvider.otherwise('/login');

                $stateProvider
                        .state('tesouraria', {
                            url: '/tesouraria',
                            templateUrl: '/app/views/tesouraria/main.html',
                            data: {
                                displayName: 'Main'
                            },

                            resolve: {
                                loadMyDirectives: function ($ocLazyLoad) {
                                    return $ocLazyLoad.load(
                                            {
                                                name: 'geccalApp',
                                                files: [
                                                    '/app/scripts/filters/filters.js',
                                                    '/app/scripts/directives/header/header.js',
                                                    '/app/scripts/directives/modal/modal.js',
                                                    '/app/scripts/directives/datepicker/datepicker.js',
                                                    '/app/scripts/directives/alert/alert.js',
                                                    '/app/scripts/service/AutenticacaoSrv.js',
                                                    //'/app/scripts/service/AuthorizationSrv.js',
                                                    '/app/scripts/controllers/MenuHeaderCtrl.js'
                                                    //'/app/scripts/service/AclSrv.js',

                                                ]
                                            }),
                                            $ocLazyLoad.load(
                                                    {
                                                        name: 'toggle-switch',
                                                        files: ["/app/bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                                                            "/app/bower_components/angular-toggle-switch/angular-toggle-switch.css"
                                                        ]
                                                    }),
                                            $ocLazyLoad.load(
                                                    {
                                                        name: 'ngAnimate',
                                                        files: ['/app/bower_components/angular-animate/angular-animate.js']
                                                    })
                                    $ocLazyLoad.load(
                                            {
                                                name: 'ngCookies',
                                                files: ['/app/bower_components/angular-cookies/angular-cookies.js']
                                            })
                                    $ocLazyLoad.load(
                                            {
                                                name: 'ngResource',
                                                files: ['/app/bower_components/angular-resource/angular-resource.js']
                                            })
                                    $ocLazyLoad.load(
                                            {
                                                name: 'ngSanitize',
                                                files: ['/app/bower_components/angular-sanitize/angular-sanitize.js']
                                            })
                                    $ocLazyLoad.load(
                                            {
                                                name: 'ngTouch',
                                                files: ['/app/bower_components/angular-touch/angular-touch.js']
                                            })
                                }
                            }
                        })
                        .state('tesouraria.registroCaixaListar', {
                            templateUrl: '/app/views/tesouraria/registro-caixa/listar.html',
                            url: '/registro-caixa/listar',
                            controller: (['AclSrv', function(AclSrv){
                                AclSrv.auth('tesouraria/registro-caixa/listar');
                            }]),
                            resolve: {
                                loadMyFiles: function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'geccalApp',
                                        files: [
                                            '/app/scripts/tesouraria/services/RegistroCaixaSrv.js',
                                            '/app/scripts/tesouraria/controllers/RegistroCaixaCtrl.js',
                                        ]
                                    })
                                }
                            }

                        })
                        .state('tesouraria.registroCaixaFormulario', {
                            templateUrl: '/app/views/tesouraria/registro-caixa/formulario.html',
                            url: '/registro-caixa/formulario/:id',
                            controller: (['AclSrv', function(AclSrv){
                                AclSrv.auth('tesouraria/registro-caixa/formulario');
                            }]),
                            resolve: {
                                loadMyFiles: function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'geccalApp',
                                        files: [
                                            '/app/scripts/tesouraria/controllers/RegistroCaixaCtrlFormulario.js',
                                            '/app/scripts/tesouraria/services/RegistroCaixaSrv.js',
                                            '/app/scripts/tesouraria/services/TipoPagamentoSrv.js',
                                            '/app/scripts/tesouraria/services/DescricaoLancamentoSrv.js',
                                            '/app/scripts/tesouraria/services/TipoRegistroCaixaSrv.js',
                                            '/app/scripts/tesouraria/services/ValorRegistroCaixaSrv.js',
                                        ]
                                    })
                                }
                            }

                        })
                        .state('tesouraria.relatorio', {
                            templateUrl: '/app/views/tesouraria/relatorio/listar.html',
                            url: '/relatorio/listar',
                            controller: (['AclSrv', function(AclSrv){
                                AclSrv.auth('tesouraria/relatorio/listar');
                            }]),
                            resolve: {
                                loadMyFiles: function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'geccalApp',
                                        files: [
                                            '/app/scripts/tesouraria/controllers/RelatorioCtrl.js',
                                            '/app/scripts/tesouraria/services/RelatorioSrv.js',
                                        ]
                                    })
                                }
                            }
                        })
                        .state('login', {
                            templateUrl: '/app/views/login.html',
                            url: '/login',
                            resolve: {
                                loadMyFiles: function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'geccalApp',
                                        files: [
                                            '/app/scripts/controllers/LoginCtrl.js',
                                            '/app/scripts/service/AutenticacaoSrv.js'
                                        ]
                                    })
                                }
                            }
                        })
                        .state('tesouraria.movimentoBancarioConta', {
                            templateUrl: '/app/views/tesouraria/movimento-bancario/conta.html',
                            url: '/movimento-bancario/conta',
                            controller: (['AclSrv', function(AclSrv){
                                AclSrv.auth('tesouraria/movimento-bancario/conta');
                            }]),
                            resolve: {
                                loadMyFiles: function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'geccalApp',
                                        files: [
                                            '/app/scripts/tesouraria/controllers/ContaCtrl.js',
                                            '/app/scripts/tesouraria/services/ContaSrv.js',
                                        ]
                                    })
                                }
                            }
                        })
                        .state('tesouraria.movimentoBancarioDetalharConta', {
                            templateUrl: '/app/views/tesouraria/movimento-bancario/detalhar.html',
                            url: '/movimento-bancario/detalhar-conta/:id',
                            controller: (['AclSrv', function(AclSrv){
                                AclSrv.auth('tesouraria/movimento-bancario/detalhar');
                            }]),
                            resolve: {
                                loadMyFiles: function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'geccalApp',
                                        files: [
                                            '/app/scripts/tesouraria/controllers/DetalharContaCtrl.js',
                                            '/app/scripts/tesouraria/services/ContaSrv.js',
                                            '/app/scripts/tesouraria/services/AnoMesMovimentacaoSrv.js',
                                            '/app/scripts/tesouraria/services/MovimentoContaSrv.js',
                                        ]
                                    })
                                }
                            }
                        })
                        .state('tesouraria.movimentoBancarioFormulario', {
                            templateUrl: '/app/views/tesouraria/movimento-bancario/formulario.html',
                            url: '/movimento-bancario/formulario/:idConta/:nuAnoMes',
                            controller: (['AclSrv', function(AclSrv){
                                AclSrv.auth('tesouraria/movimento-bancario/formulario');
                            }]),
                            resolve: {
                                loadMyFiles: function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'geccalApp',
                                        files: [
                                            '/app/scripts/tesouraria/controllers/ContaFormularioCtrl.js',
                                            '/app/scripts/tesouraria/services/AnoMesMovimentacaoSrv.js',
                                            '/app/scripts/tesouraria/services/DescricaoMovimentoContaSrv.js',
                                            '/app/scripts/tesouraria/services/MovimentoContaDescricaoSrv.js',
                                            '/app/scripts/tesouraria/services/TipoRegistroCaixaSrv.js',
                                        ]
                                    })
                                }
                            }
                        })

            }]);



