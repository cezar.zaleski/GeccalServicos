'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('geccalApp')
	.directive('myDatepicker',function(){
		return {
			restrict: 'A',
			scope: {
				model: "=",
				format: "@",
				options: "=datepickerOptions",
				myid: "@",
				myname: "@"
			},
			templateUrl:'/app/scripts/directives/datepicker/datepicker.html',
			link: function(scope, element) {
				scope.popupOpen = false;
				scope.openPopup = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					scope.popupOpen = true;
				};

				scope.open = function($event) {
					$event.preventDefault();
					$event.stopPropagation();
					scope.opened = true;
				};
			}
		};
	});


