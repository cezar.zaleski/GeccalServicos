angular.module('geccalApp').
    controller('RegistroCaixaCtrlFormulario', ['$scope','RegistroCaixaSrv','$location','TipoPagamentoSrv',
        'DescricaoLancamentoSrv','TipoRegistroCaixaSrv','$filter', 'ValorRegistroCaixaSrv', '$stateParams',
        'Notification','$controller',
        function($scope, RegistroCaixaSrv, $location, TipoPagamentoSrv, DescricaoLancamentoSrv, TipoRegistroCaixaSrv,
        $filter, ValorRegistroCaixaSrv, $stateParams, Notification, $controller){

            /**
             * declaração de variáveis e objetos
             *
             */
            angular.extend($controller('AbstractController', {$scope: $scope}));
            $scope.itensTipoRegistroCaixas = [];
            $scope.itensDescricaoLancamentos = [];
            $scope.idRegistroCaixa = null;
            $scope.nuValorRegistro = '';
            //$scope.idRegistroCaixa = '111';
            //$scope.idRegistroCaixa = '108';
            $scope.idRegistroCaixa = null;
            //$scope.pagamentos = null;
            $scope.showModal = false;
            $scope.format = 'dd/MM/yyyy';
            $scope.ngPagamentos = true;

            $scope.directiveOptions = {
                no_results_text: "Nenhum resultado para"
            };



            /*********************************************************/
            /**
             * Declaração de functions
             */


            $scope.lodValorRegistro = function(){
                $scope.ngPagamentos = true;
                if($scope.idRegistroCaixa){
                    $scope.pagamentos = ValorRegistroCaixaSrv.get({id: $scope.idRegistroCaixa});
                }
                if($scope.pagamentos){
                    $scope.ngPagamentos = false;
                }
            };

            $scope.toggleModal = function(){
                $scope.showModal = !$scope.showModal;
            };

            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.deleteRegistryValue = function(e){
                ValorRegistroCaixaSrv.remove(
                    {
                        id: e.target.getAttribute('data-id')
                    },
                    {},
                    function(data, status, headers, config){
                        $scope.lodValorRegistro();
                    },
                    function(data, status, headers, config){
                        $scope.alerts = data.data.listaMensagem;
                    }
                );
            };
            $scope.closeModal = function(){
                $scope.showModal = false;
            }

            $scope.registroCaixaSubmit = function(item){
                if(item == undefined){
                    Notification.error({message: 'Campo obrigatório não preenchido', delay: 10000, title: 'Erro'});
                    return;
                }
                item.idRegistroCaixa = $scope.idRegistroCaixa;
                item.dtRegistro = $filter('date')( item.dtRegistro, 'yyyy-MM-dd');

                if($scope.idRegistroCaixa){
                    $scope.result = RegistroCaixaSrv.update(
                        {id: $scope.idRegistroCaixa},
                        item,
                        function(data, status, headers, config){
                            Notification.success({message: 'Registro de caixa salvo com sucesso', delay: 10000, title: 'Sucesso'});
                            $location.path('/tesouraria/registro-caixa/listar')
                        },
                        function(data, status, headers, config){
                            $scope.tratamentoErros(data);
                        }
                    )
                }else{
                    $scope.result = RegistroCaixaSrv.save(
                        {},
                        item,
                        function(data, status, headers, config){
                            Notification.success({message: 'Registro de caixa salvo com sucesso', delay: 10000, title: 'Sucesso'});
                            $location.path('/tesouraria/registro-caixa/listar')
                        },
                        function(data, status, headers, config){
                            Notification.error({message: data.data.listaMensagem.mensagem.msg, delay: 10000, title: 'Erro'});
                        }
                    )
                }
            }

            $scope.formaPagamentoSubmit = function(itemPagamento){
                itemPagamento.idRegistroCaixa = $scope.idRegistroCaixa;
                $scope.result = ValorRegistroCaixaSrv.save(
                    {},
                    itemPagamento,
                    function(data, status, headers, config){
                        $scope.alerts = [
                            { type: 'success', msg: 'Valor de registro cadastrado com sucesso' }
                        ];
                        itemPagamento.idTipoPagamento = null;
                        itemPagamento.nuValor = null;
                        $scope.idRegistroCaixa = data.idRegistroCaixa;
                        $scope.lodValorRegistro();
                    },
                    function(data, status, headers, config){
                        $scope.alerts = data.data.listaMensagem;

                    }
                )
            }
            $scope.getRegistroCaixa = function(){
                $scope.item = RegistroCaixaSrv.get({id: $stateParams.id});
                $scope.item.nuValor = 10;
            }


            /*********************************************************/
            /**
             * Verificação edição
             */
            if($stateParams.id){
                $scope.idRegistroCaixa = $stateParams.id;
                $scope.getRegistroCaixa();
            }

            /******************************************************************************************************/
            /**
             * carregamento de objetos
             */
            $scope.tipoPagamentos = TipoPagamentoSrv.query();
            TipoRegistroCaixaSrv.query(function (response) {
                angular.forEach(response, function (item) {
                    if (item.id_tipo_registro_caixa) {
                        $scope.itensTipoRegistroCaixas.push({"id": item.id_tipo_registro_caixa, "label": item.no_tipo_registro_caixa + ' (' + item.no_finalidade + ')' });
                    }
                });
            });

            $scope.tipoRegistroCaixas = {
                "values": $scope.itensTipoRegistroCaixas
            };


            DescricaoLancamentoSrv.query(function (response) {
                angular.forEach(response, function (item) {
                    if (item.id_desc_lancamento) {
                        $scope.itensDescricaoLancamentos.push({"id": item.id_desc_lancamento, "label": item.co_desc_lancamento + ' ' + item.no_desc_lancamento });
                    }
                });
            });

            $scope.descricaoLancamentos = {
                "values": $scope.itensDescricaoLancamentos
            };
            $scope.dtRegistro = $filter('date')( new Date(), 'yyyy-MM-dd');
            $scope.lodValorRegistro();
        }]
);