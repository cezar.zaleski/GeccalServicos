angular.module('geccalApp').
controller('RelatorioCtrl', ['$scope','RelatorioSrv','DTOptionsBuilder','DTColumnBuilder','$filter','$location',
    'Notification',
    function($scope, RelatorioSrv, DTOptionsBuilder,DTColumnBuilder, $filter, $location, Notification){

        $scope.load = function(){
            $scope.registros = RelatorioSrv.query(
                function(data, status, headers, config){

                },
                function(data, status, headers, config){
                    Notification.error({message: data.data.listaMensagem.mensagem.msg, delay: 10000, title: 'Erro'});
                    $location.path('/login')
                }
            );
        };
        $scope.load();

         function gerarRelatorio(data){
             location.href = '/tesouraria/gerar-relatorio?mes='+data.nu_mes+'&ano='+data.nu_ano+'&tipo='+data.tp_relatorio;
        }


        $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
                return $scope.registros.$promise;
            })
            .withOption({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bFixedHeader": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "bStateSave": true
                }
            )
            .withOption('rowCallback', rowCallback)
            .withPaginationType('simple_numbers')
            .withLanguage({
                "sProcessing": "Processando...",
                "sLengthMenu": "Mostrar _MENU_ registros por página",
                "sZeroRecords": "Nenhum Registro Encontrado...",
                "sInfo": "Paginação _START_ de _END_, Total _TOTAL_ registros",
                "sInfoEmpty": "Paginação 0 de 0, Total 0 registros",
                "sInfoFiltered": "(Filtrado de _MAX_ registros ao todo)",
                "sSearch": "Procurar: ",
                "sNext": "Próximo",
                "sLoadingRecords": "carregando...",
                "sEmptyTable": "Nenhum Registro Encontrado",
                "oPaginate": {
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sFirst": "Primeira",
                    "sLast": "Última",
                    "sZeroRecords": "Nenhum Registro Encontrado..."
                }
            });
        $scope.dtColumns = [
            DTColumnBuilder.newColumn('no_relatorio').withTitle('Tipo'),
            DTColumnBuilder.newColumn(null).withTitle('Mês/Ano').renderWith(mesAno),
            DTColumnBuilder.newColumn('ds_relatorio').withTitle('Descrição'),
            DTColumnBuilder.newColumn(null).withTitle('Ação').notSortable()
                .renderWith(actionsHtml)
        ];

        function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $('td', nRow).unbind('click');
            $('td .gerarRelatorio', nRow).bind('click', function() {
                $scope.$apply(function() {
                    gerarRelatorio(aData);
                });
            });
            return nRow;
        }

        function mesAno(data, type, full, meta) {
            return data.no_mes + '/' + data.nu_ano;
        }

        function actionsHtml(data, type, full, meta) {
            return '<button ng-click="gerarRelatorio()" ' +
                'title="Download" alt="Download" class="gerarRelatorio btn btn-primary">' +
                '   <i class="fa fa-download"></i>' +
                '</button>';
        }

    }]
)