angular.module('geccalApp').
    controller('RegistroCaixaCtrl', ['$scope','RegistroCaixaSrv','DTOptionsBuilder','DTColumnBuilder','$filter','$location',
        'Notification','$controller',
        function($scope, RegistroCaixaSrv, DTOptionsBuilder,DTColumnBuilder, $filter, $location, Notification, $controller){
            angular.extend($controller('AbstractController', {$scope: $scope}));
            $scope.load = function(){
                $scope.registros = RegistroCaixaSrv.query(
                    function(data, status, headers, config){

                    },
                    function(data, status, headers, config){
                        //$scope.tratamentoErros(data);
                        //Notification.error({message: data.data.listaMensagem.mensagem.dsMessage, delay: 10000, title: 'Erro'});
                        //$location.path('/login')
                    }
                );
            };

            $scope.excluirRegistroCaixa = false;
            $scope.preencherRegistroCaixa = function(){
                $scope.load();
                $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
                        return $scope.registros.$promise;
                    })
                    .withOption({
                            "bPaginate": true,
                            "bLengthChange": true,
                            "bFilter": true,
                            "bFixedHeader": true,
                            "bSort": true,
                            "bInfo": true,
                            "bAutoWidth": true,
                            "bStateSave": true
                        }
                    )
                    .withOption('rowCallback', rowCallback)
                    .withPaginationType('simple_numbers')
                    .withLanguage({
                        "sProcessing": "Processando...",
                        "sLengthMenu": "Mostrar _MENU_ registros por página",
                        "sZeroRecords": "Nenhum Registro Encontrado...",
                        "sInfo": "Paginação _START_ de _END_, Total _TOTAL_ registros",
                        "sInfoEmpty": "Paginação 0 de 0, Total 0 registros",
                        "sInfoFiltered": "(Filtrado de _MAX_ registros ao todo)",
                        "sSearch": "Procurar: ",
                        "sNext": "Próximo",
                        "sLoadingRecords": "carregando...",
                        "sEmptyTable": "Nenhum Registro Encontrado",
                        "oPaginate": {
                            "sPrevious": "Anterior",
                            "sNext": "Próximo",
                            "sFirst": "Primeira",
                            "sLast": "Última",
                            "sZeroRecords": "Nenhum Registro Encontrado..."
                        }
                    });
                $scope.dtColumns = [
                    DTColumnBuilder.newColumn('id_registro_caixa').notVisible(),
                    DTColumnBuilder.newColumn('dt_registro').withTitle('Data').renderWith(function(data, type) {
                        return $filter('date')(data, 'dd/MM/yyyy'); //date filter
                    }),
                    DTColumnBuilder.newColumn('no_descricao').withTitle('Descriçao Lançamento'),

                    DTColumnBuilder.newColumn('nu_valor').withTitle('Valor').renderWith(function(valor, type) {
                        return $filter('currency')(valor); //date filter
                    }),
                    DTColumnBuilder.newColumn('noTipoRegistroCaixa').withTitle('Tipo de Registro'),
                    DTColumnBuilder.newColumn('no_pessoa').withTitle('Responsável'),
                    DTColumnBuilder.newColumn(null).withTitle('Ação').notSortable()
                        .renderWith(actionsHtml)
                ];

            };

            $scope.preencherRegistroCaixa();
            function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                $('td', nRow).unbind('click');
                $('td .excluirRegistro', nRow).bind('click', function() {
                    $scope.$apply(function() {
                        $scope.idRegistroCaixaExcluir = aData.id_registro_caixa;
                        $scope.excluirRegistroCaixa = !$scope.excluirRegistroCaixa;
                    });
                });
                return nRow;
            }

            function actionsHtml(data, type, full, meta) {
                return '<a href="#/tesouraria/registro-caixa/formulario/' + data.id_registro_caixa + '"><button title="Editar" alt="Editar" class="btn btn-warning">' +
                    '   <i class="fa fa-edit"></i>' +
                    '</button></a>&nbsp;' +
                    '<button data-ano="2016" data-mes="1" data-tipo="registro_caixa" title="Excluir" alt="Excluir" class="btn btn-danger excluirRegistro" ng-click="modalExcluirRegistroCaixa()">' +
                    '   <i class="fa fa-trash-o"></i>' +
                    '</button>';
            }

            $scope.closeModalExcluir = function(){
                $scope.excluirRegistroCaixa = false;
            }

            $scope.excluirRegistro = function(){
                RegistroCaixaSrv.remove(
                    {
                        id: $scope.idRegistroCaixaExcluir
                    },
                    {},
                    function(data, status, headers, config){
                        Notification.success({message: 'Registro de caixa removido com sucesso', delay: 10000, title: 'Sucesso'});
                        $location.path('/tesouraria/registro-caixa/listar')
                    },
                    function(data, status, headers, config){
                        Notification.error({message: data.data.listaMensagem.mensagem.msg, delay: 10000, title: 'Erro'});
                    }
                );
            };




    }]
)