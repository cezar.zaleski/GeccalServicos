angular.module('geccalApp').
    controller('DetalharContaCtrl', ['$scope','ContaSrv','DTOptionsBuilder','DTColumnBuilder','$filter','$location',
        'Notification', '$stateParams','AnoMesMovimentacaoSrv','MovimentoContaSrv',
        function($scope, ContaSrv, DTOptionsBuilder,DTColumnBuilder, $filter, $location,
                 Notification, $stateParams, AnoMesMovimentacaoSrv,MovimentoContaSrv){
            $scope.carregarDetalheConta = function(){
                $scope.registros = MovimentoContaSrv.query(
                    {idConta: "idConta="+$stateParams.id},
                    function(data, status, headers, config){
                    },
                    function(data, status, headers, config){
                        Notification.error({message: data.data.listaMensagem.mensagem.msg, delay: 10000, title: 'Erro'});
                        //$location.path('/login')
                    }
                );
            };
            $scope.itensAnoMes = [];
            $scope.showModalMovBancario = false;
            $scope.idConta = $stateParams.id;


            $scope.preencherDetalheConta = function(){
                $scope.carregarDetalheConta();
                $scope.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
                        return $scope.registros.$promise;
                    })
                    .withOption({
                            "bPaginate": true,
                            "bLengthChange": true,
                            "bFilter": true,
                            "bFixedHeader": true,
                            "bSort": true,
                            "bInfo": true,
                            "bAutoWidth": true,
                            "bStateSave": true
                        }
                    )
                    //.withOption('rowCallback', rowCallback)
                    .withPaginationType('simple_numbers')
                    .withLanguage({
                        "sProcessing": "Processando...",
                        "sLengthMenu": "Mostrar _MENU_ registros por página",
                        "sZeroRecords": "Nenhum Registro Encontrado...",
                        "sInfo": "Paginação _START_ de _END_, Total _TOTAL_ registros",
                        "sInfoEmpty": "Paginação 0 de 0, Total 0 registros",
                        "sInfoFiltered": "(Filtrado de _MAX_ registros ao todo)",
                        "sSearch": "Procurar: ",
                        "sNext": "Próximo",
                        "sLoadingRecords": "carregando...",
                        "sEmptyTable": "Nenhum Registro Encontrado",
                        "oPaginate": {
                            "sPrevious": "Anterior",
                            "sNext": "Próximo",
                            "sFirst": "Primeira",
                            "sLast": "Última",
                            "sZeroRecords": "Nenhum Registro Encontrado..."
                        }
                    });
                $scope.dtColumns = [
                    DTColumnBuilder.newColumn('id_movimentacao_conta').notVisible(),
                    //DTColumnBuilder.newColumn('nuAnoMes').withTitle('Mes/Ano'),
                    DTColumnBuilder.newColumn('nu_ano_mes').withTitle('Mes/Ano').renderWith(function(data, type) {
                        return $filter('month')(parseFloat($filter('limitTo')(String(data), -2)))+'/'+$filter('limitTo')(String(data), 4);
                    }),
                    //DTColumnBuilder.newColumn('noConta').withTitle('Conta'),
                    DTColumnBuilder.newColumn(null).withTitle('Conta').renderWith(function(data,type,full) {
                        if(data.nuOp){
                          return data.no_conta + ' OP. '+$filter('digits')(data.nu_op);
                        }
                        return data.no_conta;

                    }),
                    DTColumnBuilder.newColumn('nu_saldo').withTitle('Saldo').renderWith(function(valor, type) {
                        return $filter('currency')(valor); //date filter
                    }),
                    DTColumnBuilder.newColumn(null).withTitle('Ação').notSortable()
                        .renderWith(actionsHtml)
                ];

            };

            function actionsHtml(data, type, full, meta) {
                return '<a href="#tesouraria/movimento-bancario/formulario/' + $stateParams.id + '/'+data.nu_ano_mes+'"><button title="Visualizar" alt="Visualizar" class="btn btn-warning">' +
                    '   <i class="fa fa-eye"></i>' +
                    '</button></a>';
            }


            $scope.preencherDetalheConta();
            $scope.nuAnosMeses = AnoMesMovimentacaoSrv.query();
            $scope.abrirModalEscolherMes = function(){
                $scope.showModalMovBancario = !$scope.showModalMovBancario;
            };
            $scope.cadastrarMovimentacao = function(nuAnoMes){
                if(nuAnoMes){
                    $('.modal-backdrop').remove();
                    $scope.showModalMovBancario = false;
                    $location.path('/tesouraria/movimento-bancario/formulario/'+$stateParams.id+'/'+nuAnoMes.nu_mes_ano);
                }
            };
    }]
)