angular.module('geccalApp')
    .controller('ContaFormularioCtrl', ['$scope','$location','$filter', '$stateParams',
        'Notification','AnoMesMovimentacaoSrv','DescricaoMovimentoContaSrv','DESCRICAO_MOVIMENTO',
        'MovimentoContaDescricaoSrv','CONTAS','MESES','$controller','TipoRegistroCaixaSrv',
        function($scope, $location, $filter, $stateParams, Notification, AnoMesMovimentacaoSrv, DescricaoMovimentoContaSrv,
                 DESCRICAO_MOVIMENTO, MovimentoContaDescricaoSrv, CONTAS, MESES,$controller, TipoRegistroCaixaSrv){

            /**
             * declaração de variáveis e objetos
             *
             */
            angular.extend($controller('AbstractController', {$scope: $scope}));
            $scope.itensAnoMes = [];
            $scope.itensTipoRegistroCaixas = [];
            $scope.infoConta = $filter('filter')(CONTAS, {idConta: $stateParams.idConta})[0];
            $scope.infoMes = $filter('filter')(MESES, {nuMes: parseFloat($filter('limitTo')(String($stateParams.nuAnoMes), -2))})[0];
            $scope.infoNuAno = $filter('limitTo')(String($stateParams.nuAnoMes), 4);


            $scope.showModal = false;
            $scope.format = 'dd/MM/yyyy';
            $scope.descCheque = false;
            $scope.descTransferencia = false;
            $scope.descDebito = false;
            $scope.idConta = $stateParams.idConta;

            $scope.directiveOptions = {
                no_results_text: "Nenhum resultado para"
            };



            /*********************************************************/
            /**
             * Declaração de functions
             */
            $scope.carregarMovimento = function(){
                //$scope.ngPagamentos = true;
                //if($scope.idRegistroCaixa){
                $scope.movContaBancario = MovimentoContaDescricaoSrv.query(
                    {nuAnoMes: 'nuAnoMes='+$stateParams.nuAnoMes, idConta:'idConta='+$stateParams.idConta}
                );
                //}
                //if($scope.pagamentos){
                //    $scope.ngPagamentos = false;
                //}
            }

            $scope.carregarDescricaoMovimento = function() {
                $scope.itensDescricaoMovimento = [];
                DescricaoMovimentoContaSrv.query(
                    {nuAnoMes: 'nuAnoMes=' + $stateParams.nuAnoMes, idConta: 'idConta=' + $stateParams.idConta},
                    function (response) {
                        angular.forEach(response, function (item) {
                            if (item.id_desc_movimento_conta) {
                                $scope.itensDescricaoMovimento.push({
                                    "id": item.id_desc_movimento_conta,
                                    "label": item.no_desc_movimentacao_conta
                                });
                            }
                        });
                    });
                $scope.descricaoMovimento = {
                    "values": $scope.itensDescricaoMovimento
                };
            }

            $scope.anoMes = {
                "values": $scope.itensAnoMes
            };

            $scope.verificaDescMovimentacao = function(descMov){
                $scope.descTransferencia = false;
                $scope.descCheque = false;
                $scope.descDebito = false;
                console.log(descMov == DESCRICAO_MOVIMENTO.CHEQUE);
                if(descMov == DESCRICAO_MOVIMENTO.CHEQUE){
                    $scope.descCheque = true;
                }
                if(descMov == DESCRICAO_MOVIMENTO.TRANSFERENCIA_ONLINE){
                    $scope.descTransferencia = true;
                }
                if(descMov == DESCRICAO_MOVIMENTO.DEBITO_AUTOMATICO){
                    $scope.descDebito = true;
                }
            }

            $scope.movimentoBancarioSubmit = function(item){
                if(item == undefined){
                    Notification.error({message: 'Campo obrigatório não preenchido', delay: 10000, title: 'Erro'});
                    return;
                }
                item.conta = $stateParams.idConta;
                item.nuAnoMes =$stateParams.nuAnoMes;

                $scope.result = AnoMesMovimentacaoSrv.save(
                    {},
                    item,
                    function(data, status, headers, config){
                        Notification.success({message: 'Movimentação salva com sucesso', delay: 10000, title: 'Sucesso'});
                        item.nuValor = null;
                        item.nuCheque = null;
                        item.tipoRegistroCaixa = null;
                        item.dsTransferencia = null;
                        item.dsDebito = null;
                        $scope.descDebito = null;
                        $scope.descTransferencia = null;
                        $scope.descCheque = null;
                        $scope.carregarMovimento();
                        $scope.carregarDescricaoMovimento();
                    },
                    function(data, status, headers, config){
                        $scope.tratamentoErros(data);
                    }
                )
            }

            $scope.deleteMovimentoDescricao = function(e){
                AnoMesMovimentacaoSrv.remove(
                    {
                        id: e.target.getAttribute('data-id_movimentacao_conta')+'999'+e.target.getAttribute('data-id_desc_movimento_conta')
                    },
                    {},
                    function(data, status, headers, config){
                        $scope.carregarMovimento();
                        $scope.carregarDescricaoMovimento();
                    },
                    function(data, status, headers, config){
                        //$scope.alerts = data.data.listaMensagem;
                    }
                );
            };

            /******************************************************************************************************/
            /**
             * carregamento de objetos
             */
            AnoMesMovimentacaoSrv.query(function (response) {
                angular.forEach(response, function (item) {
                    if (item.nu_mes_ano) {
                        $scope.itensAnoMes.push({"id": item.nu_mes_ano, "label": item.nu_mes_ano_desc});
                    }
                });
            });

            TipoRegistroCaixaSrv.query(function (response) {
                angular.forEach(response, function (item) {
                    if (item.id_tipo_registro_caixa) {
                        $scope.itensTipoRegistroCaixas.push({"id": item.id_tipo_registro_caixa, "label": item.no_tipo_registro_caixa + ' (' + item.no_finalidade + ')' });
                    }
                });
            });

            $scope.tipoRegistroCaixas = {
                "values": $scope.itensTipoRegistroCaixas
            };


            $scope.carregarDescricaoMovimento();
            $scope.carregarMovimento();
            $scope.dtRegistro = $filter('date')( new Date(), 'yyyy-MM-dd');
        }]
);