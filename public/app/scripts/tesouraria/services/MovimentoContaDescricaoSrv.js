angular.module('geccalApp').
    factory('MovimentoContaDescricaoSrv', ['$resource', function($resource){
        return $resource(
            '/tesouraria/movimento-conta-descricao?:nuAnoMes&:idConta', {
                nuAnoMes: '@nuAnoMes',
                idConta: '@idConta'
            }
        )
    }]
);