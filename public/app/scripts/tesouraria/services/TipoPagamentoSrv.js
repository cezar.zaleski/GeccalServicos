angular.module('geccalApp').
    factory('TipoPagamentoSrv', ['$resource', function($resource){
        return $resource(
            '/tesouraria/tipo-pagamento/:id', {
                id: '@id'
            }
        )
    }]
);