angular.module('geccalApp').
    factory('MovimentoContaSrv', ['$resource', function($resource){
        return $resource(
            '/tesouraria/movimento-conta:id?:idConta', {
                id: '@id',
                idConta: '@idConta'
            }
        )
    }]
);