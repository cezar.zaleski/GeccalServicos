angular.module('geccalApp').
    factory('DetalharContaSrv', ['$resource', function($resource){
        return $resource(
            '/tesouraria/detalhar-conta/:id', {
                id: '@id'
            }
        )
    }]
);