angular.module('geccalApp').
    factory('TipoRegistroCaixaSrv', ['$resource', function($resource){
        return $resource(
            '/tesouraria/tipo-registro-caixa/:id', {
                id: '@id'
            }
        )
    }]
);