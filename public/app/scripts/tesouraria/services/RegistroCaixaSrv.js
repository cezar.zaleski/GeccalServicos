angular.module('geccalApp').
    factory('RegistroCaixaSrv', ['$resource', function($resource){
        return $resource(
            '/tesouraria/registro-caixa/:id', {
                id: '@id'
            },
            {
                update: {
                    method: 'PUT',
                    url: '/tesouraria/registro-caixa/:id'
                }
            }
        )
    }]
);