angular.module('geccalApp').
    factory('AnoMesMovimentacaoSrv', ['$resource', function($resource){
        return $resource(
            '/tesouraria/ano-mes-movimentacao/:id', {
                id: '@id',
            }
        )
    }]
);