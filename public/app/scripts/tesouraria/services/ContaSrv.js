angular.module('geccalApp').
    factory('ContaSrv', ['$resource', function($resource){
        return $resource(
            '/tesouraria/conta/:id', {
                id: '@id'
            }
        )
    }]
);