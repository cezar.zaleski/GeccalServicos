angular.module('geccalApp').
    factory('DescricaoMovimentoContaSrv', ['$resource', function($resource){
        return $resource(
            '/tesouraria/descricao-movimento-conta?:nuAnoMes&:idConta', {
                nuAnoMes: '@nuAnoMes',
                idConta: '@idConta'
            }
        )
    }]
);