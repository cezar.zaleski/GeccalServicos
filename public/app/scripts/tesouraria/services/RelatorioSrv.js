angular.module('geccalApp').
    factory('RelatorioSrv', ['$resource', function($resource){
        return $resource(
            '/tesouraria/relatorio/:id', {
                id: '@id'
            }
        )
    }]
);