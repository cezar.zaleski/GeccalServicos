angular.module('geccalApp').
    factory('DescricaoLancamentoSrv', ['$resource', function($resource){
        return $resource(
            '/tesouraria/descricao-lancamento/:id', {
                id: '@id'
            }
        )
    }]
);