angular.module('geccalApp').
    factory('ValorRegistroCaixaSrv', ['$resource', function($resource){
        return $resource(
            '/tesouraria/valor-registro-caixa/:id', {
                id: '@id'
            }
        )
    }]
);