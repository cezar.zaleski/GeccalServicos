# Template de Software PHP

## Introdução

Este é um software PHP recém-criado.
O objetivo é facilitar a adequação aos padrões de integração contínua e qualidade de software.
Siga as instruções para configuração do software.

## Configuraçoes

### svn:ignore

Diretório raiz:

    *~
    .DS_Store
    ._*
    .~lock.*
    .settings
    .buildpath
    .project
    .idea
    cache.properties
    nbproject
    composer.phar
    vendor

Diretório `build`:

    api
    code-browser
    coverage
    logs
    pdepend
    phpdox

### Arquivos

* `composer.json`, altere o valor de: 
    * `name`
    * `description`
    * `require`
    * `autoload`
* `phpunit.xml`
    * Configure `<testsuites>`
    * Configure `<filter>`
* `phpdox.xml`
    * Configure `<collector>`
* `build.xml`
    * Defina a propriedade `sourcedir`
