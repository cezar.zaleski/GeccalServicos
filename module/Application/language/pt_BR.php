<?php

return array(
    /* -------------------------------- */
    // MENSAGENS DO SISTEMA
    /* -------------------------------- */
    'MSG001' => 'Campo valor de registro obrigatório.',
    'MSG002' => 'Descrição já cadastrada o mês/ano.',



    /* -------------------------------- */
    // MENSAGENS DE SUCESSO
    /* -------------------------------- */
    'S001' => '',

    /* -------------------------------- */
    // MENSAGENS INFORMATIVAS
    /* -------------------------------- */
    'I001' => 'Codigo de seguranca SisFies: %s valido ate as %s.',//Sem acentuação pois é utilizado no envio de SMS

    /* -------------------------------- */
    // MENSAGENS DE ERRO
    /* -------------------------------- */
    'E001' => 'Campo %s com domínio desconhecido',
    'E002' => 'Campo %s obrigatório',
    'E003' => 'Campo %s inválido',
    'E004' => 'Erro inesperado. Por favor, entre em contato com o administrador',
    'E005' => 'Usuário ou Senha não conferem.',
    'E006' => 'Sessão expirada.',
    'E099' => 'Usuário não autorizado',


    /* -------------------------------- */
    // MENSAGENS gerais
    /* -------------------------------- */
    "Value is required and can't be empty" => 'O campo %s é de preenchimento obrigatório.',
    'The input does not appear to be a valid date' => 'A data informada é inválida.',
    'The input must contain only digits' => 'O campo %s pode ter somente números.',

);
