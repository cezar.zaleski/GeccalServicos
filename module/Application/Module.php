<?php

namespace Application;

use Application\Service\AbstractService;
use Application\Service\Acl;
use Application\Utils\RenderResult;
use Application\Auth\Adapter;
use Application\Utils\Translator;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Service\DriverFactory;
use DoctrineModule\Service\EventManagerFactory;
use DoctrineORMModule\Form\Annotation\AnnotationBuilder;
use DoctrineORMModule\Service\ConfigurationFactory;
use DoctrineORMModule\Service\DBALConnectionFactory;
use DoctrineORMModule\Service\EntityManagerFactory;
use DoctrineORMModule\Service\EntityResolverFactory;
use DoctrineORMModule\Service\SQLLoggerCollectorFactory;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\Form\Element;
use Zend\Http\Response;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

class Module
{
    /** @var  ServiceManager */
    private $_serviceManager;
    private $_config;

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function onBootstrap(MvcEvent $event)
    {
        $app = $event->getApplication();
        $sm  = $app->getServiceManager();
        $em  = $app->getEventManager();
        $this->registerUnkonwnTypesForDoctrine($sm);
        $this->_serviceManager = $sm;
        AbstractService::setServiceManager($this->_serviceManager);
        $em->attach(MvcEvent::EVENT_DISPATCH, function($e) use ($sm) {
            /** @var Acl $srvAcl */
            $srvAcl = $sm->get('AclService');
              return $srvAcl->verificaPermissaoAcesso($e);
        }, 100);

        // catch exceptions
        $em->attach('dispatch.error', function ($e) use ($sm) {

//            if (APPLICATION_ENV != 'production') {
                /** @var \Exception  $exception */
                            $exception = $e->getParam('exception');
                $requestInfo = '<h2>Request Info:</h2>';
                $requestInfo .= '<b>Controller Class: </b>' . $e->getParam('controller-class') . '<br/>';
                $requestInfo .= '<b>Request Uri: </b>' . $e->getRequest()->getRequestUri() . '<br/>';
                $requestInfo .= '<b>HTTP Method: </b>' . $e->getRequest()->getMethod() . '<br/>';
                $requestInfo .= (count($e->getRequest()->getQuery())) ?
                    '<b>Query Data: </b> ' . json_encode($e->getRequest()->getQuery()) . '<pre/><br/>' : '';
                $requestInfo .= (count($e->getRequest()->getPost())) ?
                    '<b>Post Data: </b> ' . json_encode($e->getRequest()->getPost()) . '<pre/><br/>' : '';
                $message =  $exception ? $exception->getMessage():'' . "<br/><br/>";
                $previous = str_replace('#', '<br/>#', $exception?$exception->getPrevious():'') . "<br/><br/>";
                $trace = str_replace('#', '<br/>#',$exception?$exception->getTraceAsString():'');
                $xdebug = isset($exception->xdebug_message) ?
                    '<table class="xdebug-error xe-fatal-error" dir="ltr" border="1" cellspacing="0" cellpadding="1">' .
                    $exception->xdebug_message . '</table>' : '';

                $emailBody = $requestInfo . '<h2>Message</h2>' . $message . '<h2>Previous</h2>' . $previous . '<h2>Trace</h2>' . $trace;

                /*$mailTransport = $sm->get('MailTransport');
                $mailMessage = $sm->get('MailMessage');
                try {
                    $mailMessage->setSubject('[FIES SERVIÇOS][' . $_SERVER['HTTP_REFERER'] . '] Erro')
                        ->addTo('erro.fies@mec.gov.br')
                        ->setBody($emailBody);
                    $mailTransport->send($mailMessage);
                } catch (\Exception $e) {
                    echo '<br>' . $e->getTraceAsString();
                    die();
                }*/
                echo $xdebug . '<br/>' . $emailBody . '<br/>';
//            }
        });
    }

    private function registerUnkonwnTypesForDoctrine($sm)
    {
        $entityManager = $sm->get('Doctrine\ORM\EntityManager');
        $platform = $entityManager->getConnection()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');
        $platform->registerDoctrineTypeMapping('bit', 'string');
        $platform->registerDoctrineTypeMapping('name', 'string');
        $platform->registerDoctrineTypeMapping('unknown', 'string');
        $platform->registerDoctrineTypeMapping('set', 'string');
        $platform->registerDoctrineTypeMapping('varbinary', 'string');
        $platform->registerDoctrineTypeMapping('tinyblob', 'string');
    }

    /**
     * @return Message
     */
    private function mailMessage()
    {
        $message = new Message();
        $message->setEncoding($this->_config['mail']['encoding']);
        return $message;
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'AuthService' => function() {
                    $authService = new AuthenticationService();
                    $authService->setStorage(new Session('obUser'));
                    return $authService;
                },
                'AclService' => function(ServiceLocatorInterface $srvManager) {
                    $srvAcl = new Acl();
                    $aclRules = $srvManager->get('acl_rules');
                    $aclApp = $srvManager->get('acl_app');
                    $srvAcl->setArrRules($aclRules['permissoes']);
                    $srvAcl->setArrApp($aclApp['permissoes']);
                    /**
                     * @TODO
                     * implentar quando utilizar token
                     */
//                    $config = $srvManager->get('Config');
//                    $segurancaOptions = $config['moduloSeguranca'];
//                    $srvAcl->setSegurancaOptions($segurancaOptions);
                    return $srvAcl;
                },
                'RenderResult' => function(ServiceLocatorInterface $serviceManager) {
                    $srvRenderResult = new RenderResult();
                    return $srvRenderResult;
                },
                'Application\Auth\Adapter' => function ($service) {
                    return new Adapter($service->get('Doctrine\ORM\EntityManager'));
                },

            ),
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
            ),
        );
    }
}

