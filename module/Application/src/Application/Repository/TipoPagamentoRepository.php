<?php

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\View\Helper\Criptografia;

class TipoPagamentoRepository extends EntityRepository{
    
    public function fetchPairs()
    {
        return $this->findAll();
        $entities = $this->findAll();
        $array = array();
        foreach ($entities as $entity){
            $array[$entity->getIdTipoPagamento()] = $entity->getNoTipoPagamento();
        }
        return $array;
    }
}