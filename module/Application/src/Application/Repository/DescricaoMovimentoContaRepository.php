<?php

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class DescricaoMovimentoContaRepository extends EntityRepository{

    public function fetchPairs()
    {
        $descMovimentoConta = $this->findBy(array(), array('noDescMovimentacaoConta' => 'ASC'));
        $data = array();
        /* @var $desc \Application\Entity\DescricaoMovimentoConta */
        foreach($descMovimentoConta as $desc){
            $data[$desc->getIdDescMovimentoConta()] = $desc->getNoDescMovimentacaoConta();
        }
        return $data;
    }

    /**
     * Busca as descrições que ainda não foram cadastradas para colocar na combo
     * @param int $idConta
     * @param int $nuAnoMes
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function buscarDescricaoNaoCadastrada($idConta, $nuAnoMes)
    {
        $sql = "
            select dmc.id_desc_movimento_conta, dmc.no_desc_movimentacao_conta from geccal.tb_descricao_movimento_conta dmc
            left join (
                select mcd.id_desc_movimento_conta from geccal.tb_movimentacao_conta_descricao mcd
                inner join geccal.tb_movimentacao_conta mc on mc.id_movimentacao_conta = mcd.id_movimentacao_conta
                where mc.id_conta = :idConta and mc.nu_ano_mes = :nuAnoMes
            ) mcd on mcd.id_desc_movimento_conta = dmc.id_desc_movimento_conta
            where mcd.id_desc_movimento_conta is null order by dmc.no_desc_movimentacao_conta asc
        ";
        $conn = $this->getEntityManager()->getConnection();
        $statement = $conn->prepare($sql);
        $statement->bindValue('idConta', $idConta);
        $statement->bindValue('nuAnoMes', $nuAnoMes);
        $statement->execute();
        return $statement->fetchAll();

    }

}