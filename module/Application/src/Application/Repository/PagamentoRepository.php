<?php

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class PagamentoRepository extends EntityRepository{


    public function removerPagamentoPorRegistro($idRegistroCaixa)
    {
        $qb = $this->createQueryBuilder('PG');
        $query = $qb->delete()
            ->where($qb->expr()->eq('PG.registroCaixa', ':idRegistroCaixa'))
            ->setParameter(':idRegistroCaixa', $idRegistroCaixa);

        return $query->getQuery()->execute();

    }

	
}