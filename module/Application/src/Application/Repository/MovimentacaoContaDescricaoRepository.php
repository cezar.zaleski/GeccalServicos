<?php

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;

class MovimentacaoContaDescricaoRepository extends EntityRepository
{
    public function listarMovimentoDescricao($idConta, $nuAnoMes)
    {
        $qb = $this->createQueryBuilder('MCD');
        $query = $qb->select('MCD')
            ->innerJoin('MCD.movimentacaoConta', 'MC')
            ->innerJoin('MC.conta', 'C')
            ->where($qb->expr()->eq('C.idConta',':idConta'))
            ->andWhere($qb->expr()->eq('MC.nuAnoMes',':nuAnoMes'))
            ->setParameter('idConta', $idConta)
            ->setParameter('nuAnoMes', $nuAnoMes);
        return $query->getQuery()->getResult();

    }

    /**
     * Remove todas as descriçoes de uma movimentaçao
     * @param int $idMovConta
     * @param int $idDescMovConta
     * @return mixed
     */
    public function removerMovimentacao($idMovConta, $idDescMovConta)
    {
        $qb = $this->createQueryBuilder('MCD');
        $query = $qb->delete()
            ->where($qb->expr()->eq('MCD.movimentacaoConta', ':idMovConta'))
            ->andWhere($qb->expr()->eq('MCD.descricaoMovimentoConta', ':idDescMovConta'))
            ->setParameter(':idMovConta', $idMovConta)
            ->setParameter(':idDescMovConta', $idDescMovConta);

        return $query->getQuery()->execute();

    }
	
	
}