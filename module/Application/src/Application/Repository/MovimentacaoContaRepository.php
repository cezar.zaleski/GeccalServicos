<?php

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;

class MovimentacaoContaRepository extends EntityRepository
{
    public function buscarMovimentacaoPorConta2($idConta)
    {
        $qb = $this->createQueryBuilder('MC');
        $query = $qb->select('MC.idMovimentacaoConta, MC.nuAnoMes', 'C.noConta', 'C.nuOp')
            ->innerJoin('MC.conta', 'C')
            ->where($qb->expr()->eq('MC.conta',':idConta'))
            ->setParameter('idConta', $idConta);
        return $query->getQuery()->getArrayResult();

    }

    public function buscarMovimentacaoPorConta($idConta)
    {
        $sql = "
            select
            mcc.id_movimentacao_conta, mcc.nu_ano_mes,c.no_conta,c.nu_op,
              coalesce((select sum(nu_valor) from geccal.tb_movimentacao_conta_descricao mcd
              where mcc.id_movimentacao_conta = mcd.id_movimentacao_conta and mcd.id_tipo_registro_caixa = 1),0) -
            coalesce((select sum(nu_valor) from geccal.tb_movimentacao_conta_descricao mcd
              where mcc.id_movimentacao_conta = mcd.id_movimentacao_conta and mcd.id_tipo_registro_caixa = 2),0) nu_saldo
            from geccal.tb_movimentacao_conta mcc
            inner join geccal.tb_conta c on mcc.id_conta = c.id_conta
            where c.id_conta = :idConta;
        ";
        $conn = $this->getEntityManager()->getConnection();
        $statement = $conn->prepare($sql);
        $statement->bindValue('idConta', $idConta);
        $statement->execute();
        return $statement->fetchAll();

    }
	
	
}