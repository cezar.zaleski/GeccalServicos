<?php

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class RegistroCaixaRepository extends EntityRepository{


    /**
     * Recupera todos os registros de caixa
     * @return array
     */
    public function findAllRegistro()
    {
        $qb = $this->createQueryBuilder('RG');
        $query = $qb->select(
            'RG.idRegistroCaixa as id_registro_caixa, RG.dtRegistro as dt_registro, RG.noDescricao as no_descricao,
             P.noPessoa as no_pessoa, COALESCE(sum(PG.nuValor), 0) as nu_valor, RG.stAtivo, TR.noTipoRegistroCaixa'
            )
            ->innerJoin('RG.usuario', 'U')
            ->innerJoin('U.pessoa', 'P')
            ->innerJoin('RG.tipoRegistroCaixa', 'TR')
            ->leftJoin(
                'Application\Entity\Pagamento',
                'PG',
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq('PG.registroCaixa', 'RG.idRegistroCaixa'),
                    $qb->expr()->eq('PG.stAtivo', true)
                )
            )
            ->where($qb->expr()->eq('RG.stAtivo', true))
            ->groupBy('RG.dtRegistro, RG.noDescricao, P.noPessoa, TR.noTipoRegistroCaixa');

        return $query->getQuery()->getResult();
    }

    /**
     * Recupera o registro de caixa pelo identificador
     * @param int $idRegistroCaixa
     * @return array
     */
    public function findByRegistro($idRegistroCaixa)
    {
        $qb = $this->createQueryBuilder('RG');
        $query = $qb->select(
            'RG.idRegistroCaixa as id_registro_caixa, RG.dtRegistro, RG.noDescricao, DL.idDescLancamento,
             TR.idTipoRegistroCaixa'
            )
            ->leftJoin(
                'Application\Entity\Pagamento',
                'PG',
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq('PG.registroCaixa', 'RG.idRegistroCaixa'),
                    $qb->expr()->eq('PG.stAtivo', true)
                )
            )
            ->innerJoin('RG.descLancamento', 'DL')
            ->innerJoin('RG.tipoRegistroCaixa', 'TR')
            ->where($qb->expr()->eq('RG.stAtivo', true))
            ->andWhere($qb->expr()->eq('RG.idRegistroCaixa', ':idRegistroCaixa'))
            ->setParameter('idRegistroCaixa', $idRegistroCaixa);

        return $query->getQuery()->getResult();
    }

    public function findAll()
    {
        return $this->findBy(array('stAtivo'=> TRUE), array('dtRegistro'=> 'ASC'));
    }

    /**
     * Busca os meses do ano que existem registro ce caixa cadastrado para montar relação de relatórios disponíveis
     * @return array
     */
    public function findByRelatorios()
    {
        $queryBase = $this->createQueryBuilder('R');
		$query = $queryBase->select("MONTH(R.dtRegistro) as nu_mes, YEAR(R.dtRegistro) as nu_ano")
		->andWhere($queryBase->expr()->eq('R.stAtivo', ':stAtivo'))
                ->setParameter(':stAtivo', TRUE)
		->groupBy('nu_mes, nu_ano');
        return $query->getQuery()->getArrayResult();
    }
    /**
     * Realiza busca dos registros que farão parte do 
     * Relatório de Registro de Caixa
     * @param type $mes
     * @param type $ano
     * @return type Object
     */
    public function findByRelatorioCaixa($mes, $ano) 
    {
        $qb = $this->createQueryBuilder('RG');
        $query = $qb->select(
            'RG.idRegistroCaixa, RG.dtRegistro, RG.noDescricao,
             P.noPessoa, COALESCE(sum(PG.nuValor), 0) as nuValor, RG.stAtivo,
             D.coDescLancamento, D.coDescLancamento, T.idTipoRegistroCaixa'
        )
            ->innerJoin('RG.usuario', 'U')
            ->innerJoin('U.pessoa', 'P')
            ->innerJoin('RG.descLancamento', 'D')
            ->innerJoin('RG.tipoRegistroCaixa', 'T')
            ->leftJoin(
                'Application\Entity\Pagamento',
                'PG',
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq('PG.registroCaixa', 'RG.idRegistroCaixa'),
                    $qb->expr()->eq('PG.stAtivo', true)
                )
            )
            ->where($qb->expr()->eq('RG.stAtivo', true))
            ->andWhere($qb->expr()->eq('MONTH(RG.dtRegistro)', ':mes'))
            ->andWhere($qb->expr()->eq('YEAR(RG.dtRegistro)', ':ano'))
            ->setParameter(':mes', $mes)
            ->setParameter(':ano', $ano)
            ->groupBy('RG.dtRegistro, RG.noDescricao, P.noPessoa');

        return $query->getQuery()->getResult();
    }

    /**
     * Busca os registros que comporão o relatório de movimento de caixa
     * @param int $mes
     * @param int $ano
     * @return array
     */
    public function findByRelatorioMovimentoCaixa($mes, $ano)
    {
        $qb = $this->createQueryBuilder('RG');
        $query = $qb->select(
            'RG.idRegistroCaixa, RG.dtRegistro, T.noTipoRegistroCaixa, TP.idTipoPagamento,
             P.noPessoa, sum(PG.nuValor) as nuValor,
             T.idTipoRegistroCaixa'
        )
            ->innerJoin('RG.usuario', 'U')
            ->innerJoin('U.pessoa', 'P')
            ->innerJoin('RG.tipoRegistroCaixa', 'T')

            ->leftJoin(
                'Application\Entity\Pagamento',
                'PG',
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq('PG.registroCaixa', 'RG.idRegistroCaixa'),
                    $qb->expr()->eq('PG.stAtivo', true)
                )
            )
            ->innerJoin('PG.tipoPagamento', 'TP')
            ->where($qb->expr()->eq('RG.stAtivo', true))
            ->andWhere($qb->expr()->eq('MONTH(RG.dtRegistro)', ':mes'))
            ->andWhere($qb->expr()->eq('YEAR(RG.dtRegistro)', ':ano'))
            ->setParameter(':mes', $mes)
            ->setParameter(':ano', $ano)
            ->groupBy('RG.idRegistroCaixa, RG.dtRegistro, T.noTipoRegistroCaixa, TP.idTipoPagamento,
             P.noPessoa, T.idTipoRegistroCaixa');

        return $query->getQuery()->getResult();
    }

    /**
     * Retorna o saldo até a data informada como parâmetro
     * @param \DateTime $dtParamento
     * @return array
     */
    public function saldo(\DateTime $dtParamento)
    {
        $qb = $this->createQueryBuilder('RG');
        $query = $qb->select(
            'COALESCE(SUM(CASE WHEN (RG.tipoRegistroCaixa = 1 and PG.tipoPagamento = 1) THEN PG.nuValor ELSE 0 END) -
             SUM(CASE WHEN (RG.tipoRegistroCaixa = 2 and PG.tipoPagamento = 1) THEN PG.nuValor ELSE 0 END),0) AS slDinheiro,
             COALESCE(SUM(CASE WHEN (RG.tipoRegistroCaixa = 1 and PG.tipoPagamento = 2) THEN PG.nuValor ELSE 0 END) -
             SUM(CASE WHEN (RG.tipoRegistroCaixa = 2 and PG.tipoPagamento = 2) THEN PG.nuValor ELSE 0 END),0) AS slCheque,
             COALESCE(SUM(CASE WHEN (RG.tipoRegistroCaixa = 1 and PG.tipoPagamento = 3) THEN PG.nuValor ELSE 0 END) -
             SUM(CASE WHEN (RG.tipoRegistroCaixa = 2 and PG.tipoPagamento = 3) THEN PG.nuValor ELSE 0 END),0) AS slDebito,
             COALESCE(SUM(CASE WHEN (RG.tipoRegistroCaixa = 1 and PG.tipoPagamento = 4) THEN PG.nuValor ELSE 0 END) -
             SUM(CASE WHEN (RG.tipoRegistroCaixa = 2 and PG.tipoPagamento = 4) THEN PG.nuValor ELSE 0 END),0) AS slCredito'
        )
            ->innerJoin('RG.tipoRegistroCaixa', 'T')
            ->innerJoin(
                'Application\Entity\Pagamento',
                'PG',
                Join::WITH,
                $qb->expr()->andX(
                    $qb->expr()->eq('PG.registroCaixa', 'RG.idRegistroCaixa'),
                    $qb->expr()->eq('PG.stAtivo', true)
                )
            )
            ->where($qb->expr()->eq('RG.stAtivo', true))
            ->andWhere($qb->expr()->lt('RG.dtRegistro', ':dtRegistro'))
            ->setParameter(':dtRegistro', $dtParamento);

        return $query->getQuery()->getResult();
    }
	
}