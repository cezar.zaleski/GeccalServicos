<?php

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\View\Helper\Criptografia;

class TipoRegistroCaixaRepository extends EntityRepository{
    
    public function fetchPairs()
    {
        $entities = $this->findAll();
        $array = array();
        foreach ($entities as $entity){
            $array[$entity->getIdTipoRegistroCaixa()] = $entity->getNoTipoRegistroCaixa();
        }
        return $array;
    }
	
}