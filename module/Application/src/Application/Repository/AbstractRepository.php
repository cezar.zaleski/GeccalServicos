<?php

namespace Application;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

abstract class AbstractRepository extends EntityRepository
{
    /**
     *
     * @param  QueryBuilder $queryBuilder
     * @param  int $offset
     * @param  int $maxResult
     *
     * @return Paginator
     */
    protected function paginate($queryBuilder, $offset = 1, $maxResult = 18)
    {
        $queryBuilder->setMaxResults($maxResult);
        $queryBuilder->setFirstResult(($offset - 1) * $maxResult);
        return new Paginator($queryBuilder, false);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param int $offset
     * @param int $maxResult
     * @return array
     */
    protected function formattedPaginateResults($queryBuilder, $offset = 1, $maxResult = 18)
    {
        $paginate = $this->paginate($queryBuilder, $offset, $maxResult);
        $countAllResults =  $paginate->count();

        return array(
            'countResults' => $countAllResults,
            'currentPage' => $offset,
            'itemsPerPage' => $maxResult,
            'pagesCount' => ceil($countAllResults / $maxResult),
            'results' => $paginate->getQuery()->getResult()
        );
    }
}
