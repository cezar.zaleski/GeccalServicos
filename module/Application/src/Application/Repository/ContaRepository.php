<?php

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;

class ContaRepository extends EntityRepository {



    public function fetchPairs()
    {
        $contas = $this->findAll();
        $data = array();
        foreach($contas as $conta){
            $data[$conta->getIdConta()] = $conta->getNoconta();
        }
        return $data;
    }

    /**
     * Retorna todas as contas com o ultimo saldo
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function listarConta()
    {
        $sql = "SELECT
                  c.id_tipo_conta,c.id_conta,id_movimentacao_conta,
                       COALESCE((SELECT SUM(nu_valor) FROM geccal.tb_movimentacao_conta_descricao mcd
                       WHERE mcc.id_movimentacao_conta = mcd.id_movimentacao_conta AND mcd.id_tipo_registro_caixa = 1),0) -
                       COALESCE((SELECT SUM(nu_valor) FROM geccal.tb_movimentacao_conta_descricao mcd
                       WHERE mcc.id_movimentacao_conta = mcd.id_movimentacao_conta AND mcd.id_tipo_registro_caixa = 2),0) nu_saldo,
                  c.id_conta, c.nu_op, c.no_conta
                FROM geccal.tb_conta c
                  LEFT JOIN
                  (SELECT id_movimentacao_conta, id_conta FROM geccal.tb_movimentacao_conta ORDER BY nu_ano_mes DESC LIMIT 1)
                  mcc ON mcc.id_conta = c.id_conta";
        $conn = $this->getEntityManager()->getConnection();
        $statement = $conn->prepare($sql);
        $statement->execute();
        return $statement->fetchAll();

    }

    /**
     * Retona o saldo das contas por mes
     * @param int $nuAnoMes
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function saldoContaMesBalancete($nuAnoMes)
    {
        $sql = "
                SELECT c.id_tipo_conta,nu_ano_mes,
                         COALESCE((SELECT SUM(nu_valor) FROM geccal.tb_movimentacao_conta_descricao mcd
                         WHERE mcc.id_movimentacao_conta = mcd.id_movimentacao_conta AND mcd.id_tipo_registro_caixa = 1),0) -
                         COALESCE((SELECT SUM(nu_valor) FROM geccal.tb_movimentacao_conta_descricao mcd
                         WHERE mcc.id_movimentacao_conta = mcd.id_movimentacao_conta AND mcd.id_tipo_registro_caixa = 2),0) nu_saldo
                  FROM geccal.tb_conta c
                    LEFT JOIN geccal.tb_movimentacao_conta mcc ON mcc.id_conta = c.id_conta AND mcc.nu_ano_mes = :nuAnoMes
                  GROUP BY c.id_tipo_conta";
        $conn = $this->getEntityManager()->getConnection();
        $statement = $conn->prepare($sql);
        $statement->bindValue('nuAnoMes', $nuAnoMes);
        $statement->execute();
        return $statement->fetchAll();
    }

    /**
     * Retona o saldo das contas por mes
     * @param int $nuAnoMes
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function saldoContaAnoBalancete($nuAnoMes)
    {
        $sql = "
                SELECT c.id_tipo_conta,nu_ano_mes,
                         COALESCE((SELECT SUM(nu_valor) FROM geccal.tb_movimentacao_conta_descricao mcd
                         WHERE mcc.id_movimentacao_conta = mcd.id_movimentacao_conta AND mcd.id_tipo_registro_caixa = 1),0) -
                         COALESCE((SELECT SUM(nu_valor) FROM geccal.tb_movimentacao_conta_descricao mcd
                         WHERE mcc.id_movimentacao_conta = mcd.id_movimentacao_conta AND mcd.id_tipo_registro_caixa = 2),0) nu_saldo
                  FROM geccal.tb_conta c
                    LEFT JOIN geccal.tb_movimentacao_conta mcc ON mcc.id_conta = c.id_conta AND mcc.nu_ano_mes = :nuAnoMes
                  GROUP BY c.id_tipo_conta";
        $conn = $this->getEntityManager()->getConnection();
        $statement = $conn->prepare($sql);
        $statement->bindValue('nuAnoMes', $nuAnoMes);
        $statement->execute();
        return $statement->fetchAll();
    }

}
