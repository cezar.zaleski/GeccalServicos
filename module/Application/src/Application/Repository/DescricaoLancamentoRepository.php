<?php

namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\View\Helper\Criptografia;

class DescricaoLancamentoRepository extends EntityRepository{
    
    public function fetchPairs()
    {
        $descLancamentos = $this->findBy(
            array('tipoLancamento' => array(1,3,4)),
            array('coDescLancamento' => 'ASC')
        );
        $data = array();
        foreach($descLancamentos as $descLancamento){
            $data[$descLancamento->getIdDescLancamento()] = $descLancamento->getNoDescLancamento();
        }
        return $data;
    }

    /**
     * Retorna os registros para geraçao do relatorio de balancete
     * @param int $mes
     * @param int $ano
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findByBalancete($mes, $ano)
    {
        $sql = "SELECT th.no_tipo_historico, th.id_tipo_historico, tl.no_tipo_lancamento, tl.id_tipo_lancamento,
                  ds.co_desc_lancamento, ds.no_desc_lancamento, ds.id_desc_lancamento,
                  SUM(CASE WHEN month(registro.dt_registro) = :mes  THEN registro.nu_valor ELSE 0 END) as val_nu_mes,
                  COALESCE(sdc.nu_valor,0) + COALESCE(registro.nu_valor,0) as val_nu_ano
                FROM geccal.tb_tipo_lancamento tl
                  INNER JOIN geccal.tb_tipo_historico th on th.id_tipo_historico = tl.id_tipo_historico
                  LEFT JOIN geccal.tb_descricao_lancamento ds on tl.id_tipo_lancamento = ds.id_tipo_lancamento
                  LEFT JOIN  geccal.tb_saldo_desc_lancamento sdc on sdc.id_desc_lancamento = ds.id_desc_lancamento
                  LEFT JOIN (
                              SELECT rg.id_desc_lancamento, rg.dt_registro, SUM(pg.nu_valor) as nu_valor FROM geccal.tb_registro_caixa rg
                                INNER JOIN geccal.tb_pagamento pg on pg.id_registro_caixa = rg.id_registro_caixa
                              WHERE YEAR(rg.dt_registro) = :ano
                              GROUP BY rg.id_desc_lancamento, rg.dt_registro) registro on registro.id_desc_lancamento = ds.id_desc_lancamento
                GROUP BY th.no_tipo_historico, tl.no_tipo_lancamento, ds.co_desc_lancamento, ds.no_desc_lancamento
                ORDER BY th.no_tipo_historico, th.id_tipo_historico,
                  tl.no_tipo_lancamento, tl.id_tipo_lancamento, ds.nu_ordem";
        $conn = $this->getEntityManager()->getConnection();
        $statement = $conn->prepare($sql);
        $statement->bindValue('mes', $mes);
        $statement->bindValue('ano', $ano);
        $statement->execute();
        return $statement->fetchAll();

    }
	
}