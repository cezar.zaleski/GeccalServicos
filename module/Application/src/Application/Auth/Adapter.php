<?php

namespace Application\Auth;

use Application\Service\AbstractService;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Crypt\Password\Bcrypt;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\Log\Filter\Priority as LogFilter;
use Application\Service\Exception\ServiceException;

class Adapter extends AbstractService implements AdapterInterface {

    private $em;
    private $bcrypt;
    private $data;
    public $ip;

    function __construct($em)
    {
        $this->em = $em;
        $this->bcrypt = new Bcrypt();
        $this->bcrypt->setCost(11);
        $this->bcrypt->setSalt('Grupo Espírita Cristão à Caminho da Luz');
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }

    public function authenticate()
    {
        /* @var $repo \Application\Repository\UsuarioRepository */
        $repo = $this->em->getRepository('Application\Entity\Usuario');
        $data = $this->data;
        $senha = $this->bcrypt->create($data['noSenha']);
        $user = $repo->findByUsuarioAndSenha($data['noLogin'], $senha);
        if ($user) {
            $this->sessionStart($user);
            $this->recordLog(TRUE);
            return $user;
        }
        throw new ServiceException('E005', 401);
    }

    private function sessionStart($user)
    {
        UsuarioAuth::_populatyIdentity($user);
    }


    private function recordLog($state)
    {
        $messages = $this->getData();
        $datetime = new \DateTime('now');
        $logger = new Logger();
        $writer = new Stream(__DIR__ . '/../../../../../data/log/usuario/' .
            'geccal' . $datetime->format('Y-m-d') . '.log');
        $logger->addWriter($writer);
        $filter = new LogFilter(Logger::DEBUG);
        $writer->addFilter($filter);
        foreach ($messages as $i => $message) {
            if ($i == 'senha' && $state) {
                continue;
            }
            $message = str_replace("\n", "\n", $message);
            $logger->debug("Autentication: $i: $message");
        }
        $logger->alert("IP: " . $this->ip);
        ($state) ? $logger->alert('Status: Usuário Logado \n') :
            $logger->alert('Status: Falha ao Logar\n');
    }

}