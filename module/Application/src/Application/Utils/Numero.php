<?php

namespace Application\Utils;

use Zend\Validator\AbstractValidator;

class Numero extends AbstractValidator
{

    const NOT_NUMERO = 'notNumero';

    protected $_messageTemplates = array(
        self::NOT_NUMERO => "'%value%' não número válido.",
    );


    /**
     *
     * @param   mixed $numero
     * @return  boolean
     * @throws  Zend_Valid_Exception If validation of $numero is impossible
     * @see ValidatorInterface::isValid()
     */
    public function isValid($numero)
    {
        return true;
    }

    public function formataNumero($numero, $set = null, $decimal = 2)
    {
        if (!$set) {
            $result = str_replace(',', '.', str_replace('.', '', $numero));
        } else {
            $result = number_format($numero, $decimal, ',', '.');
        }
        return $result;
    }
}
