<?php
/**
 * Ministério da Educação do Brasil
 *
 * Este arquivo é parte integrante dos sistemas do Ministério da Educação do
 * Brasil. Antes da utilização do mesmo consulte a instituição.
 *
 * Este archivo de código fuente pertenece al Ministerio de Educación de Brasil.
 * Antes de usarlo, póngase en contacto con la institución.
 *
 * This source file belongs to Ministry of Education of Brazil. Before using it,
 * contact the institution.
 *
 * @category   Fies
 * @package    Base
 * @version    1.0.0
 */

namespace Application\Utils;

class Semestre
{
    public static function atual()
    {
        $semestre = '1';
        if (date('m') > 6) {
            $semestre = '2';
        }

        return date('Y') . $semestre;
    }

    public static function formatar($nuSemestreAno, $separador = "°/")
    {
        $semestre = substr($nuSemestreAno, 4) . $separador . substr($nuSemestreAno, 0, 4);
        return $semestre;
    }

    public static function anterior($nuSemestreAno)
    {
        $nuSemestre = substr($nuSemestreAno, 4);
        $nuAno = substr($nuSemestreAno, 0, 4);

        if ($nuSemestre == 1) {
            return ((int)$nuAno - 1) . 2;
        } else {
            return (int)$nuAno . 1;
        }
    }

    public static function posterior($nuSemestreAno)
    {
        $nuSemestre = substr($nuSemestreAno, 4);
        $nuAno = substr($nuSemestreAno, 0, 4);

        if ($nuSemestre == 1) {
            return (int)$nuAno . 2;
        } else {
            return (int)($nuAno + 1) . 1;
        }
    }

    public static function dataInicio($nuSemestreAno)
    {
        $ano = substr($nuSemestreAno, 0, 4);
        $semestre = substr($nuSemestreAno, 4);

        if ($semestre == 1) {
            return \DateTime::createFromFormat('d/m/Y', '01/01/' . $ano);
        } else {
            return \DateTime::createFromFormat('d/m/Y', '01/07/' . $ano);
        }
    }

    public static function dataFim($nuSemestreAno)
    {
        $ano = substr($nuSemestreAno, 0, 4);
        $semestre = substr($nuSemestreAno, 4);

        if ($semestre == 1) {
            return \DateTime::createFromFormat('d/m/Y', '30/06/' . $ano);
        } else {
            return \DateTime::createFromFormat('d/m/Y', '31/12/' . $ano);
        }
    }
}
