<?php

namespace Application\Utils;


use Application\Service\AbstractService;
use Application\Service\Exception\ServiceException;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\View\Model\JsonModel;

class RenderResult extends AbstractService
{
    /** @var  Request $request */
    private $request;
    /** @var Response $response */
    private $response;

    /**
     * @param array $arrData
     * @return mixed|null|JsonModel
     */
    public function renderResult(array $arrData)
    {
        /* @var $srvSerializar \JMS\Serializer\Serializer */
        $srvSerializar = $this->getServiceManager()->get('jms_serializer.serializer');
        return $srvSerializar->serialize($arrData, 'json');
    }

    public function renderXml(array $arrData, $erro = false)
    {
        $xml = $this->arrayToXml($arrData, new \SimpleXMLElement('<retorno/>'), $erro);
        $response = $this->getResponse();
        $response->getHeaders()->addHeaderLine('Content-Type', 'application/xml');
        return $response->setContent($xml->asXML());
    }

    public function arrayToXml($arrData, \SimpleXMLElement $xml, $erro = false, &$prevKey = null)
    {
        foreach ($arrData as $k => $v) {
            if (is_numeric($k)) {
                $k = ($erro == true) ? 'mensagem' : 'child';
                if (is_string($prevKey) && is_array($v)) {
                    $k = $prevKey;
                }
            }
            $prevKey = is_string($k) && is_array($v) ? $k : $prevKey;

            if (is_array($v)) {
                $add = true;
                foreach ($v as $kFilho => $vFilho) {
                    if (is_numeric($kFilho)) {
                        $this->arrayToXml($vFilho, $xml->addChild($k), $erro, $prevKey);
                        $add = false;
                    }
                }
                if ($add) {
                    $this->arrayToXml($v, $xml->addChild($k), $erro, $prevKey);
                }
            } else {
                $xml->addChild($k, $v);
            }
        }
        return $xml;
    }


    /**
     * @param ServiceException $e
     * @return Response
     */
    public function errorException(ServiceException $e)
    {
        $errorCode = $e->getCode()? : 405;
        $response = $this->getResponse();
        $response->setStatusCode($errorCode);
        $errorMsg = json_decode($e->getMessage(), true);
        $errorMsg = $errorMsg? : array('mensagem' =>
            array(
                'coMensagem' => $e->getMessage(),
                'tpMensagem' => AbstractService::ERROR_MSG,
                'msg' => $this->translate($e->getMessage(), $e->getExecptionParams()),
            ),
        );
        $result = $this->renderResult(array('listaMensagem' => $errorMsg, 'stSucesso' => 0));
        $response->setContent($result);
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-type', 'application/json');
        $headers->addHeaderLine('Access-Control-Allow-Origin', '*');
        $response->setHeaders($headers);
        return $response;
    }

    /**
     * Get request object
     *
     * @return Request
     */
    public function getRequest()
    {
        if (!$this->request) {
            $this->request = new Request();
        }

        return $this->request;
    }

    /**
     * Get response object
     *
     * @return Response
     */
    public function getResponse()
    {
        if (!$this->response) {
            $this->response = new Response();
        }

        return $this->response;
    }
}