<?php

namespace Application\Utils;

class Utils
{

    /**
     * Retorna valores de arrays de acordo com chaves informada
     * @param type $arrayKeys
     * @param type $arrayData
     * @return type
     */
    public static function retornaDadosArrayPorKey($arrayKeys, $arrayData)
    {
        $result_array = array_intersect_key($arrayData, array_flip($arrayKeys));

        return $result_array;
    }
}
