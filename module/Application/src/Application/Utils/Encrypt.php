<?php

namespace Application\Utils;

use Zend\Crypt\BlockCipher;

class Encrypt
{

    private $_saltApplication = 'b86duwigk9w04kg4gowsg8kco8sskks';

    public function encryptPassword($password)
    {
        $hashSenha = hash('sha1', $password . $this->_saltApplication);
        for ($i = 0; $i < 10000; $i++) {
            $hashSenha = hash('sha1', $hashSenha);
        }
        return $hashSenha;
    }


    public function encryptMessage($secretMessage)
    {
        $blockCipher = BlockCipher::factory(
            'mcrypt',
            array(
                'algo' => 'aes',
                'mode' => 'cfb',
                'hash' => 'sha1'
            )
        );
        $blockCipher->setKey($this->_saltApplication);
        $hashSenha = $blockCipher->encrypt($secretMessage);
        return $hashSenha;
    }

    public function decryptMessage($secretMessage)
    {
        $blockCipher = BlockCipher::factory(
            'mcrypt',
            array(
                'algo' => 'aes',
                'mode' => 'cfb',
                'hash' => 'sha1'
            )
        );
        $blockCipher->setKey($this->_saltApplication);
        $hashSenha = $blockCipher->decrypt($secretMessage);
        return $hashSenha;
    }
}
