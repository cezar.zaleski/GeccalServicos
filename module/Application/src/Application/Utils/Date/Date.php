<?php

namespace Application\Utils\Date;

/**
 * Classe responsável por tratamento de datas
 *
 * Toda data no sistema deverá ser um objeto dessa classe.
 * Utilizando ela a data poderá ser alterada de formato sem grandes
 * esforços.
 *
 * @package Core
 * @subpackage Date
 */
class Date
{

    private $intDate = null;

    public function __construct($strDate = null, $strFormat = null)
    {
        if (!is_null($strDate)) {
            self::setDate($strDate, $strFormat);
        }
    }

    public function setDate($strDate, $strFormat = null)
    {
        if ($strFormat) {
            self::setDateByFormat($strDate, $strFormat);
        } else {
            //dd/mm/yyyy ou dd/mm/yy ou d/m/yy
            $regexFormattedDate = '#^ (?P<d>\d{1,2} ) / (?P<m>\d{1,2} ) / (?P<y>\d{2,4})$#x';

            //yyyy-mm-dd
            $regexDataApplicationDate = '#^ (?P<y>\d{4}) (?:\-|\/) (?P<m> \d{2}) (?:\-|\/) (?P<d> \d{2}) (?: \s (?P<h> \d{2} ) \: (?P<i> \d{2} ) \: (?P<s> \d{2} ) (?:\.\d+)? )?  $#x';

            //hh:ii:ss
            $regexTime = '#^ (?P<h> \d{2} ) \: (?P<i>\d{2} ) \: (?P<s>\d{2}) $#x';

            //timestamp
            $regexTimestamp = '#^ (?P<ut> \d+) $#x';

            //ssd
            $regexSsdDate = '#^ (?P<y>\d{4}) (?:\-|\/) (?P<m> \d{2}) (?:\-|\/) (?P<d> \d{2}).*?$#x';

            if (preg_match($regexFormattedDate, $strDate, $arrValores)) {
                self::setDateByArray($arrValores);
            } else {
                if (preg_match($regexDataApplicationDate, $strDate, $arrValores)) {
                    self::setDateByArray($arrValores);
                } else {
                    if (preg_match($regexTime, $strDate, $arrValores)) {
                        self::setDateByArray($arrValores);
                    } else {
                        if (preg_match($regexTimestamp, $strDate, $arrValores)) {
                            self::setDateByArray($arrValores);
                        } else {
                            if (preg_match($regexSsdDate, $strDate, $arrValores)) {
                                self::setDateByArray($arrValores);
                            } else {
                                return (false);
                            }
                        }
                    }
                }
            }
        }
    }

    private function setDateByFormat($strDate, $strFormat)
    {
        $strFormatOriginal = $strFormat;
        $strFormat = str_replace('\\', '\\\\', $strFormat);
        $strFormat = str_replace('#', '', $strFormat);
        $strFormat = str_replace(':', '\:', $strFormat);
        $strFormat = str_replace('(', '\(', $strFormat);
        $strFormat = str_replace(')', '\)', $strFormat);
        $strFormat = str_replace('[', '\[', $strFormat);
        $strFormat = str_replace(']', '\]', $strFormat);
        $strFormat = str_replace('{', '\{', $strFormat);
        $strFormat = str_replace('}', '\}', $strFormat);
        $strFormat = str_replace('^', '\^', $strFormat);
        $strFormat = str_replace('$', '\$', $strFormat);
        $strFormat = str_replace('.', '\.', $strFormat);
        $strFormat = str_replace('*', '\*', $strFormat);
        $strFormat = str_replace('+', '\+', $strFormat);
        $strFormat = str_replace('-', '\-', $strFormat);
        $strFormat = str_replace('d', '(?P<d>\d{2})', $strFormat);
        $strFormat = str_replace('j', '(?P<d>\d{1})', $strFormat);
        $strFormat = str_replace('m', '(?P<m>(?:0[1-9]|1[0-2]))', $strFormat);
        $strFormat = str_replace('n', '(?P<m>\d{1})', $strFormat);
        $strFormat = str_replace('y', '(?P<y>\d{2})', $strFormat);
        $strFormat = str_replace('Y', '(?P<y>\d{4})', $strFormat);
        $strFormat = str_replace('h', '(?P<h>\d{2})', $strFormat);
        $strFormat = str_replace('H', '(?P<h>\d{2})', $strFormat);
        $strFormat = str_replace('i', '(?P<i>\d{2})', $strFormat);
        $strFormat = str_replace('s', '(?P<s>\d{2})', $strFormat);
        $strFormat = str_replace(' ', '\s', $strFormat);

        $strFormat = '#^' . $strFormat . '$#x';

        if (preg_match($strFormat, $strDate, $arrValores)) {

            self::setDateByArray($arrValores);
            if ($this->getDate($strFormatOriginal) != $strDate) {
                $this->intDate = null;
                $this->throwDataInvalida($strDate);
            };
        } else {
            $this->throwDataInvalida($strDate);
            return (false);
        }
    }

    private function throwDataInvalida($strDate = null)
    {
        if (!$strDate) {
            $strDate = "informada";
        }
        //throw new DateInvalidException("A data $strDate não é válida.");
    }

    private function setDateByArray($arrValores)
    {
        if (array_key_exists('ut', $arrValores)) {
            $this->intDate = $arrValores['ut'];
        } else {
            $arrValores['d'] = (!array_key_exists("d", $arrValores)) ? null : $arrValores['d'];
            $arrValores['m'] = (!array_key_exists("m", $arrValores)) ? null : $arrValores['m'];
            $arrValores['y'] = (!array_key_exists("y", $arrValores)) ? null : $arrValores['y'];
            $arrValores['h'] = (!array_key_exists("h", $arrValores)) ? null : $arrValores['h'];
            $arrValores['i'] = (!array_key_exists("i", $arrValores)) ? null : $arrValores['i'];
            $arrValores['s'] = (!array_key_exists("s", $arrValores)) ? null : $arrValores['s'];
            $this->intDate = mktime(
                $arrValores['h'],
                $arrValores['i'],
                $arrValores['s'],
                $arrValores['m'],
                $arrValores['d'],
                $arrValores['y']
            );
        }

        if (!checkdate($arrValores['m'], $arrValores['d'], $arrValores['y'])) {
            $this->throwDataInvalida();
            $this->intDate = null;
            return false;
        }

        //echo date("d-m-Y H*i*s" , $this->intDate );
    }

    public function getDate($strFormat = null)
    {
        if ($strFormat == null) {
            return ($this->intDate);
        } elseif ($this->intDate) {
            return date($strFormat, $this->intDate);
        } else {
            return null;
        }
    }

    public function format($data, $formato = null)
    {
        if (!$data) {
            return;
        }

        $date = new \DateTime($this->retirarMascara($data), new \DateTimeZone('America/Sao_Paulo'));
        return $date->format($formato ? $formato : 'd/m/Y');
    }

    public function retirarMascara($data)
    {
        return preg_replace('/[^0-9]/', '', trim($data));
    }


    #####################################################
    #
    #	FUNCOES AUXILIARES 	DE DATA
    #
    #####################################################

    /**
     * Retorna array com os mêses do ano
     *
     * @param int $intMonthInKey
     * Verifica se o array deve ser montado com o número do mês como chave do array.
     *
     * @param unknown_type $keyNames
     * Caso o primeiro parâmetro seja falso, nesse parâmetro pode ser passado um array
     * com dois valores definindo os nomes das chaves do array, onde o primeiro valor é do
     * número do mês e o segundo o do texto do mês.
     *
     * @return array
     */
    public static function getArrayMonths($intMonthInKey = true, $keyNames = array('value', 'text'))
    {
        $arrMonths = array(
            1 => "Janeiro",
            2 => "Fevereiro",
            3 => "Março",
            4 => "Abril",
            5 => "Maio",
            6 => "Junho",
            7 => "Julho",
            8 => "Agosto",
            9 => "Setembro",
            10 => "Outubro",
            11 => "Novembro",
            12 => "Dezembro"
        );

        if ($intMonthInKey == false) {
            if (!is_array($keyNames) || count($keyNames) == 0) {
                $keyNames = array(0, 1);
            }
            if (count($keyNames) == 1) {
                $keyNames[1] = 1;
            }

            $arrValuesMonths = array();
            foreach ($arrMonths as $intMonth => $strMonth) {
                $arrValuesMonths[] = array($keyNames[0] => $intMonth, $keyNames[1] => $strMonth);
            }

            $arrMonths = $arrValuesMonths;
        }

        return ($arrMonths);
    }

    /**
     * Retorna Mês textual a partir do número do mes
     *
     * @param int $month Número do mês
     * @param boolean $abrv Define se retornará somente a abreviação do mês
     */
    public static function getTextualMonth($month, $abrv = false)
    {
        $arrMonths = self::getArrayMonths();
        $month = $arrMonths[$month];
        if ($abrv == true) {
            $month = substr($month, 0, 3);
        }

        return ($month);
    }

    /**
     * Retorna array com intervalo de anos
     *
     * Esse intervalo será contruído tendo como Application o ano atual. O primeiro valor determina o número de anos deverá
     * ser acrescido do ano atual e o segundo quantos anos deverão ser acrescidos do ano atual.
     *
     * @param int $yearBefore
     * Número de anos a menos do que o atual
     *
     * @param int $yearsAfter
     * Número de anos a mais do que o atual
     *
     * @return array
     */
    public static function getYearsRange($yearBefore = 10, $yearsAfter = 10, $intYearInKey = true, $keyName = 'ano')
    {
        $firstYear = date("Y") - $yearBefore;
        $lastYear = date("Y") + $yearsAfter;

        $arrAnos = array();
        for ($ano = $firstYear; $ano <= $lastYear; $ano++) {
            $arrAnos[$ano] = $ano;
        }

        if ($intYearInKey == false) {
            if ($keyName === null) {
                $keyName = 0;
            }

            $arrValuesYears = array();
            foreach ($arrAnos as $ano) {
                $arrValuesYears[] = array($keyName => $ano);
            }

            $arrAnos = $arrValuesYears;
        }

        return ($arrAnos);
    }

    /**
     * Busca último dia do mês
     *
     * @param int $year
     * É necessário passar esse valor por conta dos anos bissextos
     *
     * @param int $month
     *
     * @return int
     */
    public static function getLastDayOfMonth($year, $month)
    {
        for ($dia = 28; $dia < 32; ++$dia) {
            $data = mktime(0, 0, 0, $month, $dia, $year);
            if (date("n", $data) > $month) {
                return (--$dia);
            }
        }

        return (30);
    }

    /**
     * Retorna data em formato textual
     *
     * Retorna conforme o exemplo: 20 de setembro de 2008
     *
     * @param Date $date
     * @return unknown
     */
    public static function getTextualDate(Date $date = null)
    {
        if ($date == null) {
            $date = new Date(time());
        }
        $textualDate = $date->getDate("d") . " de " . self::getTextualMonth($date->getDate("n"))
            . " de " . $date->getDate("Y");
        return ($textualDate);
    }

    public function __toString()
    {
        return (string)$this->getDate('d/m/Y');
    }

    /**
     * Retorna objeto com das formatadas [d/m/Y]
     * @param type $dados
     * @return type
     */
    public static function formataDataRetorno($dados, $format='d/m/Y')
    {
        foreach ($dados as $key => $value) {
            $data = new \DateTime();
            if ($value instanceof $data) {
                $dados[$key] = $value->format($format);
            }
        }
        return $dados;
    }
}
