<?php

namespace Application\Utils;

use Zend\Validator\AbstractValidator;

class Cnpj extends AbstractValidator
{

    const NOT_CNPJ = 'notCnpj';

    protected $_messageTemplates = array(
        self::NOT_CNPJ => "'%value%' não é um CNPJ válido.",
    );

    public function isValid($cnpj)
    {
        $j = 0;
        for ($i = 0; $i < (strlen($cnpj)); $i++) {
            if (is_numeric($cnpj[$i])) {
                $num[$j] = $cnpj[$i];
                $j++;
            }
        }
        if (count($num) != 14) {
            $isCnpjValid = false;
        }
        if ($num[0] == 0 && $num[1] == 0 && $num[2] == 0 && $num[3] == 0 && $num[4] == 0 && $num[5] == 0 && $num[6] == 0 && $num[7] == 0 && $num[8] == 0 && $num[9] == 0 && $num[10] == 0 && $num[11] == 0) {
            $isCnpjValid = false;
        } else {
            $j = 5;
            for ($i = 0; $i < 4; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $j = 9;
            for ($i = 4; $i < 12; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            } else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[12]) {
                $isCnpjValid = false;
            }
        }
        if (!isset($isCnpjValid)) {
            $j = 6;
            for ($i = 0; $i < 5; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $j = 9;
            for ($i = 5; $i < 13; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            } else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[13]) {
                $isCnpjValid = false;
            } else {
                $isCnpjValid = true;
            }
        }
        if ($isCnpjValid === false) {
            $this->error(self::NOT_CNPJ, $cnpj);
        }
        return $isCnpjValid;
    }

    public function inserirMascara($cnpj)
    {
        $cnpj = $this->retirarMascara($cnpj);
        if (strlen($cnpj) === 14) {
            $cnpjFormatado = substr($cnpj, 0, 2) . '.';
            $cnpjFormatado .= substr($cnpj, 2, 3) . '.';
            $cnpjFormatado .= substr($cnpj, 5, 3) . '/';
            $cnpjFormatado .= substr($cnpj, 8, 4) . '-';
            $cnpjFormatado .= substr($cnpj, 12, 2);
            return $cnpjFormatado;
        } else {
            return '-';
        }
    }

    public function retirarMascara($cnpj)
    {
        $filter = new \Zend\I18n\Filter\Alnum();
        return $filter->filter($cnpj);
    }
}
