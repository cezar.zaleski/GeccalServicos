<?php

namespace Application\Utils;

use Zend\Validator\AbstractValidator;

class Cep extends AbstractValidator
{
    const CEP = 'Cep';

    protected $_messageTemplates = array(
        self::CEP => "'%value%' não é um CEP Válido"
    );

    public function isValid($value)
    {
        $value = (new \Zend\Filter\Digits())->filter($value);

        $this->setValue($value);

        if (!preg_match('/^[0-9]{5}[0-9]{3}$/', $value)) {
            $this->error(self::CEP);
            return false;
        }

        return true;
    }
}
