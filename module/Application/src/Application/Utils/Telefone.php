<?php

namespace Application\Utils;

use Zend\Validator\AbstractValidator;

class Telefone extends AbstractValidator
{

    const NOT_TEL = 'notTelefone';

    protected $_messageTemplates = array(
        self::NOT_TEL => "'%value%' Telefone invalido.",
    );

    protected $_numericOnly;

    /**
     * Se o CPF for apenas numerico 12345678909 então $numbersOnly é true
     * Se não, (99) 9999-9999
     * @param bool $numbersOnly
     */
    public function __construct($numbersOnly = false, $options = null)
    {
        parent::__construct($options);
        //$this->_setNumericOnly($numbersOnly);
    }

//     /**
//      * Sets $_numericOnly
//      * @param bool $bool
//      */
//     private function _setNumericOnly($bool = false)
//     {
//         $this->_numericOnly = $bool;
//         if ($this->_numericOnly === true) {
//             $this->_regexp = '/^(\d){11}$/';
//         } else {
//             $this->_regexp = '/^(\d){3}(\.\d{3}){2}-(\d){2}$/';
//         }
//         return $this;
//     }

    /**
     *
     * @param   mixed $telefone
     * @return  boolean
     * @throws  Zend_Valid_Exception If validation of $telefone is impossible
     * @see ValidatorInterface::isValid()
     */
    public function isValid($telefone)
    {
        $telefone = $this->inserirMascara($telefone);
        // checks regexp, first and second validation Digit
        if (preg_match($this->_regexp, $telefone) &&
            $this->_checkDigitOne($this->_removeNonDigits($telefone)) &&
            $this->_checkDigitTwo($this->_removeNonDigits($telefone))
        ) {
            return true;
        }
        $this->error(self::NOT_CPF, $telefone);
        return false;
    }

    /**
     *
     * @param string $cpf
     * @return bool
     */
    private function _checkDigitOne($telefone)
    {
        $multipliers = array(10, 9, 8, 7, 6, 5, 4, 3, 2);
        return $this->_getDigit($telefone, $multipliers) == $telefone{9};
    }

    /**
     *
     * @param string $telefone
     * @return bool
     */
    private function _checkDigitTwo($telefone)
    {
        $multipliers = array(11, 10, 9, 8, 7, 6, 5, 4, 3, 2);
        return $this->_getDigit($telefone, $multipliers) == $telefone{10};
    }

    /**
     *
     * @param string $telefone
     * @param array(int) $multipliers
     * @return int
     */
    private function _getDigit($telefone, $multipliers)
    {
        $sum = null;
        foreach ($multipliers as $key => $v) {
            $sum += $telefone{$key} * $v;
        }
        $digit = $sum % 11;
        if ($digit < 2) {
            $digit = 0;
        } else {
            $digit = 11 - $digit;
        }
        return $digit;
    }

    public function inserirMascara($telefone)
    {
        $telefone = $this->retirarMascara($telefone);
        if (strlen($telefone) > 9) {
            return $telefone;
        } else {
            return substr($telefone, 0, 4) . "-" . substr($telefone, 4);
        }
    }

    public function retirarMascara($telefone)
    {
        return preg_replace('/[^0-9]/', '', trim($telefone));
    }
}
