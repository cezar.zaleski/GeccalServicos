<?php

namespace Application\Mvc\Controller;

use Application\Service\AbstractService;
use Zend\Session\Container;

abstract class AbstractActionController extends \Zend\Mvc\Controller\AbstractActionController
{
    protected $translator;

    protected function translate($message)
    {
        if (!$this->translator) {
            $this->translator = $this->getServiceLocator()->get('translator');
        }
        return $this->translator->translate($message);
    }

    /**
     * Retorna  os dados (identificações) gravados em sessão do usuário logado
     *
     * @return object(stdClass) com dados do usuário
     */
    public function getIdentity()
    {
        $identity = AbstractService::getServiceManager()->get('AuthService');
        return $identity->getIdentity();
    }


    /**
     * @param $srvAlias
     * @return array|object
     */
    public function getSm($srvAlias)
    {
        return AbstractService::getServiceManager()->get($srvAlias);
    }
}
