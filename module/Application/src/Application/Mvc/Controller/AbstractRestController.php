<?php

namespace Application\Mvc\Controller;


use Application\Service\AbstractService;
use Application\Service\Exception\ServiceException;
use Application\Utils\RenderResult;
use Zend\EventManager\EventManagerInterface;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;


abstract class AbstractRestController extends AbstractRestfulController
{

    protected $translator;

    /** @var  RenderResult $srvRenderResult */
    protected $srvRenderResult;

    protected function translate($message, $params=null)
    {
        if (!$this->translator) {
            $this->translator = $this->getServiceLocator()->get('translator');
        }
        if (is_array($params)) {
            return vsprintf($this->translator->translate($message), $params);
        }
        return sprintf($this->translator->translate($message), $params);
    }

    /**
     * Retorna  os dados (identificações) gravados em sessão do usuário logado
     *
     * @return object(stdClass) com dados do usuário
     */
    public function getIdentity()
    {
        $identity = AbstractService::getServiceManager()->get('AuthService');
        return $identity->getIdentity();
    }

    /**
     * @param $srvAlias
     * @return array|object
     */
    public function getSm($srvAlias)
    {
        return AbstractService::getServiceManager()->get($srvAlias);
    }

    /**
     * @param ServiceException $e
     * @return Response
     */
    public function errorException(ServiceException $e)
    {
        $errorCode = $e->getCode()? : 405;
        $this->response->setStatusCode($errorCode);
        $errorMsg = json_decode($e->getMessage(), true);

        $errorMsg = $errorMsg? : array('mensagem' =>
            array(
                'coMensagem' => $e->getMessage(),
                'tpMensagem' => AbstractService::ERROR_MSG,
                'type' => 'danger',
                'msg' => $this->translate($e->getMessage(), $e->getExecptionParams()),
            ),
        );

        $result = $this->getSrvRenderResult()->renderResult(array('listaMensagem' => $errorMsg, 'stSucesso' => 0));
        $this->response->setContent($result);
        $headers = $this->response->getHeaders();
        $headers->addHeaderLine('Content-type', 'application/json');
        $headers->addHeaderLine('Access-Control-Allow-Origin', '*');
        $this->response->setHeaders($headers);
        return $this->response;
    }

    public function formErrorException($form)
    {
        $messages = $form->getMessages();
        $msgReplicadas = array();
        $listaMensagem = array();
        foreach ($messages as $element => $elementMessages) {
            foreach ($elementMessages as $msg) {
                // Recupera a label do form
                $nome = ($form->get($element)->getLabel()) ?
                    $form->get($element)->getLabel() :
                    $form->get($element)->getName();
                // Remove da label os :
                $nome = str_replace(":", "", $nome);
                if (!in_array($msg, $msgReplicadas)) {
                    $errorMsg =
                        array(
                            'type' => 'danger',
                            'coMensagem' => $msg,
                            'tpMensagem' => AbstractService::ERROR_MSG,
                            'msg' => $this->translate($msg, $nome),
                    );
                    array_push($listaMensagem, $errorMsg);
                }
                $msgReplicadas[] = $msg;
            }
        }
        $result = $this->getSrvRenderResult()->renderResult(array('listaMensagem' => $listaMensagem, 'stSucesso' => 0));
        $this->response->setContent($result);
        $this->response->setStatusCode(401);
        $headers = $this->response->getHeaders();
        $headers->addHeaderLine('Content-type', 'application/json');
        $headers->addHeaderLine('Access-Control-Allow-Origin', '*');
        $this->response->setHeaders($headers);
        return $this->response;
    }
    /**
     * @return RenderResult
     */
    public function getSrvRenderResult()
    {
        if (! $this->srvRenderResult) {
            return $this->getSm('RenderResult');
        }
        return $this->srvRenderResult;
    }

    /**
     * @param RenderResult $srvRenderResult
     */
    public function setSrvRenderResult($srvRenderResult)
    {
        $this->srvRenderResult = $srvRenderResult;
    }

    /**
    //     * @param array $arrData
    //     * @return mixed|null|JsonModel
    //     */
    protected function renderResult($arrData)
    {
        $result = $this->getSrvRenderResult()->renderResult($arrData);
        $this->response->setContent($result);
        $headers = $this->response->getHeaders();
        $headers->addHeaderLine('Content-type', 'application/json');
        $headers->addHeaderLine('Access-Control-Allow-Origin', '*');
        $this->response->setHeaders($headers);
        return $this->response;
    }

    protected $allowedCollectionMethods = array(
        'POST',
        'GET',
        'PUT',
        'DELETE'
    );

    public function setEventManager(EventManagerInterface $events) {
        parent::setEventManager($events);

        $events->attach('dispatch', function ($e) {
            $this->checkOptions($e);
        }, 10);
    }

    public function checkOptions($e) {
        $response = $this->getResponse();
        $request = $e->getRequest();
        $method = $request->getMethod();

        if (!in_array($method, $this->allowedCollectionMethods)) {
            $this->methodNotAllowed();
            return $response;
        }
    }

    public function methodNotAllowed() {
        $this->response->setStatusCode(
            \Zend\Http\PhpEnvironment\Response::STATUS_CODE_405
        );
        throw new Exception('Method Not Allowed');
    }

    /**
     * @return EntityManager
     */
    protected function getEm() {
        return $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
    }
}
