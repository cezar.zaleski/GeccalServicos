<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Conta
 *
 * @ORM\Table(name="tb_conta", indexes={@ORM\Index(name="fk_conta_tipo_conta1_idx", columns={"id_tipo_conta"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ContaRepository")
 */
class Conta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_conta", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idConta;

    /**
     * @var string
     *
     * @ORM\Column(name="no_conta", type="string", length=45, nullable=false)
     */
    private $noConta;

    /**
     * @var float
     *
     * @ORM\Column(name="nu_saldo_inicial", type="float", nullable=true)
     */
    private $nuSaldoInicial;

    /**
     * @var int
     *
     * @ORM\Column(name="nu_op", type="integer", nullable=true)
     */
    private $nuOp;

    /**
     * @var \Application\Entity\TipoConta
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\TipoConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_conta", referencedColumnName="id_tipo_conta")
     * })
     */
    private $tipoConta;



    /**
     * Get idConta
     *
     * @return integer 
     */
    public function getIdConta()
    {
        return $this->idConta;
    }

    /**
     * Set noConta
     *
     * @param string $noConta
     * @return Conta
     */
    public function setNoConta($noConta)
    {
        $this->noConta = $noConta;

        return $this;
    }

    /**
     * Get noConta
     *
     * @return string 
     */
    public function getNoConta()
    {
        return $this->noConta;
    }

    /**
     * Set idTipoConta
     *
     * @param \Application\Entity\TipoConta $tipoConta
     * @return Conta
     */
    public function setTipoConta(\Application\Entity\TipoConta $tipoConta = null)
    {
        $this->tipoConta = $tipoConta;

        return $this;
    }

    /**
     * Get idTipoConta
     *
     * @return \Application\Entity\TipoConta 
     */
    public function getTipoConta()
    {
        return $this->tipoConta;
    }

    /**
     * @return float
     */
    public function getNuSaldoInicial()
    {
        return $this->nuSaldoInicial;
    }

    /**
     * @param float $nuSaldoInicial
     * @return Conta
     */
    public function setNuSaldoInicial($nuSaldoInicial)
    {
        $this->nuSaldoInicial = $nuSaldoInicial;
        return $this;
    }



    /**
     * @return int
     */
    public function getNuOp()
    {
        return $this->nuOp;
    }

    /**
     * @param int $nuOp
     * @return Conta
     */
    public function setNuOp($nuOp)
    {
        $this->nuOp = $nuOp;
        return $this;
    }


}
