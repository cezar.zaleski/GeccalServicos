<?php

namespace Application\Entity;

use Zend\Stdlib\Hydrator;


/**
 * Class AbstractEntity
 * @package Application\Entity
 */
class AbstractEntity
{

    /**
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        $hydrator = new Hydrator\ClassMethods;
        $hydrator->hydrate($options, $this);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $hydrator = new Hydrator\ClassMethods(false);
        return $hydrator->extract($this);
    }
}
