<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MovimentacaoConta
 *
 * @ORM\Table(name="geccal.tb_movimentacao_conta", indexes={@ORM\Index(name="fk_movimentacao_conta_tipo1_idx", columns={"id_tipo"}), @ORM\Index(name="fk_movimentacao_conta_conta1_idx", columns={"id_conta"}), @ORM\Index(name="fk_movimentacao_conta_descricao_movimento_conta1_idx", columns={"id_desc_movimento_conta"})})
 * @ORM\Entity(repositoryClass="Application\Repository\MovimentacaoContaRepository")
 */
class MovimentacaoConta extends AbstractEntity
{
    /**
     * @var integer
     * @ORM\Column(name="id_movimentacao_conta", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idMovimentacaoConta;

    /**
     * @var integer
     * @ORM\Column(name="nu_ano_mes", type="integer", nullable=false)
     */
    private $nuAnoMes;

    /**
     * @var boolean
     *
     * @ORM\Column(name="st_ativo", type="boolean", nullable=false)
     */
    private $stAtivo;


    /**
     * @var \Application\Entity\Conta
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Conta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_conta", referencedColumnName="id_conta")
     * })
     */
    private $conta;

    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\MovimentacaoContaDescricao", mappedBy="movimentacaoConta", cascade={"all"})
     */
    private  $movimentacaoContaDescricao;


    /**
     * Set stAtivo
     *
     * @param boolean $stAtivo
     * @return MovimentacaoConta
     */
    public function setStAtivo($stAtivo)
    {
        $this->stAtivo = $stAtivo;

        return $this;
    }

    /**
     * Get stAtivo
     *
     * @return boolean 
     */
    public function getStAtivo()
    {
        return $this->stAtivo;
    }

        /**
     * Set conta
     *
     * @param \Application\Entity\Conta $conta
     * @return MovimentacaoConta
     */
    public function setConta(\Application\Entity\Conta $conta = null)
    {
        $this->conta = $conta;

        return $this;
    }

    /**
     * Get conta
     *
     * @return \Application\Entity\Conta 
     */
    public function getConta()
    {
        return $this->conta;
    }

    /**
     * @return int
     */
    public function getNuAnoMes()
    {
        return $this->nuAnoMes;
    }

    /**
     * @param int $nuAnoMes
     * @return MovimentacaoConta
     */
    public function setNuAnoMes($nuAnoMes)
    {
        $this->nuAnoMes = $nuAnoMes;
        return $this;
    }

    /**
     * Add \Application\Entity\MovimentacaoContaDescricao
     *
     * @param \Application\Entity\MovimentacaoContaDescricao $movimentacaoContaDescricao
     * @return MovimentacaoContaDescricao
     */
    public function addMovimentacaoContaDescricao(\Application\Entity\MovimentacaoContaDescricao $movimentacaoContaDescricao)
    {
        $this->movimentacaoContaDescricao[] = $movimentacaoContaDescricao;
        $movimentacaoContaDescricao->setMovimentacaoConta($this);
        return $this;
    }

    /**
     * @return int
     */
    public function getIdMovimentacaoConta()
    {
        return $this->idMovimentacaoConta;
    }

    /**
     * @param int $idMovimentacaoConta
     * @return MovimentacaoConta
     */
    public function setIdMovimentacaoConta($idMovimentacaoConta)
    {
        $this->idMovimentacaoConta = $idMovimentacaoConta;
        return $this;
    }





    /**
     * Remove movimentacaoContaDescricao
     *
     * @param \Application\Entity\MovimentacaoContaDescricao $movimentacaoContaDescricao
     */
    public function removeMovimentacaoContaDescricao(\Application\Entity\MovimentacaoContaDescricao $movimentacaoContaDescricao)
    {
        $this->movimentacaoContaDescricao->removeElement($movimentacaoContaDescricao);
    }

    /**
     * Get movimentacaoContaDescricao
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovimentacaoContaDescricao()
    {
        return $this->movimentacaoContaDescricao;
    }
}
