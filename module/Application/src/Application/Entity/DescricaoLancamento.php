<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DescricaoLancamento
 *
 * @ORM\Table(name="tb_descricao_lancamento", indexes={@ORM\Index(name="fk_descricao_lancamento_tipo_lancamento_idx", columns={"id_tipo_lancamento"})})
 * @ORM\Entity(repositoryClass="Application\Repository\DescricaoLancamentoRepository")
 */
class DescricaoLancamento
{

    const ID_DESC_CAIXA = 10;
    const ID_CAIXA_SALDO = 32;
    const ID_CONTA_CORRENTE_SALDO = 33;
    const ID_CONTA_POUPANCA_SALDO = 34;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_desc_lancamento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idDescLancamento;

    /**
     * @var string
     *
     * @ORM\Column(name="no_desc_lancamento", type="string", length=45, nullable=false)
     */
    private $noDescLancamento;

    /**
     * @var string
     *
     * @ORM\Column(name="co_desc_lancamento", type="string", length=45, nullable=false)
     */
    private $coDescLancamento;

    /**
     * @var \Application\Entity\TipoLancamento
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\TipoLancamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_lancamento", referencedColumnName="id_tipo_lancamento")
     * })
     */
    private $tipoLancamento;

    /**
     * @return int
     */
    public function getIdDescLancamento()
    {
        return $this->idDescLancamento;
    }

    /**
     * @param int $idDescLancamento
     */
    public function setIdDescLancamento($idDescLancamento)
    {
        $this->idDescLancamento = $idDescLancamento;
    }

    /**
     * @return string
     */
    public function getNoDescLancamento()
    {
        return $this->noDescLancamento;
    }

    /**
     * @param string $noDescLancamento
     */
    public function setNoDescLancamento($noDescLancamento)
    {
        $this->noDescLancamento = $noDescLancamento;
    }

    /**
     * @return string
     */
    public function getCoDescLancamento()
    {
        return $this->coDescLancamento;
    }

    /**
     * @param string $coDescLancamento
     */
    public function setCoDescLancamento($coDescLancamento)
    {
        $this->coDescLancamento = $coDescLancamento;
    }

    /**
     * @return TipoLancamento
     */
    public function getTipoLancamento()
    {
        return $this->tipoLancamento;
    }

    /**
     * @param TipoLancamento $tipoLancamento
     */
    public function setTipoLancamento($tipoLancamento)
    {
        $this->tipoLancamento = $tipoLancamento;
    }




}
