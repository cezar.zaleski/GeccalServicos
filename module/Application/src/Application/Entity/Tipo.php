<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipo
 *
 * @ORM\Table(name="tipo")
 * @ORM\Entity
 */
class Tipo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTipo;

    /**
     * @var string
     *
     * @ORM\Column(name="no_tipo", type="string", length=45, nullable=false)
     */
    private $noTipo;



    /**
     * Get idTipo
     *
     * @return integer 
     */
    public function getIdTipo()
    {
        return $this->idTipo;
    }

    /**
     * Set noTipo
     *
     * @param string $noTipo
     * @return Tipo
     */
    public function setNoTipo($noTipo)
    {
        $this->noTipo = $noTipo;

        return $this;
    }

    /**
     * Get noTipo
     *
     * @return string 
     */
    public function getNoTipo()
    {
        return $this->noTipo;
    }
}
