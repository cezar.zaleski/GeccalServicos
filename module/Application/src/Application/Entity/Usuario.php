<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuario
 *
 * @ORM\Table(name="tb_usuario", indexes={@ORM\Index(name="fk_usuario_pessoa1_idx", columns={"id_pessoa"}), @ORM\Index(name="fk_usuario_perfil1_idx", columns={"id_perfil"})})
 * @ORM\Entity(repositoryClass="Application\Repository\UsuarioRepository")
 */
class Usuario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_usuario", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idUsuario;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_cadastro", type="date", nullable=false)
     */
    private $dtCadastro;

    /**
     * @var boolean
     *
     * @ORM\Column(name="st_ativo", type="boolean", nullable=false)
     */
    private $stAtivo;

    /**
     * @var string
     *
     * @ORM\Column(name="no_usuario", type="string", length=100, nullable=false)
     */
    private $noUsuario;

    /**
     * @var string
     *
     * @ORM\Column(name="no_senha", type="text", nullable=false)
     */
    private $noSenha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_ult_visita", type="date", nullable=true)
     */
    private $dtUltVisita;

    /**
     * @var string
     *
     * @ORM\Column(name="st_cookie", type="text", nullable=true)
     */
    private $stCookie;

    /**
     * @var \Application\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pessoa", referencedColumnName="id_pessoa")
     * })
     */
    private $pessoa;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\Perfil", inversedBy="idUsuario")
     * @ORM\JoinTable(name="tb_usuario_perfil",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_perfil", referencedColumnName="id_perfil")
     *   }
     * )
     */
    private $perfil;

    function __construct()
    {
        parent::
        $this->perfil = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idUsuario
     *
     * @return integer 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Set dtCadastro
     *
     * @param \DateTime $dtCadastro
     * @return Usuario
     */
    public function setDtCadastro($dtCadastro)
    {
        $this->dtCadastro = $dtCadastro;

        return $this;
    }

    /**
     * Get dtCadastro
     *
     * @return \DateTime 
     */
    public function getDtCadastro()
    {
        return $this->dtCadastro;
    }

    /**
     * Set stAtivo
     *
     * @param boolean $stAtivo
     * @return Usuario
     */
    public function setStAtivo($stAtivo)
    {
        $this->stAtivo = $stAtivo;

        return $this;
    }

    /**
     * Get stAtivo
     *
     * @return boolean 
     */
    public function getStAtivo()
    {
        return $this->stAtivo;
    }

    /**
     * Set noUsuario
     *
     * @param string $noUsuario
     * @return Usuario
     */
    public function setNoUsuario($noUsuario)
    {
        $this->noUsuario = $noUsuario;

        return $this;
    }

    /**
     * Get noUsuario
     *
     * @return string 
     */
    public function getNoUsuario()
    {
        return $this->noUsuario;
    }

    /**
     * Set noSenha
     *
     * @param string $noSenha
     * @return Usuario
     */
    public function setNoSenha($noSenha)
    {
        $this->noSenha = $noSenha;

        return $this;
    }

    /**
     * Get noSenha
     *
     * @return string 
     */
    public function getNoSenha()
    {
        return $this->noSenha;
    }

    /**
     * Set dtUltVisita
     *
     * @param \DateTime $dtUltVisita
     * @return Usuario
     */
    public function setDtUltVisita($dtUltVisita)
    {
        $this->dtUltVisita = $dtUltVisita;

        return $this;
    }

    /**
     * Get dtUltVisita
     *
     * @return \DateTime 
     */
    public function getDtUltVisita()
    {
        return $this->dtUltVisita;
    }

    /**
     * Set stCookie
     *
     * @param string $stCookie
     * @return Usuario
     */
    public function setStCookie($stCookie)
    {
        $this->stCookie = $stCookie;

        return $this;
    }

    /**
     * Get stCookie
     *
     * @return string 
     */
    public function getStCookie()
    {
        return $this->stCookie;
    }

    /**
     * Set idPessoa
     *
     * @param \Application\Entity\Pessoa $pessoa
     * @return Usuario
     */
    public function setPessoa(\Application\Entity\Pessoa $pessoa = null)
    {
        $this->pessoa = $pessoa;

        return $this;
    }

    /**
     * Get idPessoa
     *
     * @return \Application\Entity\Pessoa 
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPerfil()
    {
        return $this->perfil;
    }



    /**
     * Add perfil
     *
     * @param \Application\Entity\Perfil $perfil
     *
     * @return Usuario
     */
    public function addPerfil(\Application\Entity\Perfil $perfil)
    {
        $this->perfil[] = $perfil;

        return $this;
    }

    /**
     * Remove perfil
     *
     * @param \Application\Entity\Perfil $perfil
     */
    public function removePerfil(\Application\Entity\Perfil $perfil)
    {
        $this->perfil->removeElement($perfil);
    }
}
