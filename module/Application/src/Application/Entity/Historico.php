<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historico
 *
 * @ORM\Table(name="tb_historico", indexes={@ORM\Index(name="fk_historico_usuario1_idx", columns={"id_usuario"})})
 * @ORM\Entity
 */
class Historico
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_historico", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idHistorico;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_historico", type="date", nullable=false)
     */
    private $dtHistorico;

    /**
     * @var string
     *
     * @ORM\Column(name="no_acao", type="string", length=45, nullable=false)
     */
    private $noAcao;

    /**
     * @var boolean
     *
     * @ORM\Column(name="st_ativo", type="boolean", nullable=false)
     */
    private $stAtivo;

    /**
     * @var \Application\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     * })
     */
    private $idUsuario;



    /**
     * Get idHistorico
     *
     * @return integer 
     */
    public function getIdHistorico()
    {
        return $this->idHistorico;
    }

    /**
     * Set dtHistorico
     *
     * @param \DateTime $dtHistorico
     * @return Historico
     */
    public function setDtHistorico($dtHistorico)
    {
        $this->dtHistorico = $dtHistorico;

        return $this;
    }

    /**
     * Get dtHistorico
     *
     * @return \DateTime 
     */
    public function getDtHistorico()
    {
        return $this->dtHistorico;
    }

    /**
     * Set noAcao
     *
     * @param string $noAcao
     * @return Historico
     */
    public function setNoAcao($noAcao)
    {
        $this->noAcao = $noAcao;

        return $this;
    }

    /**
     * Get noAcao
     *
     * @return string 
     */
    public function getNoAcao()
    {
        return $this->noAcao;
    }

    /**
     * Set stAtivo
     *
     * @param boolean $stAtivo
     * @return Historico
     */
    public function setStAtivo($stAtivo)
    {
        $this->stAtivo = $stAtivo;

        return $this;
    }

    /**
     * Get stAtivo
     *
     * @return boolean 
     */
    public function getStAtivo()
    {
        return $this->stAtivo;
    }

    /**
     * Set idUsuario
     *
     * @param \Application\Entity\Usuario $idUsuario
     * @return Historico
     */
    public function setIdUsuario(\Application\Entity\Usuario $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \Application\Entity\Usuario 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
