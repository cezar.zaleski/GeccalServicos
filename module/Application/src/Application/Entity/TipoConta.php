<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoConta
 *
 * @ORM\Table(name="tb_tipo_conta")
 * @ORM\Entity
 */
class TipoConta
{
    const CORRENTE = 1;
    const POUPANCA = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_conta", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTipoConta;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_conta", type="string", length=45, nullable=false)
     */
    private $tipoConta;



    /**
     * Get idTipoConta
     *
     * @return integer 
     */
    public function getIdTipoConta()
    {
        return $this->idTipoConta;
    }

    /**
     * Set tipoConta
     *
     * @param string $tipoConta
     * @return TipoConta
     */
    public function setTipoConta($tipoConta)
    {
        $this->tipoConta = $tipoConta;

        return $this;
    }

    /**
     * Get tipoConta
     *
     * @return string 
     */
    public function getTipoConta()
    {
        return $this->tipoConta;
    }
}
