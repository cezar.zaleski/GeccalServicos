<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoPagamento
 *
 * @ORM\Table(name="tb_tipo_pagamento")
 * @ORM\Entity(repositoryClass="Application\Repository\TipoPagamentoRepository")
 */
class TipoPagamento extends AbstractEntity
{
    const TP_PG_DINHEIO = 1;
    const TP_PG_CHEQUE = 2;
    const TP_PG_DEBITO = 3;
    const TP_PG_CREDITO = 4;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_pagamento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTipoPagamento;

    /**
     * @var string
     *
     * @ORM\Column(name="no_tipo_pagamento", type="string", length=45, nullable=false)
     */
    private $noTipoPagamento;



    /**
     * Get idTipoPagamento
     *
     * @return integer 
     */
    public function getIdTipoPagamento()
    {
        return $this->idTipoPagamento;
    }

    /**
     * Set noTipoPagamento
     *
     * @param string $noTipoPagamento
     * @return TipoPagamento
     */
    public function setNoTipoPagamento($noTipoPagamento)
    {
        $this->noTipoPagamento = $noTipoPagamento;

        return $this;
    }

    /**
     * Get noTipoPagamento
     *
     * @return string 
     */
    public function getNoTipoPagamento()
    {
        return $this->noTipoPagamento;
    }
}
