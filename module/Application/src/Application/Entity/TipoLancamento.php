<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoLancamento
 *
 * @ORM\Table(name="tb_tipo_lancamento", indexes={@ORM\Index(name="fk_tipo_lancamento_tipo_historico1_idx", columns={"id_tipo_historico"})})
 * @ORM\Entity
 */
class TipoLancamento
{

    const TP_RECEITA = 1;
    const TP_BANCOS = 2;
    const TP_DESPESA = 3;
    const TP_INVESTIMENTO = 4;
    const TP_SALDO_FINAL = 5;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_lancamento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTipoLancamento;

    /**
     * @var string
     *
     * @ORM\Column(name="no_tipo_lancamento", type="string", length=45, nullable=false)
     */
    private $noTipoLancamento;

    /**
     * @var \Application\Entity\TipoHistorico
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\TipoHistorico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_historico", referencedColumnName="id_tipo_historico")
     * })
     */
    private $idTipoHistorico;



    /**
     * Get idTipoLancamento
     *
     * @return integer 
     */
    public function getIdTipoLancamento()
    {
        return $this->idTipoLancamento;
    }

    /**
     * Set noTipoLancamento
     *
     * @param string $noTipoLancamento
     * @return TipoLancamento
     */
    public function setNoTipoLancamento($noTipoLancamento)
    {
        $this->noTipoLancamento = $noTipoLancamento;

        return $this;
    }

    /**
     * Get noTipoLancamento
     *
     * @return string 
     */
    public function getNoTipoLancamento()
    {
        return $this->noTipoLancamento;
    }

    /**
     * Set idTipoHistorico
     *
     * @param \Application\Entity\TipoHistorico $idTipoHistorico
     * @return TipoLancamento
     */
    public function setIdTipoHistorico(\Application\Entity\TipoHistorico $idTipoHistorico = null)
    {
        $this->idTipoHistorico = $idTipoHistorico;

        return $this;
    }

    /**
     * Get idTipoHistorico
     *
     * @return \Application\Entity\TipoHistorico 
     */
    public function getIdTipoHistorico()
    {
        return $this->idTipoHistorico;
    }
}
