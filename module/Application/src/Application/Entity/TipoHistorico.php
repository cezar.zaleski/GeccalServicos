<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoHistorico
 *
 * @ORM\Table(name="tb_tipo_historico")
 * @ORM\Entity
 */
class TipoHistorico
{
    const TP_ENTRADA = 1;
    const TP_SALDO = 2;
    const TP_SAIDAS = 3;
    const TP_SALDO_FINAL = 4;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_historico", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTipoHistorico;

    /**
     * @var string
     *
     * @ORM\Column(name="no_tipo_historico", type="string", length=45, nullable=false)
     */
    private $noTipoHistorico;

    /**
     * @var boolean
     *
     * @ORM\Column(name="st_ativo", type="boolean", nullable=false)
     */
    private $stAtivo;



    /**
     * Get idTipoHistorico
     *
     * @return integer 
     */
    public function getIdTipoHistorico()
    {
        return $this->idTipoHistorico;
    }

    /**
     * Set noTipoHistorico
     *
     * @param string $noTipoHistorico
     * @return TipoHistorico
     */
    public function setNoTipoHistorico($noTipoHistorico)
    {
        $this->noTipoHistorico = $noTipoHistorico;

        return $this;
    }

    /**
     * Get noTipoHistorico
     *
     * @return string 
     */
    public function getNoTipoHistorico()
    {
        return $this->noTipoHistorico;
    }

    /**
     * Set stAtivo
     *
     * @param boolean $stAtivo
     * @return TipoHistorico
     */
    public function setStAtivo($stAtivo)
    {
        $this->stAtivo = $stAtivo;

        return $this;
    }

    /**
     * Get stAtivo
     *
     * @return boolean 
     */
    public function getStAtivo()
    {
        return $this->stAtivo;
    }
}
