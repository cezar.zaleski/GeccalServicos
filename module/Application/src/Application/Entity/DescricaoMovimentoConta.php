<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DescricaoMovimentoConta
 *
 * @ORM\Table(name="tb_descricao_movimento_conta")
 * @ORM\Entity(repositoryClass="Application\Repository\DescricaoMovimentoContaRepository")
 */
class DescricaoMovimentoConta
{
    const CHEQUE = 4;
    const TRANSFERENCIA_ONLINE = 5;
    const DEBITO_AUTOMATICO = 6;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_desc_movimento_conta", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idDescMovimentoConta;

    /**
     * @var string
     *
     * @ORM\Column(name="no_desc_movimentacao_conta", type="string", length=45, nullable=false)
     */
    private $noDescMovimentacaoConta;

    /**
     * Get idDescMovimentoConta
     *
     * @return integer 
     */
    public function getIdDescMovimentoConta()
    {
        return $this->idDescMovimentoConta;
    }

    /**
     * Set noDescMovimentacaoConta
     *
     * @param string $noDescMovimentacaoConta
     * @return DescricaoMovimentoConta
     */
    public function setNoDescMovimentacaoConta($noDescMovimentacaoConta)
    {
        $this->noDescMovimentacaoConta = $noDescMovimentacaoConta;

        return $this;
    }

    /**
     * Get noDescMovimentacaoConta
     *
     * @return string 
     */
    public function getNoDescMovimentacaoConta()
    {
        return $this->noDescMovimentacaoConta;
    }
}
