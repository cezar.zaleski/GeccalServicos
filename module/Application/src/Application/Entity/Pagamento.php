<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pagamento
 *
 * @ORM\Table(name="tb_pagamento", indexes={@ORM\Index(name="fk_forma_pagamento_tipo_pagamento1_idx", columns={"id_tipo_pagamento"}), @ORM\Index(name="fk_forma_pagamento_registro_caixa1_idx", columns={"id_registro_caixa"})})
 * @ORM\Entity(repositoryClass="Application\Repository\PagamentoRepository")
 */
class Pagamento extends AbstractEntity
{


    /**
     * @var integer
     *
     * @ORM\Column(name="id_pagamento", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPagamento;

    /**
     * @var float
     *
     * @ORM\Column(name="nu_valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $nuValor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="st_ativo", type="boolean", nullable=false)
     */
    private $stAtivo;

    /**
     * @var \Application\Entity\TipoPagamento
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\TipoPagamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_pagamento", referencedColumnName="id_tipo_pagamento")
     * })
     */
    private $tipoPagamento;

    /**
     * @var \Application\Entity\RegistroCaixa
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\RegistroCaixa", inversedBy="idPagamentoRegistro")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_registro_caixa", referencedColumnName="id_registro_caixa")
     * })
     */
    private $registroCaixa;



    /**
     * Get idPagamento
     *
     * @return integer 
     */
    public function getIdPagamento()
    {
        return $this->idPagamento;
    }

    /**
     * Set nuValor
     *
     * @param float $nuValor
     * @return Pagamento
     */
    public function setNuValor($nuValor)
    {
        $this->nuValor = $nuValor;

        return $this;
    }

    /**
     * Get nuValor
     *
     * @return float 
     */
    public function getNuValor()
    {
        return $this->nuValor;
    }

    /**
     * Set stAtivo
     *
     * @param boolean $stAtivo
     * @return Pagamento
     */
    public function setStAtivo($stAtivo)
    {
        $this->stAtivo = $stAtivo;

        return $this;
    }

    /**
     * Get stAtivo
     *
     * @return boolean 
     */
    public function getStAtivo()
    {
        return $this->stAtivo;
    }

    /**
     * Set idTipoPagamento
     *
     * @param \Application\Entity\TipoPagamento $idTipoPagamento
     * @return Pagamento
     */
    public function setTipoPagamento(\Application\Entity\TipoPagamento $tipoPagamento = null)
    {
        $this->tipoPagamento = $tipoPagamento;

        return $this;
    }

    /**
     * Get idTipoPagamento
     *
     * @return \Application\Entity\TipoPagamento 
     */
    public function getTipoPagamento()
    {
        return $this->tipoPagamento;
    }

    /**
     * Set idRegistroCaixa
     *
     * @param \Application\Entity\RegistroCaixa $idRegistroCaixa
     * @return Pagamento
     */
    public function setRegistroCaixa(\Application\Entity\RegistroCaixa $registroCaixa = null)
    {
        $this->registroCaixa = $registroCaixa;

        return $this;
    }

    /**
     * Get idRegistroCaixa
     *
     * @return \Application\Entity\RegistroCaixa 
     */
    public function getRegistroCaixa()
    {
        return $this->registroCaixa;
    }
}
