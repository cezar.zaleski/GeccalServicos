<?php

namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Perfil
 *
 * @ORM\Table(name="tb_perfil")
 * @ORM\Entity(repositoryClass="Application\Repository\PerfilRepository")
 */
class Perfil extends AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id_perfil", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idPerfil;

    /**
     * @var string
     *
     * @ORM\Column(name="no_perfil", type="string", length=45, nullable=false)
     */
    private $noPerfil;

    /**
     * @var boolean
     *
     * @ORM\Column(name="st_ativo", type="boolean", nullable=true)
     */
    private $stAtivo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Application\Entity\Usuario", mappedBy="perfil")
     */
    private $idUsuario;

    /**
     * Constructor
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        parent::__construct($options);
        $this->idUsuario = new ArrayCollection();
    }


    /**
     * Get idPerfil
     *
     * @return integer 
     */
    public function getIdPerfil()
    {
        return $this->idPerfil;
    }

    /**
     * Set noPerfil
     *
     * @param string $noPerfil
     * @return Perfil
     */
    public function setNoPerfil($noPerfil)
    {
        $this->noPerfil = $noPerfil;

        return $this;
    }

    /**
     * Get noPerfil
     *
     * @return string 
     */
    public function getNoPerfil()
    {
        return $this->noPerfil;
    }

    /**
     * Set stAtivo
     *
     * @param boolean $stAtivo
     * @return Perfil
     */
    public function setStAtivo($stAtivo)
    {
        $this->stAtivo = $stAtivo;

        return $this;
    }

    /**
     * Get stAtivo
     *
     * @return boolean 
     */
    public function getStAtivo()
    {
        return $this->stAtivo;
    }

    /**
     * Add idAcao
     *
     * @param \Application\Entity\Acao $idAcao
     * @return Perfil
     */
    public function addIdAcao(\Application\Entity\Acao $idAcao)
    {
        $this->idAcao[] = $idAcao;

        return $this;
    }

    /**
     * Remove idAcao
     *
     * @param \Application\Entity\Acao $idAcao
     */
    public function removeIdAcao(\Application\Entity\Acao $idAcao)
    {
        $this->idAcao->removeElement($idAcao);
    }

    /**
     * Get idAcao
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdAcao()
    {
        return $this->idAcao;
    }
    
    public function toArray()
    {
        return array(
            'idPerfil' => $this->getIdPerfil(),
            'noPerfil' => $this->getNoPerfil(),
            'stAtivo'  => $this->getStAtivo(),
            'idAcao'   => $this->getIdAcao(),
        );
    }

}
