<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MovimentacaoConta
 *
 * @ORM\Table(name="tb_movimentacao_conta_descricao")
 * @ORM\Entity(repositoryClass="Application\Repository\MovimentacaoContaDescricaoRepository")
 */
class MovimentacaoContaDescricao extends AbstractEntity
{

    /**
     * @var integer
     * @ORM\Column(name="id_mov_conta_descricao", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idMovContaDescricao;

    /**
     * @var \Application\Entity\MovimentacaoConta
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\MovimentacaoConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_movimentacao_conta", referencedColumnName="id_movimentacao_conta")
     * })
     */
    private $movimentacaoConta;

    /**
     * @var \Application\Entity\DescricaoMovimentoConta
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\DescricaoMovimentoConta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_desc_movimento_conta", referencedColumnName="id_desc_movimento_conta")
     * })
     */
    private $descricaoMovimentoConta;

    /**
     * @var integer
     *
     * @ORM\Column(name="nu_valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $nuValor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="nu_cheque", type="integer", nullable=true)
     */
    private $nuCheque;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_transferencia", type="string", length=200, nullable=true)
     */
    private $dsTransferencia;

    /**
     * @var string
     *
     * @ORM\Column(name="ds_debito", type="string", length=200, nullable=true)
     */
    private $dsDebito;

    /**
     * @var \Application\Entity\TipoRegistroCaixa
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\TipoRegistroCaixa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_registro_caixa", referencedColumnName="id_tipo_registro_caixa")
     * })
     */
    private $tipoRegistroCaixa;

    /**
     * @return int
     */
    public function getMovimentacaoConta()
    {
        return $this->movimentacaoConta;
    }

    /**
     * @param int $movimentacaoConta
     * @return MovimentacaoContaDescricao
     */
    public function setMovimentacaoConta($movimentacaoConta)
    {
        $this->movimentacaoConta = $movimentacaoConta;
        return $this;
    }

    /**
     * @return DescricaoMovimentoConta
     */
    public function getDescricaoMovimentoConta()
    {
        return $this->descricaoMovimentoConta;
    }

    /**
     * @param DescricaoMovimentoConta $descricaoMovimentoConta
     * @return MovimentacaoContaDescricao
     */
    public function setDescricaoMovimentoConta($descricaoMovimentoConta)
    {
        $this->descricaoMovimentoConta = $descricaoMovimentoConta;
        return $this;
    }

    /**
     * @return int
     */
    public function getNuValor()
    {
        return $this->nuValor;
    }

    /**
     * @param int $nuValor
     * @return MovimentacaoContaDescricao
     */
    public function setNuValor($nuValor)
    {
        $this->nuValor = $nuValor;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isNuCheque()
    {
        return $this->nuCheque;
    }

    /**
     * @param boolean $nuCheque
     * @return MovimentacaoContaDescricao
     */
    public function setNuCheque($nuCheque)
    {
        $this->nuCheque = $nuCheque;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isDsTransferencia()
    {
        return $this->dsTransferencia;
    }

    /**
     * @param boolean $dsTransferencia
     * @return MovimentacaoContaDescricao
     */
    public function setDsTransferencia($dsTransferencia)
    {
        $this->dsTransferencia = $dsTransferencia;
        return $this;
    }

    /**
     * @return TipoRegistroCaixa
     */
    public function getTipoRegistroCaixa()
    {
        return $this->tipoRegistroCaixa;
    }

    /**
     * @param TipoRegistroCaixa $tipoRegistroCaixa
     * @return MovimentacaoContaDescricao
     */
    public function setTipoRegistroCaixa($tipoRegistroCaixa)
    {
        $this->tipoRegistroCaixa = $tipoRegistroCaixa;
        return $this;
    }

    /**
     * @return string
     */
    public function getDsDebito()
    {
        return $this->dsDebito;
    }

    /**
     * @param string $dsDebito
     * @return MovimentacaoContaDescricao
     */
    public function setDsDebito($dsDebito)
    {
        $this->dsDebito = $dsDebito;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdMovContaDescricao()
    {
        return $this->idMovContaDescricao;
    }

    /**
     * @param int $idMovContaDescricao
     * @return MovimentacaoContaDescricao
     */
    public function setIdMovContaDescricao($idMovContaDescricao)
    {
        $this->idMovContaDescricao = $idMovContaDescricao;
        return $this;
    }







    /**
     * Get nuCheque
     *
     * @return integer
     */
    public function getNuCheque()
    {
        return $this->nuCheque;
    }

    /**
     * Get dsTransferencia
     *
     * @return string
     */
    public function getDsTransferencia()
    {
        return $this->dsTransferencia;
    }
}
