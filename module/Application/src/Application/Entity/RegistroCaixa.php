<?php

namespace Application\Entity;

use Application\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM,
    Doctrine\Common\Collections\ArrayCollection;

/**
 * RegistroCaixa
 *
 * @ORM\Table(name="tb_registro_caixa", indexes={@ORM\Index(name="fk_registro_caixa_descricao_lancamento1_idx", columns={"id_desc_lancamento"}), @ORM\Index(name="fk_registro_caixa_usuario1_idx", columns={"id_usuario"}), @ORM\Index(name="fk_registro_caixa_tipo_registro_caixa1_idx", columns={"id_tipo_registro_caixa"})})
 * @ORM\Entity(repositoryClass="Application\Repository\RegistroCaixaRepository")
 */
class RegistroCaixa extends AbstractEntity
{

    const slInicialDinheiro = 3580.29;
    const slInicialCheque = 0;
    const slInicialCredito = 0;
    const slInicialDebito = 0;
    const slInicialDinheiroAno = 1789.64;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_registro_caixa", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRegistroCaixa;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_registro", type="date", nullable=true)
     */
    private $dtRegistro;

    /**
     * @var string
     *
     * @ORM\Column(name="no_descricao", type="text", length=65535, nullable=true)
     */
    private $noDescricao;

    /**
     * @var float
     *
     * @ORM\Column(name="nu_valor", type="float", precision=10, scale=0, nullable=true)
     */
    private $nuValor;

    /**
     * @var boolean
     *
     * @ORM\Column(name="st_ativo", type="boolean", nullable=false)
     */
    private $stAtivo;

    /**
     * @var \Application\Entity\DescricaoLancamento
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\DescricaoLancamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_desc_lancamento", referencedColumnName="id_desc_lancamento")
     * })
     */
    private $descLancamento;

    /**
     * @var \Application\Entity\TipoRegistroCaixa
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\TipoRegistroCaixa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_registro_caixa", referencedColumnName="id_tipo_registro_caixa")
     * })
     */
    private $tipoRegistroCaixa;

    /**
     * @var \Application\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     * })
     */
    private $usuario;
    
    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\Pagamento", mappedBy="registroCaixa", cascade={"persist", "remove"})
     */
    protected $pagamento;
    
    public function __construct($options = null)
    {
        parent::__construct($options);
        $this->pagamento = new ArrayCollection();
    }




    /**
     * Get idRegistroCaixa
     *
     * @return integer 
     */
    public function getIdRegistroCaixa()
    {
        return $this->idRegistroCaixa;
    }

    /**
     * Set dtRegistro
     *
     * @param \DateTime $dtRegistro
     * @return RegistroCaixa
     */
    public function setDtRegistro($dtRegistro)
    {
        $this->dtRegistro = $dtRegistro;

        return $this;
    }

    /**
     * Get dtRegistro
     *
     * @return \DateTime 
     */
    public function getDtRegistro()
    {
        return $this->dtRegistro;
    }

    /**
     * Set noDescricao
     *
     * @param string $noDescricao
     * @return RegistroCaixa
     */
    public function setNoDescricao($noDescricao)
    {
        $this->noDescricao = $noDescricao;

        return $this;
    }

    /**
     * Get noDescricao
     *
     * @return string 
     */
    public function getNoDescricao()
    {
        return $this->noDescricao;
    }

    /**
     * Set nuValor
     *
     * @param float $nuValor
     * @return RegistroCaixa
     */
    public function setNuValor($nuValor)
    {
        $this->nuValor = $nuValor;

        return $this;
    }

    /**
     * Get nuValor
     *
     * @return float 
     */
    public function getNuValor()
    {
        return $this->nuValor;
    }

    /**
     * Set stAtivo
     *
     * @param boolean $stAtivo
     * @return RegistroCaixa
     */
    public function setStAtivo($stAtivo)
    {
        $this->stAtivo = $stAtivo;

        return $this;
    }

    /**
     * Get stAtivo
     *
     * @return boolean 
     */
    public function getStAtivo()
    {
        return $this->stAtivo;
    }

    /**
     * Set idDescLancamento
     *
     * @param \Application\Entity\DescricaoLancamento $idDescLancamento
     * @return RegistroCaixa
     */
    public function setDescLancamento(\Application\Entity\DescricaoLancamento $descLancamento = null)
    {
        $this->descLancamento = $descLancamento;

        return $this;
    }

    /**
     * Get idDescLancamento
     *
     * @return \Application\Entity\DescricaoLancamento 
     */
    public function getDescLancamento()
    {
        return $this->descLancamento;
    }

    /**
     * Set idTipoRegistroCaixa
     *
     * @param \Application\Entity\TipoRegistroCaixa $idTipoRegistroCaixa
     * @return RegistroCaixa
     */
    public function setTipoRegistroCaixa(\Application\Entity\TipoRegistroCaixa $tipoRegistroCaixa = null)
    {
        $this->tipoRegistroCaixa = $tipoRegistroCaixa;

        return $this;
    }

    /**
     * Get idTipoRegistroCaixa
     *
     * @return \Application\Entity\TipoRegistroCaixa 
     */
    public function getTipoRegistroCaixa()
    {
        return $this->tipoRegistroCaixa;
    }

    /**
     * Set idUsuario
     *
     * @param \Application\Entity\Usuario $idUsuario
     * @return RegistroCaixa
     */
    public function setUsuario(\Application\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \Application\Entity\Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
    
    public function setPagamento(Pagamento $pagamento) {
        $pagamento->setRegistroCaixa($this);
        $this->pagamento = $pagamento;
    }
    
    public function getPagamento() {
        return $this->pagamento;
    }

    /**
     * Add pagamento
     *
     * @param \Application\Entity\Pagamento $pagamento
     *
     * @return RegistroCaixa
     */
    public function addPagamento(\Application\Entity\Pagamento $pagamento)
    {
        $this->pagamento[] = $pagamento;

        return $this;
    }

    /**
     * Remove pagamento
     *
     * @param \Application\Entity\Pagamento $pagamento
     */
    public function removePagamento(\Application\Entity\Pagamento $pagamento)
    {
        $this->pagamento->removeElement($pagamento);
    }
}
