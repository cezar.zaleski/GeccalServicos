<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoRegistroCaixa
 *
 * @ORM\Table(name="tb_tipo_registro_caixa")
 * @ORM\Entity(repositoryClass="Application\Repository\TipoRegistroCaixaRepository")
 */
class TipoRegistroCaixa
{
    const TP_REG_ENTRADA = 1;
    const TP_REG_SAIDA = 2;
    /**
     * @var integer
     *
     * @ORM\Column(name="id_tipo_registro_caixa", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTipoRegistroCaixa;

    /**
     * @var string
     *
     * @ORM\Column(name="no_tipo_registro_caixa", type="string", length=45, nullable=false)
     */
    private $noTipoRegistroCaixa;

    /**
     * @var string
     *
     * @ORM\Column(name="no_finalidade", type="string", length=45, nullable=false)
     */
    private $noFinalidade;



    /**
     * Get idTipoRegistroCaixa
     *
     * @return integer 
     */
    public function getIdTipoRegistroCaixa()
    {
        return $this->idTipoRegistroCaixa;
    }

    /**
     * Set noTipoRegistroCaixa
     *
     * @param string $noTipoRegistroCaixa
     * @return TipoRegistroCaixa
     */
    public function setNoTipoRegistroCaixa($noTipoRegistroCaixa)
    {
        $this->noTipoRegistroCaixa = $noTipoRegistroCaixa;

        return $this;
    }

    /**
     * Get noTipoRegistroCaixa
     *
     * @return string 
     */
    public function getNoTipoRegistroCaixa()
    {
        return $this->noTipoRegistroCaixa;
    }

    /**
     * Set noFinalidade
     *
     * @param string $noFinalidade
     * @return TipoRegistroCaixa
     */
    public function setNoFinalidade($noFinalidade)
    {
        $this->noFinalidade = $noFinalidade;

        return $this;
    }

    /**
     * Get noFinalidade
     *
     * @return string 
     */
    public function getNoFinalidade()
    {
        return $this->noFinalidade;
    }
    
    
    public function toArray()
    {
        return array(
            'idTipoRegistroCaixa' => $this->getIdTipoRegistroCaixa(),
            'noTipoRegistroCaixa' => $this->getNoTipoRegistroCaixa(),
            'noFinalidade' => $this->getNoFinalidade(),
        );
    }
}
