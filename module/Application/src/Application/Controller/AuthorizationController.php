<?php

namespace Application\Controller;

use Application\Mvc\Controller\AbstractRestController;
use Application\Service\Exception\ServiceException;

class AuthorizationController extends AbstractRestController {

    public function get($id)
    {
        try {
            /* @var $srvAcl \Application\Service\Acl */
            $srvAcl = $this->getServiceLocator()->get('AclService');
            $srvAcl->verificarPermissaoApp($id);
            return $this->renderResult(array('stSucesso' => true));
        } catch (ServiceException $e) {
            return $this->errorException($e);
        }
    }
}