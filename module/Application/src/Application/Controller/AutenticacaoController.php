<?php

namespace Application\Controller;

use Application\Mvc\Controller\AbstractRestController;
use Application\Form\LoginForm;
use Application\Service\Exception\ServiceException;

class AutenticacaoController extends AbstractRestController {

    public function create($data)
    {
        try{
            $form = new LoginForm();
            $form->setData($data);
            if($form->isValid()){
                /* @var $srvAdapter \Application\Auth\Adapter */
                $srvAdapter = $this->getServiceLocator()->get('Application\Auth\Adapter');
                $srvAdapter->setData($form->getData());
                $srvAdapter->ip = $this->getRequest()->getServer()->get('REMOTE_ADDR');
                $result = $srvAdapter->authenticate();
                return $this->renderResult(array(
                    'stSucesso' => true
                ));
            }else{
                return $this->formErrorException($form);
            }

        } catch (ServiceException $e) {
            return $this->errorException($e);
        }
    }
}