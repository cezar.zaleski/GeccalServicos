<?php


namespace Application\Form;

use Zend\InputFilter\InputFilter;

class LoginFilter extends InputFilter {

    function __construct() {

        $this->add(array(
            'name' => 'noLogin',
            'required' => TRUE,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'E002'
                        )
                    )
                ),

            )
        ));
        $this->add(array(
            'name' => 'noSenha',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'E002'
                        )
                    )
                ),
            )
        ));
    }
}
