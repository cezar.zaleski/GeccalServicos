<?php

namespace Application\Form;

use Zend\Form\Form;

class LoginForm extends Form{

    public function __construct() {
        parent::__construct('login');
        $this->setInputFilter(new LoginFilter());

        $this->add(array(
            'name' => 'noLogin',
            'options' => array(
                'label' => 'Usuário',
                'type' => 'text'
            ),
            'attributes' => array(
                'required' => 'required'
            )
        ));

        $this->add(array(
            'name' => 'noSenha',
            'options' => array(
                'label' => 'Senha',
                'type' => 'password'
            ),
            'attributes' => array(
                'required' => 'required'
            )
        ));
    }


}
