<?php
namespace Application\Form\Filter;

use Zend\Filter\AbstractFilter;

class RemoveMascara extends AbstractFilter
{
    /**
     * Defined by Zend\Filter\FilterInterface
     *
     * Retorna o valor eliminando todos os caracteres nao numericos, com exceção da virgula separadora de decimal
     *
     * @param  string $value
     * @return string|mixed
     */
    public function filter($valor)
    {
        if (null === $valor) {
            return null;
        }

        $valor = str_replace(array('.', ',', '/', '-', '(', ')'), '', $valor);
        $valor = preg_replace('/[^0-9.]+/', '', $valor);

        return $valor;
    }
}
