<?php

namespace Application\Service;

/*
 * Representa o estudante beneficiário do Programa de Crédito Educativo - PCE/CREDUC,
 *
 * @access public
 * @author arvieira@indracompany.com
 * @name   InadimplenteCreducService
 *
 */
class InadimplenteCreducService extends \Application\Service\AbstractService
{
    protected $entity = 'Application\Entity\FiesGlobal\InadimplenteCreducEntity';

    public function __construct()
    {
        parent::__construct();
    }

    public function validaCreduc($nuCpf)
    {
        //return false;
        $creduc = $this->findOneBy(array('nuCpf' => $nuCpf));
        return $creduc;
    }
}
