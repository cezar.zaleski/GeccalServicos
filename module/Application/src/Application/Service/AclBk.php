<?php
/**
 * Ministério da Educação do Brasil
 *
 * Este arquivo é parte integrante dos sistemas do Ministério da Educação do
 * Brasil. Antes da utilização do mesmo consulte a instituição.
 *
 * Este archivo de código fuente pertenece al Ministerio de Educación de Brasil.
 * Antes de usarlo, póngase en contacto con la institución.
 *
 * This source file belongs to Ministry of Education of Brazil. Before using it,
 * contact the institution.
 *
 * @author Paulo Borges <psdos@indracompany.com / contrictorhkss@gmail.com>
 *
 */
namespace Application\Service;

use Application\Service\Exception\ServiceException;
use Application\Utils\Cpf;
use Application\Utils\RenderResult;
use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Http\Response;

class AclBk extends AbstractService
{

    /** @var  array $arrRules */
    private $arrRules;
    private $permitido = 'allow';
    private $negado = 'deny';
    /** @var array $segurancaOptions */
    private $segurancaOptions;
    /** @var  RenderResult $srvRenderResult */
    private $srvRenderResult;

    public function verificaPermissaoAcesso($events)
    {
        try {
            $responseToken = $this->verificaToken();

            if (isset($responseToken->success) && $responseToken->success == 1) {
                $perfil = 'estudante'; /** @TODO  $responseToken->perfil; Deve ser implementado no "sitema seguranca" */
                $params = $events->getRouteMatch()->getParams();
                $method = $events->getRequest()->getMethod();
                $controller = $params['controller'];

                if (isset($this->arrRules[$controller][$this->permitido][$perfil])) {
                    $regras = $this->arrRules[$controller][$this->permitido][$perfil];
                    if (in_array($method, $regras)) {
                        return true;
                    }
                }
            }
            $this->retornaAcessoNaoAutorizado();
        } catch (ServiceException $e) {
            return $this->getSrvRenderResult()->errorExeception($e);
        }
    }

    private function retornaAcessoNaoAutorizado()
    {
        throw new ServiceException('E099', 401);
    }

    /**
     * @return mixed
     */
    private function verificaToken()
    {
        $request = new Request();
        $nuCpf = isset($_GET['nuCpf'])?$_GET['nuCpf']:substr($_SERVER['REQUEST_URI'], -11);
        $token = isset($_SERVER['HTTP_AUTHORIZATION'])?$_SERVER['HTTP_AUTHORIZATION']:null;
        $this->validaDadosEntrada($nuCpf, $token);

        $request->getHeaders()->addHeaders([
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept' => 'text/json',
            'Authorization' => 'Bearer ' . $token,
        ]);
        $request->setUri($this->segurancaOptions['rootUrl'] . $this->segurancaOptions['autenticacaoUrl']);
        $request->setMethod('POST');
        $request->getPost()->set('nu_identificador', $nuCpf);

        $client = new Client;
        $client->setAdapter("Zend\Http\Client\Adapter\Curl");
        $response = $client->dispatch($request);
        return json_decode($response->getBody());
    }

    /**
     * @param $nuCpf
     * @param $token
     */
    private function validaDadosEntrada($nuCpf, $token)
    {
        $cpfValidator = new Cpf();
        if (empty($nuCpf)){
            $this->addErrorMensages('E002', 'nuCpf');
        }
        if (!empty($nuCpf)) {
            if(!is_numeric($nuCpf) || !$cpfValidator->isValid($nuCpf)) {
                $this->addErrorMensages('E003', 'nuCpf');
            }
        }
        if (empty($token)) {
            $this->addErrorMensages('E002', 'Authorization');
        }
        $this->lancaListaDeErros();
    }

    /**
     * @param array $segurancaOptions
     */
    public function setSegurancaOptions($segurancaOptions)
    {
        $this->segurancaOptions = $segurancaOptions;
    }

    /**
     * @return array
     */
    public function getArrRules()
    {
        return $this->arrRules;
    }

    /**
     * @param array $arrRules
     */
    public function setArrRules($arrRules)
    {
        $this->arrRules = $arrRules;
    }

    /**
     * @param ServiceException $e
     * @return Response
     */
    public function errorExeception(ServiceException $e)
    {
        return $this->getSrvRenderResult()->errorExeception($e);
    }
    /**
     * @return RenderResult
     */
    public function getSrvRenderResult()
    {
        if (! $this->srvRenderResult) {
            return $this->getSm('RenderResult');
        }
        return $this->srvRenderResult;
    }
}
