<?php

namespace Application\Service;

use Application\Entity\FiesGlobal\EstadoCivilEntity;
use Application\Entity\FiesGlobal\EstadoCivilRepository;
use Doctrine\ORM\EntityManager;

class EstadoCivilService extends AbstractService
{
    /** @var  estadoCivilRepository */
    private $estadoCivilRepo;

    public function listaMunicipios()
    {
        return $this->getMunicipioRepo()->findAll();
    }

    /**
     * Retorna Array de EstadoCivil pronto para utilizar em campos select em Forms
     * @return mixed
     */
    public function listaEstadoCivilInputSelect()
    {
        $arrData[''] = '-- Selecione --';
        /** @var EstadoCivilEntity $row */
        foreach ($this->listaMunicipios() as $row) {
            $arrData[$row->getCoEstadoCivil()] = $row->getDsEstadoCivil();
        }
        return $arrData;
    }

    public function getMunicipioRepo()
    {
        if (!$this->estadoCivilRepo) {
            $this->estadoCivilRepo = $this->getEntityManager()->getRepository('Application\Entity\FiesGlobal\EstadoCivilEntity');
        }
        return $this->estadoCivilRepo;
    }
}
