<?php

namespace Application\Service;

use Application\Entity\FiesGlobal\UfEntity;
use Application\Entity\FiesGlobal\UfRepository;
use Doctrine\ORM\EntityManager;

class UfService extends AbstractService
{
    /** @var  UfRepository */
    private $ufRepo;

    public function listaUfs()
    {
        return $this->getMunicipioRepo()->findAll();
    }

    /**
     * Retorna Array de UF's pronto para utilizar em campos select em Forms
     * @return mixed
     */
    public function listaUfInputSelect()
    {
        $arrData[''] = '-- Selecione --';
        /** @var UfEntity $row */
        foreach ($this->listaUfs() as $row) {
            $arrData[$row->getCoUf()] = $row->getSgUf();
        }
        return $arrData;
    }

    /**
     * @return UfRepository
     */
    public function getMunicipioRepo()
    {
        if (!$this->ufRepo) {
            $this->ufRepo = $this->getEntityManager()->getRepository('Application\Entity\FiesGlobal\UfEntity');
        }
        return $this->ufRepo;
    }
}
