<?php

namespace Application\Service;

use Application\Service\Exception\ServiceException;
use Application\PgSqlDb\Entity\Inscricao\Aditamento as AditamentoEntity;
use Application\Utils\Cpf;

class FiadorService extends AbstractService
{
    const FIANCA_CONVENCIONAL = 1;

    /**
     *
     * @param array $aditamento
     * @param array $fiadores
     */
    public function validaRenda($aditamento, $fiadores, $liminar)
    {
        if (!count($fiadores)) {
            return false;
        }

        /* @var $srvInscricao \Inscricao\Service\InscricaoService */
        $srvInscricao = $this->getServiceManager()->get('InscricaoService');

        /* @var $srvAditamentoLiminar \Aditamento\Service\AditamentoLiminarService */
        $srvAditamentoLiminar = $this->getServiceManager()->get('AditamentoLiminarService');

        //Calcula a mensalidade.
        $vlMensalidade = $srvInscricao->calculaVlMensalidade(
            $aditamento['vlSemestreComDesconto'],
            $aditamento['nuPercentSolicitadoFinanc']
        );

        if (AditamentoEntity::NAO_BOLSISTA_PROUNI == $aditamento['stBolsistaProuni']) {
            $vlMensalidade = $vlMensalidade * 2;
        }
        $rendaDisponivel = $this->somaRendaDisponivel($fiadores, $aditamento);

        if ($vlMensalidade > (string)$rendaDisponivel) {
            if ($liminar->stLiberarRendaFiador) {
                //marca como utilizada
                $srvAditamentoLiminar->marcaLiminar(
                    $liminar->coLiminar['stLiberarRendaFiador'],
                    $aditamento['coAditamento'],
                    'S',
                    'stLiberarRendaFiador'
                );
                return false;
            }
            $string = empty($prouni)?'a duas vezes o':'ao';
            throw new ServiceException('MSG015', 401, array($string));
        } else {
            if ($liminar->stLiberarRendaFiador) {
                //marca como não utilizada
                //marca como utilizada
                $srvAditamentoLiminar->marcaLiminar(
                    $liminar->coLiminar['stLiberarRendaFiador'],
                    $aditamento['coAditamento'],
                    'N',
                    'stLiberarRendaFiador'
                );
                return false;
            }
        }
    }

    /**
     * Valida se estudante tem algum tipo de fianca
     * @param array $aditamento
     * @param array $fiadores
     */
    public function validaTpFianca($aditamento, $fiadores, $liminar)
    {
        /* @var $srvAditamentoLiminar \Aditamento\Service\AditamentoLiminarService */
        $srvAditamentoLiminar = $this->getServiceManager()->get('AditamentoLiminarService');

        if ((self::FIANCA_CONVENCIONAL == $aditamento['coTipoFianca']) && !count($fiadores)) {
            if ($liminar->stLiberarExigenciaFiador) {
                //marca como utilizada
                $srvAditamentoLiminar->marcaLiminar(
                    $liminar->coLiminar['stLiberarExigenciaFiador'],
                    $aditamento['coAditamento'],
                    'S',
                    'stLiberarExigenciaFiador'
                );
            }
            throw new ServiceException('MSG016', 401);
        }

        if ($liminar->stLiberarExigenciaFiador) {
            //marca como não utilizada
            $srvAditamentoLiminar->marcaLiminar(
                $liminar->coLiminar['stLiberarExigenciaFiador'],
                $aditamento['coAditamento'],
                'N',
                'stLiberarExigenciaFiador'
            );
        }
    }
    /**
     *
     * @param array $fiadores
     * @param array $aditamento
     */
    public function somaRendaDisponivel($fiadores, $aditamento)
    {
        $this->setEntity('Application\PgSqlDb\Entity\Inscricao\FiadorIndividual');
        $repo = $this->getPgSqlRepository();

        $rendaDisponivel    = null;
        $somaRendaDisponivel = 0;
        foreach ($fiadores as $key => $row) {
            $rendaDisponivel = $repo->rendaDisponivel(
                $row['nuCpf'],
                $row['vlRendaInformada'],
                $aditamento['coInscricao'],
                $aditamento['coAditamento']);

            $somaRendaDisponivel += $rendaDisponivel[0]['vl'];
        }

        return $somaRendaDisponivel;
    }

    /**
     * Recupera os fiadores de acordo com o aditamento informado.
     * @param int $coAditamento
     * @return int
     */
    public function recuperaFiadores($coAditamento)
    {
        $this->setEntity('\Application\PgSqlDb\Entity\Inscricao\FiadorAditamento');

        /* @var $repo \Application\PgSqlDb\Repository\Inscricao\FiadorAditamentoRepository */
        $repo = $this->getDefaultRepository();
        $fiadores = $repo->buscaPorCoAditamento($coAditamento);

        $fiadoresRetorno = array();
        $cpf = new Cpf();
        foreach ($fiadores as $fiador) {
            array_push($fiadoresRetorno,
                       array(
                          'noFiador' => $fiador['noFiador'],
                          'nuCpf' => $cpf->inserirMascara($fiador['nuCpf']),
                          'vlRenda' => $fiador['vlRendaInformada']
                       ));

        }

        return $fiadoresRetorno;
    }
}
