<?php

namespace Application\Service;

class TipoDeficienciaService extends AbstractService
{
    public function __construct()
    {
        parent::__construct();
        $this->setEntity('Application\Entity\FiesGlobal\TipoDeficienciaEntity');
    }

    public function comboDeficiencia()
    {
        $lista = $this->getRepository()->findAll();

        $combo = array();
        foreach ($lista as $key => $row) {
            $combo[$row->getCoDeficiencia()] = $row->getDsDeficiencia();
        }

        return $combo;
    }
}