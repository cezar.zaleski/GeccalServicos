<?php
/**
 * Ministério da Educação do Brasil
 *
 * Este arquivo é parte integrante dos sistemas do Ministério da Educação do
 * Brasil. Antes da utilização do mesmo consulte a instituição.
 *
 * Este archivo de código fuente pertenece al Ministerio de Educación de Brasil.
 * Antes de usarlo, póngase en contacto con la institución.
 *
 * This source file belongs to Ministry of Education of Brazil. Before using it,
 * contact the institution.
 *
 * @author Paulo Borges <psdos@indracompany.com / contrictorhkss@gmail.com>
 *
 */
namespace Application\Service;

use Application\Service\Exception\ServiceException;
use Application\Utils\Cpf;
use Application\Utils\RenderResult;
use Zend\Http\Client;
use Zend\Http\Request;
use Application\Auth\UsuarioAuth;
use Zend\Http\Response;

class Acl extends AbstractService
{

    /** @var  array $arrRules */
    private $arrRules;
    /** @var  array $arrApp */
    private $arrApp;
    private $permitido = 'allow';
    /** @var  string $publico */
    private $publico = 'PUBLICO';
    /** @var  RenderResult $srvRenderResult */
    private $srvRenderResult;

    const MINUTOS_SESSAO = 20;

    public function verificaPermissaoAcesso($events)
    {
        try {
            $params = $events->getRouteMatch()->getParams();
            $method = $events->getRequest()->getMethod();
            $controller = $params['controller'];
            if($this->verificaResourcePublico($controller, $method)){
                return true;
            };
            $identity = UsuarioAuth::getLoggedUser();

            if(is_null($identity)){
                $this->retornaAcessoNaoAutorizado();
            }
            $this->validarTempoSessao($identity);

            if (isset($this->arrRules[$controller][$this->permitido][$identity->sgPerfil])) {
                $regras = $this->arrRules[$controller][$this->permitido][$identity->sgPerfil];
                if (in_array($method, $regras)) {
                    return true;
                }
            }
            $this->retornaAcessoNaoAutorizado();
        } catch (ServiceException $e) {
            return $this->getSrvRenderResult()->errorException($e, 401);
        }
    }

    /**
     * Verifica se a rota na app esta autorizada para o perfil logado
     * @param $resource
     * @return bool
     */
    public function verificarPermissaoApp($resource)
    {
        $identity = UsuarioAuth::getLoggedUser();
        if($identity){
            $this->validarTempoSessao($identity);
            if (isset($this->arrApp[$resource][$this->permitido])) {
                $regras = $this->arrApp[$resource][$this->permitido];
                if (in_array($identity->sgPerfil, $regras['perfil'])) {
                    return true;
                }
            }
        }
        $this->retornaAcessoNaoAutorizado();
    }

    /**
     * Verifica se o recurso acesso é público
     * @param string $controller
     * @param string $method
     * @return bool
     */
    private function verificaResourcePublico($controller, $method)
    {
        $this->arrRules[$controller][$this->permitido];
        if(isset($this->arrRules[$controller][$this->permitido][$this->publico])){
            $regras = $this->arrRules[$controller][$this->permitido][$this->publico];
            if (in_array($method, $regras)) {
                return true;
            }
        }
        return false;


    }

    private function retornaAcessoNaoAutorizado()
    {
        throw new ServiceException('E099', 401);
    }

    /**
     * @return array
     */
    public function getArrRules()
    {
        return $this->arrRules;
    }

    /**
     * @param array $arrRules
     */
    public function setArrRules($arrRules)
    {
        $this->arrRules = $arrRules;
    }

    /**
     * @param ServiceException $e
     * @return Response
     */
    public function errorException(ServiceException $e)
    {
        return $this->getSrvRenderResult()->errorException($e);
    }
    /**
     * @return RenderResult
     */
    public function getSrvRenderResult()
    {
        if (! $this->srvRenderResult) {
            return $this->getSm('RenderResult');
        }
        return $this->srvRenderResult;
    }

    /**
     * @return array
     */
    public function getArrApp()
    {
        return $this->arrApp;
    }

    /**
     * @param array $arrApp
     */
    public function setArrApp($arrApp)
    {
        $this->arrApp = $arrApp;
    }

    private function validarTempoSessao($identity)
    {
        $dtAtual = new \DateTime;
        $interval = $dtAtual->diff($identity->inicioSessao);
        $tpSessao = (int)$interval->format('%i');
        if($tpSessao > self::MINUTOS_SESSAO){
            UsuarioAuth::logout();
            throw new ServiceException('E006', 401);
        }
        //atualiza o inicio da sessao
        $identity->inicioSessao = $dtAtual;
    }
}
