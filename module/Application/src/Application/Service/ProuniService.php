<?php
namespace Application\Service;

class ProuniService extends AbstractService
{

    /**
     * Calcula o valor e financiamento pelo ProUni
     * @param float $vlFinancProuni
     * @param float $nuPercentualProuni
     * @return float
     */
    public function calcularValorFinanciamentoProuni($vlFinancProuni, $nuPercentualProuni)
    {
        $valorFincProuni = $vlFinancProuni * $nuPercentualProuni / 100;
        return number_format($valorFincProuni, 2, '.','');
    }
}
