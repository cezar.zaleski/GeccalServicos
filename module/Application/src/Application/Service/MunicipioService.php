<?php
/**
 * Ministério da Educação do Brasil
 *
 * Este arquivo é parte integrante dos sistemas do Ministério da Educação do
 * Brasil. Antes da utilização do mesmo consulte a instituição.
 *
 * Este archivo de código fuente pertenece al Ministerio de Educación de Brasil.
 * Antes de usarlo, póngase en contacto con la institución.
 *
 * This source file belongs to Ministry of Education of Brazil. Before using it,
 * contact the institution.
 *
 * @namespace Application\Service
 * @use Application\Service\MunicipioService
 */
namespace Application\Service;

use Application\Entity\FiesGlobal\MunicipioEntity;
use Application\Entity\FiesGlobal\MunicipioRepository;
use Doctrine\ORM\EntityManager;

class MunicipioService extends AbstractService
{
    /** @var  MunicipioRepository */
    private $municipioRepo;

    public function __construct()
    {
        parent::__construct();
        $this->setEntity('Application\Entity\FiesGlobal\MunicipioEntity');
    }

    /**
     * @param $coUf
     * @return mixed
     */
    public function listaMunicipios($coUf)
    {
        $repoUf = $this->entityManager->getRepository('Application\Entity\FiesGlobal\UfEntity');
        $ufEntity = $repoUf->find($coUf);

        $result = $this->getRepository()->findBy(array('sgUf' => $ufEntity->getSgUf()));
        $arrMunicipio = array();
        foreach ($result as $row) {
            $arrMunicipio[] = array(
                'coMunicipio' => $row->getCoMunicipio(),
                'noMunicipio' => $row->getNoMunicipio()
            );
        }
        return $arrMunicipio;
    }

    public function localizacaoPorMunicipio($coMunicipio)
    {
        $municipioRepo = $this->getMunicipioRepo();
        $municipio = $municipioRepo->findOneBy(array('coMunicipio'=>$coMunicipio));
        /** @var MunicipioEntity $municipio */
        return array(
            'coCidade' => $municipio->getCoMunicipio(),
            'sgUf' => $municipio->getSgUf(),
            'noMunicipio' => $municipio->getNoMunicipio(),
        );
    }

    public function getMunicipioRepo()
    {
        if (!$this->municipioRepo) {
            $this->municipioRepo = $this->getEntityManager()
                ->getRepository('Application\Entity\FiesGlobal\MunicipioEntity');
        }
        return $this->municipioRepo;
    }
}
