<?php

namespace Application\Service;

use Application\Service\Exception\ServiceException;
use Application\PgSqlDb\Entity\PublicSchema\TrocaMantencaEfetivada as TrocaMantencaEfetivadaEntity;

class TrocaMantencaService extends AbstractService
{
    /**
     *
     * @param array $aditamento
     * @throws ServiceException
     * @return boolean
     */
    public function valida($aditamento)
    {
        $this->setEntity('Application\PgSqlDb\Entity\PublicSchema\VwTransferenciaMantenca');
        $repo = $this->getPgSqlRepository();

        $troca = $repo->buscarPorIes($aditamento['coIes'], $aditamento['nuSemestreReferencia']);

        //Não houve troca
        if (empty($troca)) {
            return false;
        }

        $this->setEntity('Application\PgSqlDb\Entity\PublicSchema\TrocaMantencaEfetivada');
        $repo = $this->getPgSqlRepository();

        $trocaEfetivada = $repo->buscar(
            $troca['coIes'],
            $troca['coMantenedoraCedente'],
            $troca['coMantenedoraAdquirente']
        );

        if ($trocaEfetivada) {
            //Se a ies nao foi efetivada, porque não tem termo assinado
            if (TrocaMantencaEfetivadaEntity::ST_EFETIVADO != $trocaEfetivada['stEfetivado']) {
                throw new ServiceException('MSG018', 401);
            }
        } else {
            throw new ServiceException('MSG018', 401);
        }

        /*
         * Se a mantenedora vinculada ao aditamento for diferente da mantenedora adquirente entao o sistema
         * irá atulizar o cod da mantenedora
         */
        if ($aditamento['coMantenedora'] == $troca['coMantenedoraAdquirente']) {
            return false;
        }

        /* @var $srvAditamento \Aditamento\Service\AditamentoService */
        $srvAditamento = $this->getServiceManager()->get('AditamentoService');

        //Atualiza aditamento
        $dados = array();
        $dados['coMantenedora'] = $troca['coMantenedoraAdquirente'];
        $dados['stTransferenciaMantenca'] = 'S';
        $srvAditamento->atualizaDados($dados, $aditamento['coAditamento']);

        /* @var $srvCursoAditamento \Aditamento\Service\CursoAssociadoAditamentoService */
        $srvCursoAditamento = $this->getServiceManager()->get('CursoAssociadoAditamentoService');

        //Atualiza curso_associado_aditamento
        $srvCursoAditamento->atualizaMantenedora(
            $troca['coMantenedoraAdquirente'],
            $aditamento['coCursoAssociadoAditamento']
        );
    }
}
