<?php

namespace Application\Service;

use Application\Service\Exception\ServiceException;
use Application\PgSqlDb\Entity\Inscricao\Aditamento as AditamentoEntity;

class LiminarService extends AbstractService
{

    /**
     *
     * @param array $aditamento
     * @param array $fiadores
     */
    public function buscar($aditamento)
    {
        $this->setEntity('Application\PgSqlDb\Entity\Inscricao\Liminar');
        $repo = $this->getPgSqlRepository();

        $return = new \stdClass();
        $return->stLiberarRendaFiador = false;
        $return->stLiberarExigenciaFiador = false;
        $return->stLiberarIdoneidadeAluno = false;
        $return->stLiberarIdoneidadeFiador = false;

        $liminares = $repo->buscar($aditamento);
        foreach ($liminares as $liminar) {
            if ('S' == $liminar['stLiberarIdoneidadeAluno']) {
                $return->stLiberarIdoneidadeAluno = true;
                $return->coLiminar['stLiberarIdoneidadeAluno'] = $liminar['coLiminar'];
            }

            if ('S' == $liminar['stLiberarIdoneidadeFiador']) {
                $return->stLiberarIdoneidadeFiador = true;
                $return->coLiminar['stLiberarIdoneidadeFiador'] = $liminar['coLiminar'];
            }

            if ('S' == $liminar['stLiberarRendaFiador']) {
                $return->stLiberarRendaFiador = true;
                $return->coLiminar['stLiberarRendaFiador'] = $liminar['coLiminar'];
            }

            if ('S' == $liminar['stLiberarExigenciaFiador']) {
                $return->stLiberarExigenciaFiador = true;
                $return->coLiminar['stLiberarExigenciaFiador'] = $liminar['coLiminar'];
            }
        }

        return $return;
    }
}
