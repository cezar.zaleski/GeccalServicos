<?php

namespace Application\Service;

use Application\Service\Exception\ServiceException;

class AgenteFinanceiroService extends AbstractService
{
    /**
     * Codigo para Banco do Brasil
     * @var string
     */
    const BB  = '001';

    /**
     * Codigo para Caixa Economica Federal
     * @var string
     */
    const CEF = '104';

    /**
     * Flag que estudante está adimplementa com o AF
     * @var string
     */
    const TP_ADIMPLENTE = 'S';

    /**
     * Flag que estudante não está adimplementa com o AF
     * @var string
     */
    const TP_NAO_ADIMPLENTE = 'N';


    /**
     *
     * @param string $nuCpf
     * @param string $coBanco
     * @param integer $coAgencia
     */
    public function validaAdimplencia($nuCpf, $coBanco, $coAgencia)
    {
        try {
            switch ($coBanco) {
                case self::BB:
                    $this->validaAdimplenciaBb($nuCpf, $coAgencia);
                    break;
                case self::CEF:
                    $this->validaAdimplenciaCx($nuCpf);
                    break;
                default: break;
            }
        } catch (ServiceException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new ServiceException('MSG019', 401);
        }
    }

    /**
     *
     * @param string $nuCpf
     * @throws ServiceException
     */
    public function validaAdimplenciaCx($nuCpf)
    {
        /* @var $wsCef \Ws\Service\Client\Cef */
        $wsCef = $this->getServiceManager()->get('Cef');
        $verificaIdoneidade = $wsCef->verificarIdoneidade($nuCpf, $nuCpf);

        if (!empty($verificaIdoneidade->MENSAGEM)) {
            throw new ServiceException('MSG003', 401);
        }

        if (self::TP_NAO_ADIMPLENTE == $verificaIdoneidade->ADIMPLENTE){
            throw new ServiceException('MSG003', 401);
        }
    }


    /**
     *
     * @param string $nuCpf
     * @param integer $coAgencia
     * @throws ServiceException
     */
    public function validaAdimplenciaBb($nuCpf, $coAgencia)
    {
        /* @var $wsBb \Ws\Service\Client\Bb */
        $wsBb = $this->getServiceManager()->get('Bb');
        $consulta  = array(array('cpf' => $nuCpf, 'situacao' => false));
        $verificaIdoneidade = $wsBb->verificarIdoneidade($consulta, $coAgencia);

        if ($verificaIdoneidade->situacao == false) {
            throw new ServiceException('MSG003', 401);
        }
    }


    /**
     *
     * @param array $aditamento
     * @param array $fiadores
     * @param \stdClass $liminar
     * @throws ServiceException
     */
    public function validaIdoneidade($aditamento, $fiadores, $liminar)
    {
        $estudante  = array(array('cpf' => $aditamento['nuCpf'], 'estudante' => true));

        $fiadoresConsulta = array();
        foreach ($fiadores as $key => $fiador) {
            $fiadoresConsulta[$key]['cpf'] = $fiador['nuCpf'];
            $fiadoresConsulta[$key]['estudante'] = false;
        }

        $consultas = array_merge($estudante, $fiadoresConsulta);
        try {
            switch ($aditamento['coBanco']) {
                case self::BB:
                    $this->validaIdoneidadeBb($consultas, $liminar, $aditamento);
                    break;
                case self::CEF:
                    $this->validaIdoneidadeCx($consultas, $liminar, $aditamento['coAditamento']);
                    break;
                default: break;
            }
        } catch (ServiceException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new ServiceException('MSG019', 401);
        }
    }

    /**
     *
     * @param array $consultas
     * @param \stdClass $liminar
     * @param integer $coAditamento
     * @throws ServiceException
     */
    public function validaIdoneidadeCx($consultas, $liminar, $coAditamento)
    {
        /* @var $wsCef \Ws\Service\Client\Cef */
        $wsCef = $this->getServiceManager()->get('Cef');

        /* @var $srvAditamentoLiminar \Aditamento\Service\AditamentoLiminarService */
        $srvAditamentoLiminar = $this->getServiceManager()->get('AditamentoLiminarService');

        $restricao = new \stdClass();
        $restricao->aluno = false;
        $restricao->fiador = false;

        foreach ($consultas as $key => $consulta) {
            $verificaIdoneidade = $wsCef->verificarIdoneidadeCompleto($consulta['cpf'], $consultas[0]['cpf']);

            //Se o retorno da CEF, emitir mensagem.
            if (!empty($verificaIdoneidade->MENSAGEM)) {
                throw new ServiceException('MSG019', 401);
            }

            //Verifica restrição
            if ($verificaIdoneidade->consultas->consulta->restricao) {
                if ($consulta['estudante'] && !$liminar->stLiberarIdoneidadeAluno) {
                    throw new ServiceException('MSG020', 401, array('Caixa Econômica Federal'));
                }

                if (empty($consulta['estudante']) && !$liminar->stLiberarIdoneidadeFiador) {
                    throw new ServiceException('MSG020', 401, array('Caixa Econômica Federal'));
                }

                //marca Liminar em que estudante utilizou
                if ($consulta['estudante']) {
                    $srvAditamentoLiminar->marcaLiminar(
                        $liminar->coLiminar['stLiberarIdoneidadeAluno'],
                        $coAditamento,
                        'S',
                        'stLiberarIdoneidadeAluno'
                    );
                    $restricao->aluno = true;
                } else {
                    //marca Liminar em que fiador utilizou
                    $srvAditamentoLiminar->marcaLiminar(
                        $liminar->coLiminar['stLiberarIdoneidadeFiador'],
                        $coAditamento,
                        'S',
                        'stLiberarIdoneidadeFiador'
                    );
                    $restricao->fiador = true;
                }
            }
        }

        //marca Liminar em que estudante não utilizou
        if (!$restricao->aluno && $liminar->stLiberarIdoneidadeAluno) {
            $srvAditamentoLiminar->marcaLiminar(
                $liminar->coLiminar['stLiberarIdoneidadeAluno'],
                $coAditamento,
                'N',
                'stLiberarIdoneidadeAluno'
            );
        }

        //marca Liminar em que fiador não utilizou
        if (!$restricao->fiador && $liminar->stLiberarIdoneidadeFiador) {
            $srvAditamentoLiminar->marcaLiminar(
                $liminar->coLiminar['stLiberarIdoneidadeFiador'],
                $coAditamento,
                'N',
                'stLiberarIdoneidadeFiador'
            );
        }
    }

    /**
     *
     * @param array $consultas
     * @param \stdClass $liminar
     * @param array $aditamento
     * @throws ServiceException
     */
    public function validaIdoneidadeBb($consultas, $liminar, $aditamento)
    {
        /* @var $wsBb \Ws\Service\Client\Bb */
        $wsBb = $this->getServiceManager()->get('Bb');

        /* @var $srvAditamentoLiminar \Aditamento\Service\AditamentoLiminarService */
        $srvAditamentoLiminar = $this->getServiceManager()->get('AditamentoLiminarService');

        $restricao = new \stdClass();
        $restricao->aluno = false;
        $restricao->fiador = false;

        $verificaIdoneidade = $wsBb->verificarIdoneidade($consultas, $aditamento['coAgencia']);

        if (!is_array($verificaIdoneidade)) {
            $verificaIdoneidade = array($verificaIdoneidade);
        }

        foreach ($verificaIdoneidade as $key => $consulta) {
            //Recupera cpfs que estão inidoneos.
            if (!$consulta->situacao) {
                //Se estudante não tem liminar lanca erro
                if (($consulta->cpf == $aditamento['nuCpf']) && (!$liminar->stLiberarIdoneidadeAluno)) {
                    throw new ServiceException('MSG020', 401, array('Banco do Brasil'));
                }

                if (($consulta->cpf != $aditamento['nuCpf']) && (!$liminar->stLiberarIdoneidadeFiador)) {
                    throw new ServiceException('MSG020', 401, array('Banco do Brasil'));
                }

                //marca Liminar em que estudante utilizou
                if ($consulta->cpf == $aditamento['nuCpf']) {
                    $srvAditamentoLiminar->marcaLiminar(
                        $liminar->coLiminar['stLiberarIdoneidadeAluno'],
                        $aditamento['coAditamento'],
                        'S',
                        'stLiberarIdoneidadeAluno'
                    );
                    $restricao->aluno = true;
                } else {
                    //marca Liminar em que fiador utilizou
                    $srvAditamentoLiminar->marcaLiminar(
                        $liminar->coLiminar['stLiberarIdoneidadeFiador'],
                        $aditamento['coAditamento'],
                        'S',
                        'stLiberarIdoneidadeFiador'
                    );
                    $restricao->fiador = true;
                }
            }
        }

        //marca Liminar em que estudante não utilizou
        if (!$restricao->aluno && $liminar->stLiberarIdoneidadeAluno) {
            $srvAditamentoLiminar->marcaLiminar(
                $liminar->coLiminar['stLiberarIdoneidadeAluno'],
                $aditamento['coAditamento'],
                'N',
                'stLiberarIdoneidadeAluno'
            );
        }

        //marca Liminar em que fiador não utilizou
        if (!$restricao->fiador && $liminar->stLiberarIdoneidadeFiador) {
            $srvAditamentoLiminar->marcaLiminar(
                $liminar->coLiminar['stLiberarIdoneidadeFiador'],
                $aditamento['coAditamento'],
                'N',
                'stLiberarIdoneidadeFiador'
            );
        }
    }
}
