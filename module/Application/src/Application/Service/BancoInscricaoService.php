<?php

namespace Application\Service;

use Application\Service\Exception\ServiceException;
use Application\PgSqlDb\Entity\Inscricao\Aditamento as AditamentoEntity;
use Application\PgSqlDb\Entity\Inscricao\BancoInscricao;

class BancoInscricaoService extends AbstractService
{
    /* @var $repoPgSqlBancoInscricao \Application\PgSqlDb\Repository\Inscricao\BancoInscricaoRepository */
    private $repoPgSqlBancoInscricao;

    public function setRepoPgSqlBancoInscricao($repoPgSqlBancoInscricao)
    {
        $this->repoPgSqlBancoInscricao = $repoPgSqlBancoInscricao;
    }

    public function retornaAgencia($coAgencia, $coBanco)
    {
        switch ($coBanco):
            case BancoInscricao::BB:
                $agencia = $this->repoPgSqlBancoInscricao->retornaAgenciaBB($coAgencia);
                break;
            case BancoInscricao::CEF:
                $agencia = $this->repoPgSqlBancoInscricao->retornaAgenciaCaixa($coAgencia, $coBanco);
                break;
        endswitch;

        $arrAgencia = explode('-', $agencia[0]['agencia']);
        $arrAgencia[0] = str_pad($arrAgencia[0], 4, '0', STR_PAD_LEFT);
        return implode($arrAgencia, '-');
    }
}
