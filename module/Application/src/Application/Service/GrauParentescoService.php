<?php

namespace Application\Service;

use Application\Entity\FiesGlobal\GrauParentescoEntity;
use Application\Entity\FiesGlobal\GrauParentescoRepository;
use Doctrine\ORM\EntityManager;

class GrauParentescoService extends AbstractService
{
    /** @var  UfRepository */
    private $ufRepo;

    public function listaGraus()
    {
        return $this->getParentescoRepo()->findAll();
    }

    /**
     * Retorna Array de UF's pronto para utilizar em campos select em Forms
     * @return mixed
     */
    public function listaGrauParentescoInputSelect()
    {
        $arrData[''] = '-- Selecione --';
        /** @var UfEntity $row */
        foreach ($this->listaGraus() as $row) {
            $arrData[$row->getCoGrauParentesco()] = $row->getDsGrauParentesco();
        }
        return $arrData;
    }

    public function getParentescoRepo()
    {
        if (!$this->ufRepo) {
            $this->ufRepo = $this->getEntityManager()->getRepository('Application\Entity\FiesGlobal\GrauparentescoEntity');
        }
        return $this->ufRepo;
    }
}
