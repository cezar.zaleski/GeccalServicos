<?php

namespace Application\Service;

class RacaCorService extends AbstractService
{
    public function __construct()
    {
        parent::__construct();
        $this->setEntity('Application\Entity\FiesGlobal\RacaCorEntity');
    }

    public function comboRacaCor()
    {
        $lista = $this->getRepository()->findAll();
        $combo = array();
        foreach ($lista as $key => $row) {
            $combo[$row->getCoRacaCor()] = $row->getDsRacaCor();
        }
        return $combo;
    }
}