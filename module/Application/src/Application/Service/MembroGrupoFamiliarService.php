<?php

namespace Application\Service;

use Application\Service\AbstractService;

class MembroGrupoFamiliarService extends AbstractService
{
    /* @var $repoPgSqlMembroGrupoFamiliar \Application\PgSqlDb\Repository\Inscricao\MembroGrupoFamiliarRepository */
    private $repoPgSqlMembroGrupoFamiliar;

    /* @var $repoPgSqlInscricao \Application\PgSqlDb\Repository\Inscricao\InscricaoRepository */
    private $repoPgSqlInscricao;

    public function setRepoPgSqlInscricao($repoPgSqlInscricao)
    {
        $this->repoPgSqlInscricao = $repoPgSqlInscricao;
    }

    public function setRepoPgSqlMembroGrupoFamiliar($repoPgSqlMembroGrupoFamiliar)
    {
        $this->repoPgSqlMembroGrupoFamiliar = $repoPgSqlMembroGrupoFamiliar;
    }

    /**
     * Retorna a quantidades de membros do grupo familiar
     * @param type $coInscricao
     */
    public function retornaQuantidadeMembroGrupoFamiliar($coInscricao)
    {
        $qtdMembros = $this->repoPgSqlMembroGrupoFamiliar->retornaQtMembros($coInscricao);
        return $qtdMembros[0]['qtdMembros'] + 1;
    }

    public function retornaRendaFamiliarMensalBrutaPerCapita($coInscricao)
    {
        $qtdMembros = $this->retornaQuantidadeMembroGrupoFamiliar($coInscricao);
        $vlRendaFamiliarMensalBruta = $this->repoPgSqlInscricao
                    ->retornaVlRendaFamiliarBrutaMensal($coInscricao);
        return ($vlRendaFamiliarMensalBruta[0]['vlRendaFamiliarBrutaMensal'] / $qtdMembros);
    }
}
