<?php
/**
 * Ministério da Educação do Brasil
 *
 * Este arquivo é parte integrante dos sistemas do Ministério da Educação do
 * Brasil. Antes da utilização do mesmo consulte a instituição.
 *
 * Este archivo de código fuente pertenece al Ministerio de Educación de Brasil.
 * Antes de usarlo, póngase en contacto con la institución.
 *
 * This source file belongs to Ministry of Education of Brazil. Before using it,
 * contact the institution.
 *
 * @category   Fies Pré-inscricao
 * @package    Application
 * @version    1.0.0
 *
 */
namespace Application\View\Helper;

use \Zend\View\Helper\AbstractHelper;

class AssetsPath extends AbstractHelper
{

    /** @var  string $path */
    private $assetsPath;

    /** @var string $applicationUrl */
    private $applicationUrl;

    /**
     * @return string
     */
    public function getAssetsPath()
    {
        return $this->assetsPath;
    }

    /**
     * @param string $assetsPath
     */
    public function setAssetsPath($assetsPath)
    {
        $this->assetsPath = $assetsPath;
    }

    /**
     * @return string
     */
    public function getApplicationUrl()
    {
        return $this->applicationUrl;
    }

    /**
     * @param string $applicationUrl
     */
    public function setApplicationUrl($applicationUrl)
    {
        $this->applicationUrl = $applicationUrl;
    }

}