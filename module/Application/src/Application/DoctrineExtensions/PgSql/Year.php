<?php

namespace Application\DoctrineExtensions\PgSql;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * ListaggFunction ::=
 *     "TO_CHAR" "(" StringPrimary ", '" String Identifier "')"
 *
 * to_char(date_colum, 'dd/mm/yyyy')
 */
class Year extends FunctionNode
{
    public $firstExpression = null;
    public $thirdExpression = null;

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER); // TO_CHAR
        $parser->match(Lexer::T_OPEN_PARENTHESIS); // (
        $this->firstExpression = $parser->StringExpression(); // column
        $parser->match(Lexer::T_CLOSE_PARENTHESIS); // )
    }

    public function getSql(SqlWalker $sqlWalker)
    {
        return "YEAR(" .
        $this->firstExpression->dispatch($sqlWalker).
        ")"
            ;
    }
}