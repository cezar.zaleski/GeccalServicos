<?php
namespace Application\Utility;

/**
 *
 * @author Reginaldo Azevedo Junior <reginaldoazevedojr@gmail.com>
 * @author Cezar Gabriel Heinen Zaleski <cezar.zaleski@gmail.com>
 */
abstract class AbstractUtilitySetGet
{

    /**
     *
     * @var mixed
     */
    protected $result;

    /**
     * inicializar variaveis uteis
     * @var array $data
     */
    abstract function __construct(array $data);

    /**
     * Passa para a utilidade os dados a serem tratados
     */
    public static function create($data){
        if (is_array($data)) {
            if (count($data) == 0) {
                throw new Exception("Array esta vazia!");
            }
        }else{
            if (strlen($data) == 0) {
                throw new Exception("Variavel esta vazia!");
            }
        }

        $instance = new static($data);

        return $instance;
    }

    /**
     * Retorna a utilidade da classe
     */
    public function get()
    {
        return $this->result;
    }
}