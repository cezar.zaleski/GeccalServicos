<?php
namespace Tesouraria;

use Tesouraria\Service\ContaService;
use Tesouraria\Service\DescricaoMovimentoContaService;
use Tesouraria\Service\MovimentacaoContaDescricaoService;
use Tesouraria\Service\MovimentacaoContaService;
use Tesouraria\Service\RegistroCaixaService;
use Tesouraria\Service\ValorRegistroCaixaService;
use Tesouraria\Service\RelatorioService;
use Tesouraria\Service\RelatorioRegistroCaixaService;
use Tesouraria\Service\DescricaoLancamentoService;
use Tesouraria\Form\RegistroCaixaForm;
use Tesouraria\Form\MovimentoContaForm;
class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Registra as ViewHelpers
     * @return array ViewHelpers
     */
    public function getViewHelperConfig() {
        return array(
            'invokables' => array(
                'RelatorioMovimentoCaixa' => 'Tesouraria\View\Helper\RelatorioMovimentoCaixaHelper',
                'FormatarValorBalancete' => 'Tesouraria\View\Helper\FormatarValorBalanceteHelper'
            )
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'RegistroCaixaService' => function () {
                    return new RegistroCaixaService();
                },
                'RelatorioRegistroCaixaService' => function () {
                    return new RelatorioRegistroCaixaService();
                },
                'ValorRegistroCaixaService' => function () {
                    return new ValorRegistroCaixaService();
                },
                'RegistroCaixaForm' => function ($srvManager) {
                    $srvDoctrine = $srvManager->get('Doctrine\ORM\EntityManager');
                    /* @var $repo \Application\Repository\DescricaoLancamentoRepository */
                    $repoDesc = $srvDoctrine->getRepository('Application\Entity\DescricaoLancamento');
                    /* @var $repo \Application\Repository\TipoRegistroCaixaRepository */
                    $repoTpRegistro = $srvDoctrine->getRepository('Application\Entity\TipoRegistroCaixa');
                    $options = array(
                        'idDescLancamento' => $repoDesc->fetchPairs(),
                        'idTipoRegistroCaixa' => $repoTpRegistro->fetchPairs()
                    );
                    return new RegistroCaixaForm(null, $options);
                },
                'RelatorioService' => function () {
                    return new RelatorioService();
                },
                'DescricaoLancamentoService' => function () {
                    return new DescricaoLancamentoService();
                },
                'MovimentacaoContaService' => function () {
                    return new MovimentacaoContaService();
                },
                'DescricaoMovimentoContaService' => function () {
                    return new DescricaoMovimentoContaService();
                },
                'MovimentoContaForm' => function ($srvManager) {
                    $srvDoctrine = $srvManager->get('Doctrine\ORM\EntityManager');
                    /* @var $repoConta \Application\Repository\ContaRepository */
                    $repoConta = $srvDoctrine->getRepository('Application\Entity\Conta');
                    /* @var $srvMovConta \Tesouraria\Service\MovimentacaoContaService */
                    $srvMovConta = $srvManager->get('MovimentacaoContaService');
                    /* @var $repoDescMovconta \Application\Repository\DescricaoMovimentoContaRepository */
                    $repoDescMovconta = $srvDoctrine->getRepository('Application\Entity\DescricaoMovimentoConta');

                    $options = array(
                        'nuAnoMes' => $srvMovConta->listarAnoMesCombo(true),
                        'descricaoMovimentoConta' => $repoDescMovconta->fetchPairs(),
                        'conta' => $repoConta->fetchPairs()
                    );
                    return new MovimentoContaForm(null, $options);
                },
                'MovimentacaoContaDescricaoService' => function () {
                    return new MovimentacaoContaDescricaoService();
                },
                'ContaService' => function () {
                    return new ContaService();
                },
            )
        );
    }
}
