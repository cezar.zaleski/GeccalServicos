<?php


namespace Tesouraria\Form;

use Zend\InputFilter\InputFilter;

class RegistroCaixaFilter extends InputFilter {

    function __construct() {

        $this->add(array(
            'name' => 'dtRegistro',
            'required' => TRUE,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'E002'
                        )
                    )
                ),

            )
        ));
        $this->add(array(
            'name' => 'noDescricao',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'E002'
                        )
                    )
                ),

            )
        ));

        $this->add(array(
            'name' => 'idDescLancamento',
            'required' => TRUE,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
                array('name' => 'Digits')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'E002'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'idTipoRegistroCaixa',
            'required' => TRUE,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
                array('name' => 'Digits')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'E002'
                        )
                    )
                )
            )
        ));
    }
    
}
