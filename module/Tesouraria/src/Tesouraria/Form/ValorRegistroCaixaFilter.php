<?php


namespace Tesouraria\Form;

use Zend\InputFilter\InputFilter;

class ValorRegistroCaixaFilter extends InputFilter {

    function __construct() {

        $this->add(array(
            'name' => 'nuValor',
            'required' => TRUE,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'E002'
                        )
                    )
                ),

            )
        ));
        $this->add(array(
            'name' => 'idRegistroCaixa',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
                array('name' => 'Digits')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'E002'
                        )
                    )
                ),

            )
        ));

        $this->add(array(
            'name' => 'idTipoPagamento',
            'required' => TRUE,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
                array('name' => 'Digits')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'E002'
                        )
                    )
                )
            )
        ));
    }
    
}
