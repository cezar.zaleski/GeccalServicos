<?php

namespace Tesouraria\Form;

use Zend\Form\Form;

class RegistroCaixaForm extends Form{

    public function __construct($name = null, $options = array())
    {
        parent::__construct('registro-caixa');
        $this->setInputFilter(new RegistroCaixaFilter());
        
        $this->add(array(
            'name' => 'dtRegistro',
            'options' => array(
                'label' => 'Data',
                'type' => 'text'
            ),
            'attributes' => array(
                'required' => 'required'
            )
        ));

        $this->add(array(
            'name' => 'idRegistroCaixa',
            'options' => array(
                'type' => 'text'
            ),
        ));

        $this->add(array(
            'name' => 'noDescricao',
            'options' => array(
                'label' => 'Data',
                'type' => 'text'
            ),
            'attributes' => array(
                'required' => 'required'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'idDescLancamento',
            'options' => array(
                'label' => 'Descrição Lançamento',
                'value_options' => $options['idDescLancamento'],
            ),
            'attributes' => array(
                'required' => 'required'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'idTipoRegistroCaixa',
            'options' => array(
                'label' => 'Tipo Registro',
                'value_options' => $options['idTipoRegistroCaixa'],
            ),
            'attributes' => array(
                'required' => 'required'
            )
        ));
        
    }

    
}
