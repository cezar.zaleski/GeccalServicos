<?php

namespace Tesouraria\Form;

use Zend\Form\Form;

class ValorRegistroCaixaForm extends Form{
    
    public function __construct() {
        parent::__construct('valor-registro-caixa');
        $this->setInputFilter(new ValorRegistroCaixaFilter());
        
        $this->add(array(
            'name' => 'nuValor',
            'options' => array(
                'label' => 'Valor',
                'type' => 'text'
            ),
            'attributes' => array(
                'required' => 'required'
            )
        ));
        $this->add(array(
            'name' => 'idRegistroCaixa',
            'options' => array(
                'type' => 'text'
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'idTipoPagamento',
            'options' => array(
                'label' => 'Forma Pagamento',
                'value_options' => array(
                    '1' => 'Dinheiro',
                    '2' => 'Cheque',
                    '3' => 'Cartao Debito',
                    '4' => 'Cartao Credito',
                ),
            ),
            'attributes' => array(
                'required' => 'required'
            )
        ));
        
    }

    
}
