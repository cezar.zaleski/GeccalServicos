<?php


namespace Tesouraria\Form;


use Zend\InputFilter\InputFilter;

class MovimentoContaFilter extends InputFilter {

    public function __construct()
    {

        $this->add(array(
            'name' => 'dsTransferencia',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'E005')
                    )),
            )
        ));

        $this->add(array(
            'name' => 'dsDebito',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'E005')
                    )),
            )
        ));
        $this->add(array(
            'name' => 'nuCheque',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'E002')
                    )),
            )
        ));

        $this->add(array(
            'name' => 'descricaoMovimentoConta',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
                array('name' => 'Digits')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'E002'),
                    ),
                )
            ),
        ));



        $this->add(array(
            'name' => 'nuAnoMes',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
                array('name' => 'Digits')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'E002'
                        )
                    )
                )
            )
        ));



        $this->add(array(
            'name' => 'nuValor',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'E002'
                        )
                    )
                ),

            )
        ));

        $this->add(array(
            'name' => 'conta',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
                array('name' => 'Digits')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'E002'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'tipoRegistroCaixa',
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array('isEmpty' => 'E002')
                    )),
            )
        ));
    }
}
