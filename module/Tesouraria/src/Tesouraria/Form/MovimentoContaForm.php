<?php

namespace Tesouraria\Form;

use Zend\Form\Form;

class MovimentoContaForm extends Form{

    public function __construct($name = null, $options = array())
    {
        parent::__construct('movimento-conta');
        $this->setInputFilter(new MovimentoContaFilter());
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'nuAnoMes',
            'options' => array(
                'label' => 'Mês/Ano',
                'value_options' => $options['nuAnoMes'],
            ),
            'attributes' => array(
                'required' => 'required'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'descricaoMovimentoConta',
            'options' => array(
                'label' => 'Descrição',
                'value_options' => $options['descricaoMovimentoConta'],
            ),
            'attributes' => array(
                'required' => 'required'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'conta',
            'options' => array(
                'label' => 'conta',
                'value_options' => $options['conta'],
            ),
            'attributes' => array(
                'required' => 'required'
            )
        ));

        $this->add(array(
            'name' => 'nuValor',
            'options' => array(
                'label' => 'Valor',
                'type' => 'text'
            )
        ));

        $this->add(array(
            'name' => 'nuCheque',
            'type' => 'text',
            'options' => array(
                'label' => 'Cheque nº'
            ),
            'attributes' => array(
                'id' => 'nuCheque',
            ),
        ));

        $this->add(array(
            'name' => 'dsTransferencia',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'label' => 'Descrição Transferência',
                'id' => 'dsTransferencia',
            ),
        ));
        $this->add(array(
            'name' => 'dsDebito',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'label' => 'Descrição Débito Automático',
                'id' => 'dsDebito',
            ),
        ));
        $this->add(array(
            'name' => 'tipoRegistroCaixa',
            'type' => 'text',
            'options' => array(
                'label' => 'Tipo de Registro'
            ),
            'attributes' => array(
                'id' => 'tipoRegistroCaixa',
            ),
        ));
    }
}
