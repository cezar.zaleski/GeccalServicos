<?php


namespace Tesouraria\View\Helper;

use Zend\View\Helper\AbstractHelper;

class RelatorioMovimentoCaixaHelper extends AbstractHelper {


    /**
     * Retorna elemento do relatorio de movimento de caixa formatado
     * @param string $tpMovimento
     * @param array $registro
     * @return string
     */
    public function getElemento($tpMovimento, $registro, &$saldo)
    {
        $mvCaixa = isset($registro[$tpMovimento]) ? number_format($registro[$tpMovimento], 2,',', '.') : '0,00';
        $class = \Application\Entity\TipoRegistroCaixa::TP_REG_SAIDA == $registro['idTipoRegistroCaixa'] ? 'saida' : 'entrada';
        if($mvCaixa > 0 && $registro['idTipoRegistroCaixa'] == \Application\Entity\TipoRegistroCaixa::TP_REG_SAIDA){
            if(!isset($registro['slDinheiro'])){
                $saldo -=$registro[$tpMovimento];
            }

            return "<td><font color='red'> (R$ -{$mvCaixa})</font></td>";
        }
        if($mvCaixa > 0 && $registro['idTipoRegistroCaixa'] == \Application\Entity\TipoRegistroCaixa::TP_REG_ENTRADA &&
            !isset($registro['slDinheiro'])){
            $saldo +=$registro[$tpMovimento];
        }

        return "<td>R$ {$mvCaixa}</td>";

    }

}
