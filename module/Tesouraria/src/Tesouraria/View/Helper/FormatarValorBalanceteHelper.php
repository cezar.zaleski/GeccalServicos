<?php


namespace Tesouraria\View\Helper;

use Zend\View\Helper\AbstractHelper;

class FormatarValorBalanceteHelper extends AbstractHelper {


    /**
     * Formatar valor para apresentação em balancete
     * @param float $nuValor
     * @return string
     */
    public function formatar($nuValor)
    {
        if(is_null($nuValor)){
            return $nuValor;
        }
        return $nuValor > 0 ? "R$ ".number_format($nuValor, 2, ',', '.'): ' - ';
    }

}
