<?php
/**
 * Created by IntelliJ IDEA.
 * User: familia
 * Date: 13/12/15
 * Time: 22:44
 */

namespace Tesouraria\Controller;
use Application\Service\Exception\ServiceException;
use Application\Mvc\Controller\AbstractRestController;
use Tesouraria\Form\RegistroCaixaForm;
use Zend\View\Model\JsonModel;

class RegistroCaixaController extends AbstractRestController{

    public function getList()
    {
        try {
            /* @var $srvRegistroCaixa \Tesouraria\Service\RegistroCaixaService */
            $srvRegistroCaixa = $this->getServiceLocator()->get('RegistroCaixaService');
            $result = $srvRegistroCaixa->listarRegistroCaixa();
            return $this->renderResult($result);
        } catch (ServiceException $e) {
            return $this->errorExeception($e);
        }
    }

    public function get($id)
    {
        try {
            /* @var $srvRegistroCaixa \Tesouraria\Service\RegistroCaixaService */
            $srvRegistroCaixa = $this->getServiceLocator()->get('RegistroCaixaService');
            $result = $srvRegistroCaixa->findByRegistro($id);
            return $this->renderResult(current($result));
        } catch (ServiceException $e) {
            return $this->errorExeception($e);
        }
    }

    public function create($data)
    {
        try{
            /* @var $form \Tesouraria\Form\RegistroCaixaForm */
            $form = $this->getServiceLocator()->get('RegistroCaixaForm');
            $form->setData($data);
            if($form->isValid()){
                /* @var $srvRegitroCaixa \Tesouraria\Service\RegistroCaixaService */
                $srvRegitroCaixa = $this->getServiceLocator()->get('RegistroCaixaService');
                $return = $srvRegitroCaixa->salvarRegistroCaixa($form->getData());
            }else{
                return $this->formErrorException($form);
            }
            return $this->renderResult($return);
        } catch (\Exception $e) {
            return $this->errorException($e);
        }

    }

    public function update($id, $data)
    {
        try{
            /* @var $form \Tesouraria\Form\RegistroCaixaForm */
            $form = $this->getServiceLocator()->get('RegistroCaixaForm');
            $data['id_registro_caixa'] = $id;

            $form->setData($data);
            if($form->isValid()){
                /* @var $srvRegitroCaixa \Tesouraria\Service\RegistroCaixaService */
                $srvRegitroCaixa = $this->getServiceLocator()->get('RegistroCaixaService');
                $return = $srvRegitroCaixa->salvarRegistroCaixa($form->getData());
            }else{
                return $this->formErrorException($form);
            }
            return $this->renderResult($return);

        } catch (ServiceException $e) {
            return $this->errorException($e);
        }

    }

    public function delete($id)
    {
        try{
            /* @var $srvRegitroCaixa \Tesouraria\Service\RegistroCaixaService */
            $srvRegitroCaixa = $this->getServiceLocator()->get('RegistroCaixaService');
            $return = $srvRegitroCaixa->removerRegistroCaixa($id);
            return $this->renderResult($return);

        } catch (ServiceException $e) {
            return $this->errorException($e);
        }

    }


}