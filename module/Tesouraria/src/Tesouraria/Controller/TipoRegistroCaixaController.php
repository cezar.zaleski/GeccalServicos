<?php
/**
 * Created by IntelliJ IDEA.
 * User: familia
 * Date: 13/12/15
 * Time: 22:44
 */

namespace Tesouraria\Controller;
use Application\Service\Exception\ServiceException;
use Application\Mvc\Controller\AbstractRestController;

class TipoRegistroCaixaController extends AbstractRestController{

    public function getList()
    {
        try {
            /* @var $repo \Application\Repository\TipoRegistroCaixaRepository */
            $repo = $this->getEm()->getRepository('Application\Entity\TipoRegistroCaixa');
            $result = $repo->findAll();
            return $this->renderResult($result);
        } catch (ServiceException $e) {
            return $this->errorExeception($e);
        }
    }

    public function get($id)
    {
        return $this->renderResult(array('response'=>'get'));
    }

    public function create($data)
    {
        return $this->renderResult(array('response'=>'create'));

    }

    public function update($id, $data)
    {
        return $this->renderResult(array('response'=>'update'));

    }

    public function delete($id)
    {
        return $this->renderResult(array('response'=>'delete'));

    }


}