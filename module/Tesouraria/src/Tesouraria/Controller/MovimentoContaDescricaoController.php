<?php
/**
 * Created by IntelliJ IDEA.
 * User: familia
 * Date: 13/12/15
 * Time: 22:44
 */

namespace Tesouraria\Controller;
use Application\Service\Exception\ServiceException;
use Application\Mvc\Controller\AbstractRestController;

class MovimentoContaDescricaoController extends AbstractRestController{

    public function getList()
    {
        try {
            $nuAnoMes = $this->params()->fromQuery('nuAnoMes');
            $idConta = $this->params()->fromQuery('idConta');

            /* @var $srvMovContaDescricao \Tesouraria\Service\MovimentacaoContaDescricaoService */
            $srvMovContaDescricao = $this->getServiceLocator()->get('MovimentacaoContaDescricaoService');
            $result = $srvMovContaDescricao->buscarMovimentoContaDescricao($idConta, $nuAnoMes);
            return $this->renderResult($result);
        } catch (ServiceException $e) {
            return $this->errorExeception($e);
        }
    }

    public function get($id)
    {


    }

    public function create($data)
    {
        return $this->renderResult(array('response'=>'create'));

    }

    public function update($id, $data)
    {
        return $this->renderResult(array('response'=>'update'));

    }

    public function delete($id)
    {
        return $this->renderResult(array('response'=>'delete'));

    }


}