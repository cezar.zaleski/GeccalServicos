<?php
/**
 * Created by IntelliJ IDEA.
 * User: familia
 * Date: 13/12/15
 * Time: 22:44
 */

namespace Tesouraria\Controller;
use Application\Service\Exception\ServiceException;
use Application\Mvc\Controller\AbstractRestController;

class GerarRelatorioController extends AbstractRestController{

    public function getList()
    {
        try {
            $mes = $this->params()->fromQuery('mes',0);
            $ano = $this->params()->fromQuery('ano',0);
            $tipo = $this->params()->fromQuery('tipo',0);

            /* @var $srvRelatorio \Tesouraria\Service\RelatorioService */
            $srvRelatorio = $this->getServiceLocator()->get('RelatorioService');
            $file = $srvRelatorio->gerarRelatorio($tipo, $mes,$ano);
        return $file;
        } catch (ServiceException $e) {
            return $this->errorExeception($e);
        }
    }

    public function get($id)
    {
        return $this->renderResult(array('response'=>'get'));
    }

    public function create($data)
    {
        return $this->renderResult(array('response'=>'create'));

    }

    public function update($id, $data)
    {
        return $this->renderResult(array('response'=>'update'));

    }

    public function delete($id)
    {
        return $this->renderResult(array('response'=>'delete'));

    }


}