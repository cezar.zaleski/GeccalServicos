<?php
/**
 * Created by IntelliJ IDEA.
 * User: familia
 * Date: 13/12/15
 * Time: 22:44
 */

namespace Tesouraria\Controller;
use Application\Service\Exception\ServiceException;
use Application\Mvc\Controller\AbstractRestController;
use Tesouraria\Form\ValorRegistroCaixaForm;

class ValorRegistroCaixaController extends AbstractRestController{

    public function getList()
    {
        try {
            /* @var $srvRegistroCaixa \Tesouraria\Service\RegistroCaixaService */
            $srvRegistroCaixa = $this->getServiceLocator()->get('RegistroCaixaService');
            $result = $srvRegistroCaixa->listarRegistroCaixa();
            return $this->renderResult($result);
        } catch (ServiceException $e) {
            return $this->errorExeception($e);
        }
    }

    public function get($id)
    {
        try {
            /* @var $srvValorRegCaixa \Tesouraria\Service\ValorRegistroCaixaService */
            $srvValorRegCaixa = $this->getServiceLocator()->get('ValorRegistroCaixaService');
            $result = $srvValorRegCaixa->listarPagamentoRegistroCaixa($id);
            return $this->renderResult(array('pagamento' =>$result));
        } catch (ServiceException $e) {
            return $this->errorExeception($e);
        }
    }

    public function create($data)
    {
        try{
            $form = new ValorRegistroCaixaForm();
            $form->setData(
              array(
                  'nuValor' => $data['nuValor'],
                  'idRegistroCaixa' => $data['idRegistroCaixa'],
                  'idTipoPagamento' => $data['idTipoPagamento']['id_tipo_pagamento'],
              )
            );
            if($form->isValid()){
                /* @var $srvValorRegCaixa \Tesouraria\Service\ValorRegistroCaixaService */
                $srvValorRegCaixa = $this->getServiceLocator()->get('ValorRegistroCaixaService');
                $return = $srvValorRegCaixa->gravar($form->getData());
            }else{
            }
            return $this->renderResult($return);

        } catch (ServiceException $e) {
            return $this->errorException($e);
        }

    }

    public function update($id, $data)
    {
        return $this->renderResult(array('update'));

    }

    public function delete($id)
    {
        try{
            /* @var $srvValorRegCaixa \Tesouraria\Service\ValorRegistroCaixaService */
            $srvValorRegCaixa = $this->getServiceLocator()->get('ValorRegistroCaixaService');
            $return = $srvValorRegCaixa->removerPagamento($id);
            return $this->renderResult($return);
        }catch(ServiceException $e){
            return $this->errorException($e);
        }

    }


}