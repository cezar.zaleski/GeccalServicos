<?php
/**
 * Created by IntelliJ IDEA.
 * User: familia
 * Date: 13/12/15
 * Time: 22:44
 */

namespace Tesouraria\Controller;
use Application\Service\Exception\ServiceException;
use Application\Mvc\Controller\AbstractRestController;

class AnoMesMovimentacaoController extends AbstractRestController{

    public function getList()
    {
        try {
            /* @var $srvMovConta \Tesouraria\Service\MovimentacaoContaService */
            $srvMovConta = $this->getSm('MovimentacaoContaService');
            $result = $srvMovConta->listarAnoMesCombo();
            return $this->renderResult($result);
        } catch (ServiceException $e) {
            echo $e->getMessage();exit;
            return $this->errorExeception($e);
        }
    }

    public function get($id)
    {


    }

    public function create($data)
    {
        try{
            /* @var $form \Tesouraria\Form\MovimentoContaForm */
            $form = $this->getServiceLocator()->get('MovimentoContaForm');
            $form->setData($data);
            if($form->isValid()){
                /* @var $srvMovConta \Tesouraria\Service\MovimentacaoContaService */
                $srvMovConta = $this->getServiceLocator()->get('MovimentacaoContaService');
                $return = $srvMovConta->salvarMovimentoConta($form->getData());
            }else{
                return $this->formErrorException($form);
            }
            return $this->renderResult($return);
        } catch (\Exception $e) {
            return $this->errorException($e);
        }

    }

    public function update($id, $data)
    {
        return $this->renderResult(array('response'=>'update'));

    }

    public function delete($id)
    {
        try{
            /* @var $srvMovContaDescricao \Tesouraria\Service\MovimentacaoContaDescricaoService */
            $srvMovContaDescricao = $this->getServiceLocator()->get('MovimentacaoContaDescricaoService');
            $return = $srvMovContaDescricao->removerMovimentoContaDescricao($id);
            return $this->renderResult($return);

        } catch (ServiceException $e) {
            return $this->errorException($e);
        }

    }


}