<?php

namespace Tesouraria\Service;

use Application\Service\AbstractService;
use Application\Service\Exception\ServiceException;
use Application\Entity\MovimentacaoContaDescricao;


class MovimentacaoContaDescricaoService extends AbstractService {

    /**
     * DescricaoMovimentacaoService constructor.
     */
    public function __construct()
    {
        $this->setEntity('Application\Entity\MovimentacaoContaDescricao');
    }


    public function gravarDescricaoMovimentoConta($data)
    {
        /* @var $repo \Application\Repository\MovimentacaoContaDescricaoRepository */
        $repo = $this->getDefaultRepository();
        $objMovContaDesc = $repo->findBy(array(
            'movimentacaoConta' =>$data['movimentacaoConta']->getNuAnoMes(),
            'descricaoMovimentoConta' => $data['descricaoMovimentoConta']->getIdDescMovimentoConta()
        ));
        if(count($objMovContaDesc)){
            throw new ServiceException('MSG002', 401);
        }
        $em = $this->getDefaultEntityManager();
        $eMovContaDesc = new MovimentacaoContaDescricao($data);
        $em->persist($eMovContaDesc);
        $em->flush();
        $em->clear();

    }

    public function buscarMovimentoContaDescricao($idConta, $nuAnoMes)
    {
        if (empty($idConta)) {
            $this->addErrorMensages('E002', 'Conta');
        }
        if (empty($nuAnoMes)) {
            $this->addErrorMensages('E002', 'Mês e Ano');
        }
        /* @var $repo \Application\Repository\MovimentacaoContaDescricaoRepository */
        $repo = $this->getDefaultRepository();
        return $repo->listarMovimentoDescricao($idConta, $nuAnoMes);

    }

    public function removerMovimentoContaDescricao($id)
    {
        try{
            if(empty($id)){
                throw new ServiceException(sprintf('E002', 'Códido'), 401);
            }
            /* @var $repo \Application\Repository\MovimentacaoContaDescricaoRepository */
            $repo = $this->getDefaultRepository();
            $explode = explode('999', $id);
            $repo->removerMovimentacao($explode[0], $explode[1]);
            return array(
                'stSucesso' => true
            );
        }catch(\Exception $e){
            throw new ServiceException($e->getMessage(), 401);
        }

    }
}
