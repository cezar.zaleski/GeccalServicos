<?php

namespace Tesouraria\Service;

use PHPExcel_Style_Border;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;
use PHPExcel_IOFactory;

class RelatorioRegistroCaixaService extends AbstractRelatorioService{
    
    protected $linha;
    protected $objPHPExcel;
    protected $mesAno;
    
    
    public function __construct()
    {
        $this->objPHPExcel = PHPExcel_IOFactory::load(__DIR__ .self::TEMPLATE_REGISTRO_CAIXA);
    }


    /**
     * Gerar relatorio de registro de caixa
     * @param array $dados
     * @param int $mes
     * @param int $ano
     * @param \stdClass $saldo
     * @throws \PHPExcel_Exception
     */
    public function gerar($dados, $mes, $ano, \stdClass $saldo)
    {
        $this->mesAno    = $this->mesPorExtenso($mes)."/".$ano;
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,3,  $this->mesAno);
        $this->linha = 5;
        $entrada = $saida = 0;
        $contador = 0;
        foreach ($dados as $value){
            $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->objPHPExcel->getActiveSheet()->getStyle("B".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->objPHPExcel->getActiveSheet()->getStyle("C".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->objPHPExcel->getActiveSheet()->getStyle("E".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, $value['dtRegistro']->format('d'));
            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $this->linha, "{$value['coDescLancamento']} - {$value['noDescricao']}");
            $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($value['idTipoRegistroCaixa'] == 1 ? 3 : 4, $this->linha, $value['nuValor']);
            $value['idTipoRegistroCaixa'] == 1 ? $entrada+=$value['nuValor'] : $saida+=$value['nuValor'];
            $this->linha ++;
            if($contador > 24){
                $this->rodape($entrada, $saida, $saldo);
                $this->cabecalho($entrada,$saida);
                $contador = 0;
            }
            $contador ++;
        }
        $this->objPHPExcel = $this->rodape($entrada, $saida, $saldo);
        $nome = "Relatório de Caixa {$mes}-{$ano}";
        return $this->downloadRelatorio($this->objPHPExcel,$nome);
    }
    
    /**
     * Geraçao do rodape do relatorio
     * @param array $entrada
     * @param array $saida
     * @param \stdClass $saldo
     * @return \PHPExcel
     * @throws \PHPExcel_Exception
     */
    public function rodape($entrada, $saida, \stdClass $saldo) {
        //linha a transportar
        $this->objPHPExcel->getActiveSheet()->getStyle("B".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->objPHPExcel->getActiveSheet()->getRowDimension($this->linha)->setRowHeight(26);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $this->linha, "A TRANSPORTAR TOTAIS DO DIA .... R$");
        $this->objPHPExcel->getActiveSheet()->getStyle("C".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("C{$this->linha}:E{$this->linha}")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'A8A8A8')));
        $this->objPHPExcel->getActiveSheet()->getStyle("C".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("E".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $this->linha, $entrada);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $this->linha, $saida);
        $this->linha++;
        //linha saldo anterior
        $saldoAnterior = $saldo->saldoAnterior['slTotal'];
        $this->objPHPExcel->getActiveSheet()->getStyle("B".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->objPHPExcel->getActiveSheet()->getRowDimension($this->linha)->setRowHeight(24);
        $this->objPHPExcel->getActiveSheet()->mergeCells("B{$this->linha}:C{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $this->linha, "Saldo Anterior .... R$");
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $this->linha, $saldoAnterior);
        $this->objPHPExcel->getActiveSheet()->getStyle("E{$this->linha}")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'A8A8A8')));
        $this->objPHPExcel->getActiveSheet()->getStyle("E".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->linha++;
        //linha saldo atual
        $saldoAtual = $saldo->saldoAtual['slTotal'];
        $this->objPHPExcel->getActiveSheet()->getStyle("B".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->objPHPExcel->getActiveSheet()->getRowDimension($this->linha)->setRowHeight(24);
        $this->objPHPExcel->getActiveSheet()->mergeCells("B{$this->linha}:C{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $this->linha, "Saldo Atual .... R$");
        $this->objPHPExcel->getActiveSheet()->getStyle("D{$this->linha}")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'A8A8A8')));
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("E".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $this->linha, $saldoAtual);
        $this->linha++;
        //somas para conferência
        $this->objPHPExcel->getActiveSheet()->getStyle("B".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->objPHPExcel->getActiveSheet()->getRowDimension($this->linha)->setRowHeight(24);
        $this->objPHPExcel->getActiveSheet()->mergeCells("B{$this->linha}:C{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $this->linha, "(Somas para conferência) .... R$");
        $this->objPHPExcel->getActiveSheet()->getStyle("D{$this->linha}")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'A8A8A8')));
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $this->linha, $entrada+$saldoAnterior);
        $this->objPHPExcel->getActiveSheet()->getStyle("E{$this->linha}")->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,'startcolor' => array('rgb' =>'A8A8A8')));
        $this->objPHPExcel->getActiveSheet()->getStyle("E".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $this->linha, $saida+$saldoAtual);
        
        return $this->detalhesSaldo($saldo->saldoAtual);
    }

    /**
     * Gera o detalhe de saldo
     * @param array $saldoAtual
     * @return \PHPExcel
     * @throws \PHPExcel_Exception
     */
    public function detalhesSaldo(array $saldoAtual) {
        //linha detalhes do saldo
        $this->linha += 2;
        $this->linhaIniBorda = $this->linha;
        $this->linhaFimBorda = $this->linha +7;
        $this->objPHPExcel->getActiveSheet()->getStyle("A{$this->linha}")->getFont()->setBold(true);
        $this->objPHPExcel->getActiveSheet()->mergeCells("A{$this->linha}:D{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, "DETALHES DO SALDO");
        $this->linha++;
        //linha dinheiro
        $this->objPHPExcel->getActiveSheet()->mergeCells("A{$this->linha}:C{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, "Dinheiro");
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $this->linha, $saldoAtual['slDinheiro']);
        $this->linha++;
        //linha cheque
        $this->objPHPExcel->getActiveSheet()->mergeCells("A{$this->linha}:C{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, "Cheque");
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $this->linha, $saldoAtual['slCheque']);
        $this->linha++;
        //linha cartão débito
        $this->objPHPExcel->getActiveSheet()->mergeCells("A{$this->linha}:C{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, "Cartão Débito");
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $this->linha, $saldoAtual['slDebito']);
        $this->linha++;
        //linha cartão crédito
        $this->objPHPExcel->getActiveSheet()->mergeCells("A{$this->linha}:C{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, "Cartão Crédito");
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $this->linha, $saldoAtual['slCredito']);
        $this->linha++;
        //linha Conta Corrente  - Banco do Brasil
        $this->objPHPExcel->getActiveSheet()->mergeCells("A{$this->linha}:C{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, "Conta Corrente  - Banco do Brasil");
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $this->linha, 1234);
        $this->linha++;
        //linha Conta Poupança - Banco do Brasil
        $this->objPHPExcel->getActiveSheet()->mergeCells("A{$this->linha}:C{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, "Conta Poupança - Banco do Brasil");
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $this->linha, 1234);
        $this->linha++;
        //linha total
        $this->objPHPExcel->getActiveSheet()->mergeCells("A{$this->linha}:C{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, "TOTAL");
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $this->linha, $saldoAtual['slTotal'] + 1234 *2);
        //formatacao borda quadro
        $this->objPHPExcel->getActiveSheet()->getStyle("A{$this->linhaIniBorda}:D{$this->linhaFimBorda}")->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
        //visto
        $this->linha+=2;
        $linhaBdVisto = $this->linha+1;
        $this->objPHPExcel->getActiveSheet()->mergeCells("A{$this->linha}:B{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->getStyle("A{$this->linha}:B{$linhaBdVisto}")->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, "Caixa");
        $this->objPHPExcel->getActiveSheet()->mergeCells("C{$this->linha}:E{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $this->linha, "Visto");
        $this->objPHPExcel->getActiveSheet()->getStyle("C{$this->linha}:E{$linhaBdVisto}")->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("C".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $this->linha +=2;
        return $this->objPHPExcel;
    }

    /**
     * Geraçao do cacbeçalho do relatorio
     * @param array $entrada
     * @param array $saida
     * @return \PHPExcel
     * @throws \PHPExcel_Exception
     */
    public function cabecalho($entrada, $saida) 
    {
        //linha MOVIMENTO CAIXA
        $this->objPHPExcel->getActiveSheet()->getStyle("A{$this->linha}")->getFont()->setBold(true);
        $this->objPHPExcel->getActiveSheet()->mergeCells("A{$this->linha}:E{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->getStyle("A{$this->linha}:E{$this->linha}")->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, "MOVIMENTO DO CAIXA");
        $this->linha++;
        //linha GRUPO ESPÍRITA CRISTÃO
        $this->objPHPExcel->getActiveSheet()->getStyle("A{$this->linha}")->getFont()->setBold(true);
        $this->objPHPExcel->getActiveSheet()->mergeCells("A{$this->linha}:E{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->getStyle("A{$this->linha}:E{$this->linha}")->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, "GRUPO ESPÍRITA CRISTÃO A CAMINHO DA LUZ");
        $this->linha++;
        //linha MÊS/ANO
        $this->objPHPExcel->getActiveSheet()->getStyle("A{$this->linha}")->getFont()->setBold(true);
        $this->objPHPExcel->getActiveSheet()->mergeCells("A{$this->linha}:E{$this->linha}");
        $this->objPHPExcel->getActiveSheet()->getStyle("A{$this->linha}:E{$this->linha}")->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, $this->mesAno);
        $this->linha++;
        //linha DOC/DESCRIÇÃO/ENTRADAS/SAÍDAS
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("A{$this->linha}")->getFont()->setBold(true);
        $this->objPHPExcel->getActiveSheet()->getStyle("A".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $this->linha, "Doc");
        
        $this->objPHPExcel->getActiveSheet()->getStyle("B".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("B{$this->linha}")->getFont()->setBold(true);
        $this->objPHPExcel->getActiveSheet()->getStyle("B".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $this->linha, "DESCRIÇÃO");
        
        $this->objPHPExcel->getActiveSheet()->getStyle("C".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("D{$this->linha}")->getFont()->setBold(true);
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $this->linha, "ENTRADAS");
        
        $this->objPHPExcel->getActiveSheet()->getStyle("E".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("E{$this->linha}")->getFont()->setBold(true);
        $this->objPHPExcel->getActiveSheet()->getStyle("E".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $this->linha, "SAIDAS");
        $this->linha ++;
        //linha TRANSPORTE"A{$this->linha}:E{$this->linha}"
        $this->objPHPExcel->getActiveSheet()->getStyle("B".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("B{$this->linha}")->getFont()->setBold(true);
        $this->objPHPExcel->getActiveSheet()->getStyle("B".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $this->linha, "TRANSPORTE");
        
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("D{$this->linha}")->getFont()->setBold(true);
        $this->objPHPExcel->getActiveSheet()->getStyle("D".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $this->linha, $entrada);
        
        $this->objPHPExcel->getActiveSheet()->getStyle("E".$this->linha)->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $this->objPHPExcel->getActiveSheet()->getStyle("E{$this->linha}")->getFont()->setBold(true);
        $this->objPHPExcel->getActiveSheet()->getStyle("E".$this->linha)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $this->objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $this->linha, $saida);
        $this->linha++;
        return $this->objPHPExcel;
    }
}
