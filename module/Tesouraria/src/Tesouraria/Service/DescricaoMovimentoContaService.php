<?php

namespace Tesouraria\Service;

use Application\Service\AbstractService;
use Application\Service\Exception\ServiceException;
use Zend\Stdlib\DateTime;


class DescricaoMovimentoContaService extends AbstractService {

    /**
     * DescricaoMovimentacaoService constructor.
     */
    public function __construct()
    {
        $this->setEntity('Application\Entity\DescricaoMovimentoConta');
    }

    public function listarDescricao($idConta, $nuAnoMes)
    {
        if (empty($idConta)) {
            $this->addErrorMensages('E002', 'Conta');
        }
        if (empty($nuAnoMes)) {
            $this->addErrorMensages('E002', 'Mês e Ano');
        }
        /* @var $repo \Application\Repository\DescricaoMovimentoContaRepository */
        $repo = $this->getDefaultRepository();
//        return $repo->findBy(array(), array('noDescMovimentacaoConta' => 'ASC'));
        return $repo->buscarDescricaoNaoCadastrada($idConta, $nuAnoMes);
    }
}
