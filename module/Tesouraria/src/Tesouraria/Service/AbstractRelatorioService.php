<?php
namespace Tesouraria\Service;
use PHPExcel_IOFactory;
use Application\Service\AbstractService;


abstract class AbstractRelatorioService extends AbstractService{
    
    const TEMPLATE_REGISTRO_CAIXA   = '/../../../../../public/data/Registro Caixa.xls';
    const TEMPLATE_MOVIMENTO_CAIXA  = '/../../../../../public/data/Movimento Caixa.xls';
    const TEMPLATE_BALANCETE        = '/../../../../../public/data/Balancete Geccal.xls';
    protected $nomeArquivo;

    

    
    public function downloadRelatorio($objPHPExcel,$nome) 
    {
        // Cabeçalho do arquivo para ele baixar
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$nome.'.xls"');
        header('Cache-Control: max-age=0');
        // Se for o IE9, isso talvez seja necessário
        header('Cache-Control: max-age=1');
        // Acessamos o 'Writer' para poder salvar o arquivo
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        // Salva diretamente no output, poderíamos mudar arqui para um nome de arquivo em um diretório ,caso não quisessemos jogar na tela
        $objWriter->save('php://output'); 
    }
}
