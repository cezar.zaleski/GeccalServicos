<?php

namespace Tesouraria\Service;

use Application\Entity\RegistroCaixa;
use Application\Service\AbstractService;
use Application\Service\Exception\ServiceException;
use Zend\Debug\Debug;


class ContaService extends AbstractService {

    public function __construct()
    {
        $this->setEntity('Application\Entity\Conta');
    }

    public function saldoContaBalancete($mes, $ano)
    {
        /* @var $repo \Application\Repository\ContaRepository */
        $repo = $this->getDefaultRepository();
        $nuAnoMes = ($ano - 1).'12';
        $nuAnoMesAnterior = $nuAnoMes;
        $saldo = $repo->saldoContaAnoBalancete($nuAnoMes);
        if($mes > 1){
            $mes - 1;
        }
        $nuAnoMes = $ano.str_pad($mes, 2, 0, STR_PAD_LEFT);
        $saldoMes = $repo->saldoContaMesBalancete($nuAnoMes);
        if ($mes == 1) {
            $saldoMesAnterior = $repo->saldoContaMesBalancete($nuAnoMesAnterior);
        } else {
            $nuAnoMesAnterior = $ano.str_pad($mes - 1, 2, 0, STR_PAD_LEFT);
            $saldoMesAnterior = $repo->saldoContaMesBalancete($nuAnoMesAnterior);
        }
        $total = count($saldo);
        $count = 0;
        while($count < $total){
            $saldo[$count]['saldo_mes'] = $saldoMes[$count]['nu_saldo'];
            $count++;
        }
        $saldo['saldoMesAnterior'] = $saldoMesAnterior;
        return $saldo;
    }
}
