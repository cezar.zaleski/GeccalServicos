<?php

namespace Tesouraria\Service;

use Application\Entity\RegistroCaixa;
use Application\Service\AbstractService;
use Application\Service\Exception\ServiceException;
use Zend\Debug\Debug;


class RegistroCaixaService extends AbstractService {

    /**
     * Lista todos os registros de caixa
     * @return array
     */
    public function listarRegistroCaixa()
    {
        /* @var $repo \Application\Repository\RegistroCaixaRepository */
        $repo = $this->getDefaultEntityManager()->getRepository('Application\Entity\RegistroCaixa');
        return $repo->findAllRegistro();
    }

    /**
     * Recupera registro de caixa pelo identificador
     * @param int $idRegistroCaixa
     * @return array
     */
    public function findByRegistro($idRegistroCaixa)
    {
        /* @var $repo \Application\Repository\RegistroCaixaRepository */
        $repo = $this->getDefaultEntityManager()->getRepository('Application\Entity\RegistroCaixa');
        return $repo->findByRegistro($idRegistroCaixa);
    }


    /**
     * Grava o registro de caixa
     * @param array $data
     * @return mixed
     */
    public function gravar($data){

        $this->setEntity('Application\Entity\RegistroCaixa');

        return $this->insert($data);
    }

    /**
     * Realiza atualização ou inserção do registro de caixa
     * @param array $data
     * @return array
     */
    public function salvarRegistroCaixa($data)
    {
        try{
            $this->validarRegistroCaixa($data);
            $this->setEntity('Application\Entity\RegistroCaixa');
            $em = $this->getDefaultEntityManager();
            $data['stAtivo'] = true;
            $data['dtRegistro'] = \DateTime::createFromFormat('Y-m-d', $data['dtRegistro']);
            $data['descLancamento'] = $em->getPartialReference('Application\Entity\DescricaoLancamento', $data['idDescLancamento']);
            $data['tipoRegistroCaixa'] = $em->getPartialReference('Application\Entity\TipoRegistroCaixa', $data['idTipoRegistroCaixa']);
            $data['usuario'] = $em->getPartialReference('Application\Entity\Usuario', 1);

            if(empty($data['idRegistroCaixa'])){
                $this->gravar($data);
                return array(
                    'stSucesso' => true
                );
            }
            $this->update($data, $data['idRegistroCaixa']);
            return array(
                'stSucesso' => true
            );

        }catch(\Exception $e){
            throw new ServiceException($e->getMessage(), 401);
        }

    }

    /**
     * Retorna o saldo do registro de caixa até a data informada
     * @param \DateTime $dtParametro
     * @return array
     */
    public function saldo(\DateTime $dtParametro)
    {
        /* @var $repo \Application\Repository\RegistroCaixaRepository */
        $repo = $this->getDefaultEntityManager()->getRepository('Application\Entity\RegistroCaixa');
        $saldo = current($repo->saldo($dtParametro));
        $saldo['slDinheiro'] += RegistroCaixa::slInicialDinheiro;
        $saldo['slCheque'] += RegistroCaixa::slInicialCheque;
        $saldo['slCredito'] += RegistroCaixa::slInicialCredito;
        $saldo['slDebito'] += RegistroCaixa::slInicialDebito;
        $saldo['slTotal'] = $saldo['slDinheiro'] + $saldo['slCheque'] + $saldo['slCredito'] + $saldo['slDebito'];

        return $saldo;
    }

    /**
     * Retorna o saldo anterior ao mes/ano informado
     * @param int $mes
     * @param int $ano
     * @return array
     */
    public function saldoAnterior($mes, $ano)
    {
        $dtParametro = \DateTime::createFromFormat('d-m-Y h', '01-'.$mes.'-'.$ano.'0');
        return $this->saldo($dtParametro);
    }

    /**
     * Retorna o saldo ate o ultimo dia do mes/ano informado
     * @param int $mes
     * @param int $ano
     * @return array
     */
    public function saldoAtual($mes, $ano)
    {
        $ano = $mes == 12 ? $ano + 1 : $ano;
        $mes = $mes == 12 ? 1 : $mes + 1;
        $dtParametro = \DateTime::createFromFormat('d-m-Y h', '01-'.$mes.'-'.$ano.'0');
        return $this->saldo($dtParametro);
    }

    /**
     * Valida o registro de caixa para persistência
     * @param array $data
     */
    private function validarRegistroCaixa(array $data)
    {
        /* @var $srvValorRegistro \Tesouraria\Service\ValorRegistroCaixaService */
        $srvValorRegistro = $this->getServiceManager()->get('ValorRegistroCaixaService');
        if(empty($data['idRegistroCaixa']) || !$srvValorRegistro->validarValorRegistroCaixa($data['idRegistroCaixa'])){
            throw new ServiceException('MSG001', 401);
        }
    }

    /**
     * Remover registro de caixa
     * @param int $idRegistro
     * @return array
     */
    public function removerRegistroCaixa($idRegistro)
    {
        try{
            if(empty($idRegistro)){
                throw new ServiceException(sprintf('E002', 'Códido do Registro de Caixa'), 401);
            }
            /* @var $srvValorRegistro \Tesouraria\Service\ValorRegistroCaixaService */
            $srvValorRegistro = $this->getServiceManager()->get('ValorRegistroCaixaService');
            $this->setEntity('Application\Entity\RegistroCaixa');
            $em = $this->getDefaultEntityManager();
            $em->beginTransaction();
            $srvValorRegistro->removerPagamentoPorRegistro($idRegistro);
            $this->delete($idRegistro, $this->getDefaultEntityManager());
            $em->commit();
            return array(
                'stSucesso' => true
            );
        }catch(\Exception $e){
            $em->rollback();
            throw new ServiceException($e->getMessage(), 401);
        }

    }
}
