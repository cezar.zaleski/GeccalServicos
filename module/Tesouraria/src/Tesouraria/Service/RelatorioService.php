<?php

namespace Tesouraria\Service;

use Application\Entity\RegistroCaixa;
use Application\Entity\TipoLancamento;
use Zend\Debug\Debug;
use Zend\Http\Response;
use Zend\View\Model\ViewModel;
use Application\Entity\TipoPagamento;
use Zend\Http\Headers;

class RelatorioService extends AbstractRelatorioService {

    protected $entity;
    protected $em;
    protected $serviceManager;
    
    const REGISTRO_CAIXA  = "registro_caixa";
    const MOVIMENTO_CAIXA = "movimento_caixa";
    const BALANCETE       = "balancete";
    const ANO_INICIAL      = 2016;

    /**
     * RelatorioService constructor.
     */
    public function __construct()
    {
        $this->entity = 'Application\Entity\RegistroCaixa';
    }


    /**
     * Geração dos relatórios de acordo com o tipo de relatório informado
     * @param string $tipo
     * @param int $mes
     * @param int $ano
     * @return mixed|void
     */
    public function gerarRelatorio($tipo, $mes, $ano) {
        /* @var $srvRegCaixa \Tesouraria\Service\RegistroCaixaService */
        $srvRegCaixa = $this->getSm('RegistroCaixaService');


        switch ($tipo){
            case self::BALANCETE:
                /* @var $repo \Application\Repository\DescricaoLancamentoRepository */
                $repo = $this->getDefaultEntityManager()->getRepository('\Application\Entity\DescricaoLancamento');
                $dados = $repo->findByBalancete($mes, $ano);
                return $this->gerarBalancete($dados, $mes, $ano);
            case self::REGISTRO_CAIXA:
                /* @var $repo \Application\Repository\RegistroCaixaRepository */
                $repo = $this->getDefaultRepository();
                $dados = $repo->findByRelatorioCaixa($mes,$ano);
                /* @var $srvRelRegCaixa \Tesouraria\Service\RelatorioRegistroCaixaService */
                $srvRelRegCaixa = $this->getSm('RelatorioRegistroCaixaService');
                $saldo = new \stdClass();
                $saldo->saldoAnterior = $srvRegCaixa->saldoAnterior($mes, $ano);
                $saldo->saldoAtual = $srvRegCaixa->saldoAtual($mes, $ano);
                return $srvRelRegCaixa->gerar($dados, $mes, $ano, $saldo);
            case self::MOVIMENTO_CAIXA:
                /* @var $repo \Application\Repository\RegistroCaixaRepository */
                $repo = $this->getDefaultRepository();
                $dados = $repo->findByRelatorioMovimentoCaixa($mes,$ano);
                $saldoMovimento = $srvRegCaixa->saldoAnterior($mes,$ano);
                return $this->gerarMovimentoCaixa($dados, $saldoMovimento, $mes, $ano);
                break;
        }
    }

    /**
     * Gera o relatorio de movimento de caixa
     * @param array $dados
     * @param array $saldo
     * @return mixed
     */
    public function gerarMovimentoCaixa($dados, $saldo, $mes, $ano)
    {
        $viewModel = new ViewModel(array('registroCaixa' => $this->formatarMovimentoCaixa($dados, $saldo)));
        $viewModel->setTemplate('tesouraria/relatorio/movimento_caixa.phtml');
        $viewRender = $this->getSm('ViewRenderer');

        $nome = "Movimento de Caixa {$mes}-{$ano}";
        $relatorio = $viewRender->render($viewModel);
//        echo $relatorio;exit;


        $response = new Response();
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/vnd.ms-excel;charset=utf-8')
            ->addHeaderLine(
                'Content-Disposition',
                sprintf("attachment; filename=\"%s\"", $nome.'.xls')
            )
            ->addHeaderLine('Accept-Ranges', 'bytes')
            ->addHeaderLine('Content-Length', strlen($relatorio));

        $response->setContent($relatorio);
        return $response;
    }

    /**
     * @param array $data
     * @param array $saldo
     * @return array
     */
    private function formatarMovimentoCaixa($data, $saldo)
    {
        if(!count($data)){
            return array();
        }
        $registro = current($data);
        $dtRegistroInicial = \DateTime::createFromFormat('Ymd', $registro['dtRegistro']->format('Ym').'01');
        $return = array();
        $indice = $dtRegistroInicial->format('d/m/Y').'1';
        $return[$indice]['dtRegistro'] = clone $dtRegistroInicial;
        $return[$indice]['noPessoa'] = "Saldo Anterior Caixa ".$dtRegistroInicial->sub(new \DateInterval('P1D'))->format('d/m/Y');
        $return[$indice]['idTipoRegistroCaixa'] = \Application\Entity\TipoRegistroCaixa::TP_REG_ENTRADA;
        $return[$indice]['dinheiro'] = $saldo['slDinheiro'];
        $return[$indice]['cheque'] = $saldo['slCheque'];
        $return[$indice]['credito'] = $saldo['slCredito'];
        $return[$indice]['debito'] = $saldo['slDebito'];

        $return[$indice]['slDinheiro'] = $saldo['slDinheiro'];
        $return[$indice]['slCheque'] = $saldo['slCheque'];
        $return[$indice]['slCredito'] = $saldo['slCredito'];
        $return[$indice]['slDebito'] = $saldo['slDebito'];
        foreach($data as $val){
            $id = $val['dtRegistro']->format('d/m/Y').$val['idTipoRegistroCaixa'];
            $return[$id]['dtRegistro'] = $val['dtRegistro'];
            $return[$id]['noTipoRegistroCaixa'] = $val['noTipoRegistroCaixa'];
            $return[$id]['noPessoa'] = $val['noPessoa'];
            $return[$id]['idTipoRegistroCaixa'] = $val['idTipoRegistroCaixa'];
            switch($val['idTipoPagamento']){
                case TipoPagamento::TP_PG_CHEQUE:
                    array_key_exists('cheque', $return[$id]) ? $return[$id]['cheque'] += $val['nuValor'] : $return[$id]['cheque'] = $val['nuValor'];
                    break;
                case TipoPagamento::TP_PG_CREDITO:
                    array_key_exists('credito', $return[$id]) ? $return[$id]['credito'] += $val['nuValor'] : $return[$id]['credito'] = $val['nuValor'];
                    break;
                case TipoPagamento::TP_PG_DEBITO:
                    array_key_exists('debito', $return[$id]) ? $return[$id]['debito'] += $val['nuValor'] : $return[$id]['debito'] = $val['nuValor'];
                    break;
                case TipoPagamento::TP_PG_DINHEIO:
                    array_key_exists('dinheiro', $return[$id]) ? $return[$id]['dinheiro'] += $val['nuValor'] : $return[$id]['dinheiro'] = $val['nuValor'];
                    break;

            }
        }
        return $return;
    }

    /**
     * Retorna os relatorios disponiveis de cada mes/ano
     * @return array
     */
    public function relatoriosDisponiveis()
    {
        $data = array();
        /* @var $repo \Application\Repository\RegistroCaixaRepository */
        $repo = $this->getDefaultRepository();
        $result = $repo->findByRelatorios();
        if(!count($result)){
            return $data;
        }

        foreach($result as $reg){
            $reg['no_mes'] = $this->mesPorExtenso($reg['nu_mes']);
            $reg['no_relatorio'] = 'Registro de Caixa';
            $reg['ds_relatorio'] = 'Detalhamento do Registro de Caixa';
            $reg['tp_relatorio'] = self::REGISTRO_CAIXA;
            array_push($data, $reg);
            $reg['no_relatorio'] = 'Movimento de Caixa';
            $reg['ds_relatorio'] = 'Detalhamento do Movimento de Caixa';
            $reg['tp_relatorio'] = self::MOVIMENTO_CAIXA;
            array_push($data, $reg);
            $reg['no_relatorio'] = 'Balancete Financeiro';
            $reg['ds_relatorio'] = 'Detalhamento do Balancete Financeiro';
            $reg['tp_relatorio'] = self::BALANCETE;
            array_push($data, $reg);
        }
        return $data;
    }

    /**
     * Geraçao do relatorio de Balancete Financeiro
     * @param array $dados
     * @return Response
     */
    public function gerarBalancete($dados, $mes, $ano)
    {
        $viewModel = new ViewModel(
            array(
                'dadosBalancete' => $dados,
                'saldo' => $this->saldoBalancete($mes, $ano),
                'total' => $this->totalBalancete($dados),
                'noMes'=> $this->mesPorExtenso($mes),
                'nuAno'=> $ano
            )
        );
        $viewModel->setTemplate('tesouraria/relatorio/balancete_financeiro.phtml');
        $viewRender = $this->getSm('ViewRenderer');

        $nome = "Balancete Financeiro {$mes}-{$ano}";
        $relatorio = $viewRender->render($viewModel);


        $response = new Response();
        $headers = $response->getHeaders();
        $headers->addHeaderLine('Content-Type', 'application/vnd.ms-excel;charset=utf-8')
            ->addHeaderLine(
                'Content-Disposition',
                sprintf("attachment; filename=\"%s\"", $nome.'.xls')
            )
            ->addHeaderLine('Accept-Ranges', 'bytes')
            ->addHeaderLine('Content-Length', strlen($relatorio));

        $response->setContent($relatorio);
        return $response;
    }

    /**
     * Total dos valores para o relatorio de balancete financeiro
     * @param array $dados
     * @return mixed
     */
    private function totalBalancete($dados)
    {
        $arrEstrutura = array('mes' => 0, 'ano' =>0);
        $vlReceita = $arrEstrutura;
        $vlDespesa = $arrEstrutura;
        $vlInvestimento = $arrEstrutura;
        foreach($dados as $value){
            switch($value['id_tipo_lancamento']){
                case TipoLancamento::TP_RECEITA:
                    $vlReceita['mes'] += $value['val_nu_mes'];
                    $vlReceita['ano'] += $value['val_nu_ano'];
                    break;
                case TipoLancamento::TP_DESPESA:
                    $vlDespesa['mes'] += $value['val_nu_mes'];
                    $vlDespesa['ano'] += $value['val_nu_ano'];
                    break;
                case TipoLancamento::TP_INVESTIMENTO:
                    $vlInvestimento['mes'] += $value['val_nu_mes'];
                    $vlInvestimento['ano'] += $value['val_nu_ano'];
                    break;
            }
        }
        $return = new \stdClass();
        $return->vlReceita = $vlReceita;
        $return->vlDespesa = $vlDespesa;
        $return->vlInvestimento = $vlInvestimento;
        $return->vlfinal = $vlInvestimento;
        return $return;
    }

    /**
     * Retorna objeto contendo as informaçoes de saldo para o relatorio de balancete
     * @param int $mes
     * @param int $ano
     * @return \stdClass
     */
    public function saldoBalancete($mes, $ano)
    {
        /* @var $srvConta \Tesouraria\Service\ContaService */
        $srvConta = $this->getSm('ContaService');
        /* @var $srvRegCaixa \Tesouraria\Service\RegistroCaixaService */
        $srvRegCaixa = $this->getSm('RegistroCaixaService');

        $saldo = new \stdClass();
        $saldo->saldoConta = $srvConta->saldoContaBalancete($mes, $ano);
        $saldo->saldoCaixaMes = $srvRegCaixa->saldoAtual($mes,$ano);
        if ($ano == $this::ANO_INICIAL) {
            $saldo->saldoCaixaAno = array(
                "slDinheiro" => RegistroCaixa::slInicialDinheiroAno,
                "slCheque" => 0,
                "slDebito" => 0,
                "slCredito" => 0,
                "slTotal" => RegistroCaixa::slInicialDinheiroAno
            );
        } else {
            $saldo->saldoCaixaAno = $srvRegCaixa->saldoAnterior(1, $ano);
        }
        $saldo->saldoBancosMesAnterior = $saldo->saldoCaixaMes['slTotal'] + $saldo->saldoConta['saldoMesAnterior'][0]['nu_saldo'] +
            $saldo->saldoConta['saldoMesAnterior'][1]['nu_saldo'];
        $saldo->saldoBancosAnoAnterior = $saldo->saldoCaixaAno['slTotal'] + $saldo->saldoConta[0]['nu_saldo'] +
            $saldo->saldoConta[1]['nu_saldo'];
        return $saldo;
    }
}
