<?php

namespace Tesouraria\Service;

use Application\Entity\MovimentacaoConta;
use Application\Entity\MovimentacaoContaDescricao;
use Application\Entity\DescricaoMovimentoConta;
use Application\Entity\TipoRegistroCaixa;
use Application\Service\AbstractService;
use Application\Service\Exception\ServiceException;


class MovimentacaoContaService extends AbstractService {

    const INICIO_MOVIMENTACAO = '2015-12-01';
    private $movEntrada = array (1, 3, 5, 7, 8, 10);
    private $movSaida = array (2, 6, 9);

    /**
     * MovimentacaoContaCaixaService constructor.
     */
    public function __construct()
    {
        $this->setEntity('Application\Entity\MovimentacaoConta');
    }

    public function buscarMovimentacaoPorConta($idConta)
    {
        if($idConta){
            /* @var $repo \Application\Repository\MovimentacaoContaRepository */
            $repo = $this->getDefaultRepository();
            return $repo->buscarMovimentacaoPorConta($idConta);
        }

    }

    /**
     * Listar combo de mes ano
     * @return array
     */
    public function listarAnoMesCombo($form = false)
    {
        $anoMes = array();
        $dtInicial = new \DateTime(self::INICIO_MOVIMENTACAO);
        $dtAtual = new \DateTime('now');

        if(!$form){
            $count = 0;
            while($dtInicial->format('Ym') <= $dtAtual->format('Ym')){
                $anoMes[$count]['nu_mes_ano'] = $dtInicial->format('Ym');
                $anoMes[$count]['nu_mes_ano_desc'] = "{$this->mesPorExtenso((int)$dtInicial->format('m'))}/{$dtInicial->format('Y')}";
                $dtInicial->add(new \DateInterval('P1M'));
                $count++;
            }
            return $anoMes;
        }
        while($dtInicial->format('Ym') <= $dtAtual->format('Ym')){
            $anoMes[$dtInicial->format('Ym')] = "{$this->mesPorExtenso((int)$dtInicial->format('m'))}/{$dtInicial->format('Y')}";
            $dtInicial->add(new \DateInterval('P1M'));
        }
        return $anoMes;
    }

    public function salvarMovimentoConta($data)
    {
        $em = $this->getDefaultEntityManager();
        $em->beginTransaction();
        try{
            $this->validarMovimentoConta($data);
            $data = $this->definirTipoRegistro($data);
            $em = $this->getDefaultEntityManager();
            $data['stAtivo'] = true;
            $conta = $data['conta'];
            $data['conta'] = $em->getPartialReference('Application\Entity\Conta', $data['conta']);
            $data['descricaoMovimentoConta'] = $em->getPartialReference('Application\Entity\DescricaoMovimentoConta', $data['descricaoMovimentoConta']);

            $repo = $this->getDefaultEntityManager()->getRepository('Application\Entity\MovimentacaoConta');
            $objMovConta = $repo->findBy(array(
                'nuAnoMes' => $data['nuAnoMes'],
                'conta' => $conta
            ));
            //se existe movimentaçao de conta para o mes informado so insere a descriçao
            if(count($objMovConta)){
                $data['movimentacaoConta'] = current($objMovConta);
                /* @var $srvDescMovConta \Tesouraria\Service\MovimentacaoContaDescricaoService */
                $srvDescMovConta = $this->getSm('MovimentacaoContaDescricaoService');
                $srvDescMovConta->gravarDescricaoMovimentoConta($data);

            }else{
                //cria a movimentaçao de conta para o mes/ano informado e a descriçao
                $eMovConta = new MovimentacaoConta($data);
                $em->persist($eMovConta);
                $em->flush();
                $eMovContaDesc = new MovimentacaoContaDescricao($data);
                $eMovContaDesc->setMovimentacaoConta($eMovConta);
                $em->persist($eMovContaDesc);
                $em->flush();
            }
            $em->commit();
            return array(
                'stSucesso' => true
            );

        }catch(\Exception $e){
            $em->rollback();
            throw new ServiceException($e->getMessage(), 401);
        }
    }

    private function validarMovimentoConta($data)
    {
        if($data['descricaoMovimentoConta'] == DescricaoMovimentoConta::CHEQUE &&
            empty($data['nuCheque'])){
            throw new ServiceException(sprintf('E002', 'Cheque nº'), 401);

        }
        if($data['descricaoMovimentoConta'] == DescricaoMovimentoConta::TRANSFERENCIA_ONLINE &&
            empty($data['dsTransferencia'])){
            throw new ServiceException(sprintf('E002', 'Descrição Transferência'), 401);

        }
        if($data['descricaoMovimentoConta'] == DescricaoMovimentoConta::DEBITO_AUTOMATICO &&
            empty($data['dsDebito'])){
            throw new ServiceException(sprintf('E002', 'Descrição Débito Automático'), 401);

        }
    }

    private function definirTipoRegistro($data)
   {
       if(in_array($data['descricaoMovimentoConta'], $this->movEntrada)){
           $data['tipoRegistroCaixa'] = $this->getDefaultEntityManager()
               ->getPartialReference('Application\Entity\TipoRegistroCaixa', TipoRegistroCaixa::TP_REG_ENTRADA);
           return $data;
       }

       if(in_array($data['descricaoMovimentoConta'], $this->movSaida)){
           $data['tipoRegistroCaixa'] = $this->getDefaultEntityManager()
               ->getPartialReference('Application\Entity\TipoRegistroCaixa', TipoRegistroCaixa::TP_REG_SAIDA);
           return $data;
       }
       $data['tipoRegistroCaixa'] = $this->getDefaultEntityManager()
           ->getPartialReference('Application\Entity\TipoRegistroCaixa', $data['tipoRegistroCaixa']);
       return $data;
    }


}
