<?php

namespace Tesouraria\Service;

use Application\Service\AbstractService;
use Application\Service\Exception\ServiceException;


class ValorRegistroCaixaService extends AbstractService {

    /**
     * Lista os valores de um determinado registro de caixa
     * @param int $idRegistroCaixa
     * @return array
     */
    public function listarPagamentoRegistroCaixa($idRegistroCaixa)
    {
        $repo = $this->getDefaultEntityManager()->getRepository('Application\Entity\Pagamento');
        return $repo->findBy(array(
            'registroCaixa' => $idRegistroCaixa,
            'stAtivo' => true
        ));
    }

    /**
     * REaliza gravaçao do valor de registro de caixa
     * @param array $data
     * @return array
     */
    public function gravar($data)
    {
        try{
            $em = $this->getDefaultEntityManager();
            $em->beginTransaction();
            $data['registroCaixa'] = $this->getRegistroCaixaPartial($data['idRegistroCaixa']);
            $data['tipoPagamento'] = $em->getPartialReference('Application\Entity\TipoPagamento', $data['idTipoPagamento']);
            $data['stAtivo'] = true;
            $this->insert($data);
            $em->commit();
            return array(
                'stSucesso' => true,
                'idRegistroCaixa' => $data['registroCaixa']->getIdRegistroCaixa()
            );
        }catch(\Exception $e){
            $em->rollback();
            throw new ServiceException($e->getMessage(), 401);
        }
    }

    /**
     * Retorna registro de caixa fpartial para persistencia
     * @param null $idRegistroCaixa
     * @return bool|null|object
     */
    public function getRegistroCaixaPartial($idRegistroCaixa = null)
    {
        $this->setEntity('Application\Entity\Pagamento');
        $em = $this->getDefaultEntityManager();
        $srvRegistroCaixa = $this->getServiceManager()->get('RegistroCaixaService');
        if(empty($idRegistroCaixa)){
            $data['stAtivo'] = false;
            $data['usuario'] = $em->getPartialReference('Application\Entity\Usuario', 1);
            $registroCaixa = $srvRegistroCaixa->gravar($data);
            return $em->getPartialReference('Application\Entity\RegistroCaixa', $registroCaixa->getIdRegistroCaixa());

        }
        return $em->getPartialReference('Application\Entity\RegistroCaixa', $idRegistroCaixa);

    }

    /**
     * Remove um valor de pagamento
     * @param int $idPagamento
     * @return array
     */
    public function removerPagamento($idPagamento){
        try{
            if(empty($idPagamento)){
                throw new ServiceException(sprintf('E002', 'Códido do pagamento'), 401);
            }
            $this->setEntity('Application\Entity\Pagamento');
            $this->delete($idPagamento, $this->getDefaultEntityManager());
            return array(
                'stSucesso' => true
            );
        }catch(\Exception $e){
            throw new ServiceException('E004', 401);
        }
    }

    /**
     * Valida se existe valor cadastrado para o registro de caixa
     * @param int $idRegistroCaixa
     * @return bool
     */
    public function validarValorRegistroCaixa($idRegistroCaixa)
    {
        $pagamentos = $this->listarPagamentoRegistroCaixa($idRegistroCaixa);
        if(count($pagamentos)){
            return true;
        }
        return false;
    }

    /**
     * Remove os valores de um registro de caixa
     * @param int $idRegistro
     * @return bool
     */
    public function removerPagamentoPorRegistro($idRegistro){
        try{
            /* @var $repo \Application\Repository\PagamentoRepository */
            $repo = $this->getDefaultEntityManager()->getRepository('Application\Entity\Pagamento');
            $repo->removerPagamentoPorRegistro($idRegistro);
            return true;
        }catch(\Exception $e){
            throw new ServiceException($e->getMessage(), 401);
        }
    }
}
