<?php

namespace Tesouraria;

return array(
    'controllers' => array(
        'invokables' => array(
            'Tesouraria\Controller\RegistroCaixa' => 'Tesouraria\Controller\RegistroCaixaController',
            'Tesouraria\Controller\TipoPagamento' => 'Tesouraria\Controller\TipoPagamentoController',
            'Tesouraria\Controller\DescricaoLancamento' => 'Tesouraria\Controller\DescricaoLancamentoController',
            'Tesouraria\Controller\TipoRegistroCaixa' => 'Tesouraria\Controller\TipoRegistroCaixaController',
            'Tesouraria\Controller\ValorRegistroCaixa' => 'Tesouraria\Controller\ValorRegistroCaixaController',
            'Tesouraria\Controller\Relatorio' => 'Tesouraria\Controller\RelatorioController',
            'Tesouraria\Controller\GerarRelatorio' => 'Tesouraria\Controller\GerarRelatorioController',
            'Tesouraria\Controller\Conta' => 'Tesouraria\Controller\ContaController',
            'Tesouraria\Controller\AnoMesMovimentacao' => 'Tesouraria\Controller\AnoMesMovimentacaoController',
            'Tesouraria\Controller\DescricaoMovimentoConta' => 'Tesouraria\Controller\DescricaoMovimentoContaController',
            'Tesouraria\Controller\MovimentoConta' => 'Tesouraria\Controller\MovimentoContaController',
            'Tesouraria\Controller\MovimentoContaDescricao' => 'Tesouraria\Controller\MovimentoContaDescricaoController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'registro-caixa' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tesouraria/registro-caixa[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Tesouraria\Controller\RegistroCaixa',
                    ),
                ),
            ),
            'tipo-pagamento' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tesouraria/tipo-pagamento[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Tesouraria\Controller\TipoPagamento',
                    ),
                ),
            ),
            'descricao-lancamento' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tesouraria/descricao-lancamento[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Tesouraria\Controller\DescricaoLancamento',
                    ),
                ),
            ),
            'tipo-registro-caixa' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tesouraria/tipo-registro-caixa[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Tesouraria\Controller\TipoRegistroCaixa',
                    ),
                ),
            ),
            'valor-registro-caixa' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tesouraria/valor-registro-caixa[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Tesouraria\Controller\ValorRegistroCaixa',
                    ),
                ),
            ),
            'relatorio' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tesouraria/relatorio[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Tesouraria\Controller\Relatorio',
                    ),
                ),
            ),
            'gerar-relatorio' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tesouraria/gerar-relatorio[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Tesouraria\Controller\GerarRelatorio',
                    ),
                ),
            ),
            'conta' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tesouraria/conta[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Tesouraria\Controller\Conta',
                    ),
                ),
            ),
            'ano-mes-movimentacao' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tesouraria/ano-mes-movimentacao[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Tesouraria\Controller\AnoMesMovimentacao',
                    ),
                ),
            ),
            'descricao-movimento-conta' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tesouraria/descricao-movimento-conta[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Tesouraria\Controller\DescricaoMovimentoConta',
                    ),
                ),
            ),
            'movimento-conta' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tesouraria/movimento-conta[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Tesouraria\Controller\MovimentoConta',
                    ),
                ),
            ),
            'movimento-conta-descricao' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tesouraria/movimento-conta-descricao',
                    'defaults' => array(
                        'controller' => 'Tesouraria\Controller\MovimentoContaDescricao',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
        'template_path_stack' => array(
            'tesouraria' => __DIR__ . '/../view',
        ),
    ),
);