<?php
namespace config;

use DoctrineModule\Service\DriverFactory;
use DoctrineModule\Service\EventManagerFactory;
use DoctrineORMModule\Form\Annotation\AnnotationBuilder;
use DoctrineORMModule\Service\DBALConnectionFactory;
use DoctrineORMModule\Service\EntityManagerFactory;
use DoctrineORMModule\Service\EntityResolverFactory;
use DoctrineORMModule\Service\SQLLoggerCollectorFactory;
use Symfony\Component\Yaml\Parser;
use Doctrine\DBAL\Configuration;
use DoctrineORMModule\Service\ConfigurationFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

return array(
    'modules' => array(
        'DoctrineModule',
        'DoctrineORMModule',
        'Application',
        'Tesouraria',
        'ZFTool',
        'ZendDeveloperTools',
        'JMSSerializerModule',
    ),
    'module_listener_options' => array(
        'module_paths' => array(
            './module',
            './vendor'
        ),
        'config_glob_paths' => array(
            'config/autoload/{,*.}{global,local}.php',
            'config/autoload/desenvolvedor.php',

        )
    ),
    'service_manager' => array(
//        'abstract_factories' => array(
//            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
//            'Zend\Log\LoggerAbstractServiceFactory',
//        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
        'translator' => array(
            'locale' => 'pt_BR',
            'translation_file_patterns' => array(
                array(
                    'type' => 'gettext',
                    'base_dir' => __DIR__ . '/../language',
                    'pattern' => '%s.mo',
                ),
                array(
                    'type' => 'phparray',
                    'base_dir' => __DIR__ . '/../language',
                    'pattern' => '%s.php',
                ),
            ),
        ),
        'factories' => array(
            'acl_rules' => function(){
                $yaml = new Parser();
                return $yaml->parse(file_get_contents('config/autoload/acl.global.yml'));
            },
            'acl_app' => function(){
                $yaml = new Parser();
                return $yaml->parse(file_get_contents('config/autoload/acl.app.yml'));
            },
//            'doctrine.configuration.orm_default' => function($sm) {
//                $factory = new ConfigurationFactory('orm_default');
//
//                /**
//                 * @var Configuration $config
//                 */
//                $config = $factory->createService($sm);
//                $config->addCustomDatetimeFunction('MONTH', 'Oro\ORM\Query\AST\Functions\SimpleFunction');
//                $config->addCustomDatetimeFunction('YEAR', 'Oro\ORM\Query\AST\Functions\SimpleFunction');
////                $config->setFilterSchemaAssetsExpression('/^usuario_perfil/');
////                $config->setFilterSchemaAssetsExpression('/^inscricao\.tb_fies_grupo_ocupacao$/');
//                //$config->setFilterSchemaAssetsExpression('/^inscricao.tb_fies_motivo_rejeicao_aditamento$/');
//                // $config->setFilterSchemaAssetsExpression('/^.vw_fies_curso/');
//                return $config;
//            },
//            'doctrine.configuration.orm_oracle' => function($sm) {
//                $factory = new ConfigurationFactory('orm_oracle');
//
//                /**
//                 * @var Configuration $config
//                 */
//                $config = $factory->createService($sm);
//                //$config->setFilterSchemaAssetsExpression('/^public\.vw_fies_ies/');
//                //$config->setFilterSchemaAssetsExpression('/^inscricao.tb_fies_motivo_rejeicao_aditamento$/');
//                // $config->setFilterSchemaAssetsExpression('/^tb_fies_estado_civil$/');
//                return $config;
//            },
//            'doctrine.connection.orm_oracle'           => new DBALConnectionFactory('orm_oracle'),
//            'doctrine.entitymanager.orm_oracle'        => new EntityManagerFactory('orm_oracle'),
//            'doctrine.driver.orm_oracle'               => new DriverFactory('orm_oracle'),
//            'doctrine.eventmanager.orm_oracle'         => new EventManagerFactory('orm_oracle'),
//            'doctrine.entity_resolver.orm_oracle'      => new EntityResolverFactory('orm_oracle'),
//            'doctrine.sql_logger_collector.orm_oracle' => new SQLLoggerCollectorFactory('orm_oracle'),
//
//            'DoctrineORMModule\Form\Annotation\AnnotationBuilder' => function(ServiceLocatorInterface $srvManager) {
//                return new AnnotationBuilder($srvManager->get('doctrine.entitymanager.orm_oracle'));
//            },
        ),
    )
);
