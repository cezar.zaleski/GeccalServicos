<?php
ini_set('error_reporting', E_ALL);
ini_set('display_startup_errors', true);
ini_set('display_errors', true);
ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);

return array(
    'send-error-email' => false,
    'path' => array(
        'upload' => sys_get_temp_dir() . '/data/fies-servicos/upload',
        'temp' => sys_get_temp_dir() . '/data/fies-servicos/fies'
    ),
    /*'doctrine' => array(
        'connection' => array(
            'orm_oracle' => array(
                'params' => array(
                    'host' => 'dsv-oracle-scan.mec.gov.br',
                    'user' => 'sysdbfiesoferta',
                    'password' => 'sysdbfiesoferta',
                    'dbname' => 'dsvora.mec.gov.br',
                    'port' => '1521',
                    ///utilizar para mapear entidades
                    /*'user' => 'fies_preinscricao', // schema FIES_PREINSCRICAO
                    'user' => 'fies_global', // schema FIES_GLOBAL
                    'password' => '123456',
                    'dbname' => 'dsvora.mec.gov.br',
                ),
            ),
            'orm_default' => array(
                'params' => array(
//                    'host' => '10.1.3.39',
//                    'user' => 'gustavoantunes',
//                    'password' => '123456',
//                    'dbname' => 'dbfies_hmg',
//                    'port'  => '5444',
                    //utilizar para mapear entidades
                    'user' => 'gustavoantunes',
                    'password' => '123456'
                    
                )
            )
        ),
    ),*/
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
    ),
    'assetsPath' => '/assets',
    'applicationUrl' => 'http://fies-servicos.local',

);
