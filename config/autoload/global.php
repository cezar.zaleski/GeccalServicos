<?php
date_default_timezone_set('America/Sao_Paulo');
ini_set('session.gc-maxlifetime', 1800);
ini_set('error_reporting', E_ALL);
ini_set('display_startup_errors', true);
ini_set('display_errors', true);
ini_set('error_reporting', E_ALL);
ini_set('display_startup_errors', true);
ini_set('display_errors', true);
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host' => 'localhost', //local
                    'port' => '3306',
                    'user' => 'root',
                    'password' => '123456',
                    'dbname' => 'geccal',
                    'charset' => 'utf8',
                    'driverOptions' => array(
                        1002=>'SET NAMES utf8'
                    )
                )
            )
        ),
        'configuration' => array(
            'orm_default' => array(
                'string_functions' => array(
                    'TO_CHAR'  => 'Application\DoctrineExtensions\PgSql\ToChar',
                    'DATE'  => 'Application\DoctrineExtensions\PgSql\Date',
                    'CAST'  => 'Application\DoctrineExtensions\PgSql\CastFunction',
                    'COALESCE'  => 'Application\DoctrineExtensions\PgSql\Coalesce',
                    'MONTHNAME'  => 'Application\DoctrineExtensions\PgSql\Monthname',
                    'MONTH'  => 'Application\DoctrineExtensions\PgSql\Month',
                    'YEAR'  => 'Application\DoctrineExtensions\PgSql\Year',
                ),
                'proxy_dir' => sys_get_temp_dir() . '/data/geccal/DoctrineORMModule/Proxy'
            )
        ),
        'entitymanager' => array(
            'orm_oracle' => array(
                'connection'    => 'orm_oracle',
                'configuration' => 'orm_oracle',
            ),
            'orm_default' => array(
                'connection'    => 'orm_default',
                'configuration' => 'orm_default'
            ),
        ),
        'eventmanager' => array(
            'orm_oracle' => array()
        ),

        'sql_logger_collector' => array(
            'orm_oracle' => array(),
        ),

        'entity_resolver' => array(
            'orm_oracle' => array()
        ),
    ),
    'mail' => array(
        'from_name' => 'SisFIES Serviços',
        'from' => array(
            'erro' => 'erro.fies@mec.gov.br',
            'aluno' => 'sisfies@mec.gov.br'
        ),
        'encoding' => 'UTF-8',
        'transport' => array(
            'options' => array(
                'host' => 'smtp4.mec.gov.br',
                'connection_config' => array(
                    'username' => '',
                    'password' => ''
                )
            )
        )
    ),
);
